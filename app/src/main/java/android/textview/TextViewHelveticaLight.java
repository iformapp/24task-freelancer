package android.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.task24.util.Constants;

public class TextViewHelveticaLight extends TextView {

    public TextViewHelveticaLight(Context context) {
        super(context);
        applyCustomFont();
    }

    public TextViewHelveticaLight(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public TextViewHelveticaLight(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.HELVETICA_LIGHT)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.HELVETICA_LIGHT);
            setTypeface(tf);
        }
    }
}
