package com.task24.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExperienceModel extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {
        @Expose
        @SerializedName("profile")
        public Profile profile;
        @Expose
        @SerializedName("experience")
        public Experience experience;
    }

    public static class Profile {
        @Expose
        @SerializedName("signup_date")
        public String signupDate;
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("profile_status")
        public int profileStatus;
        @Expose
        @SerializedName("referral_code")
        public String referralCode;
        @Expose
        @SerializedName("pay_rate")
        public int payRate;
        @Expose
        @SerializedName("workbase")
        public String workbase;
        @Expose
        @SerializedName("working_experience")
        public int workingExperience;
        @Expose
        @SerializedName("password")
        public String password;
        @Expose
        @SerializedName("username")
        public String username;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class Experience {
        @Expose
        @SerializedName("id")
        public int id;
        @Expose
        @SerializedName("service_id")
        public int serviceId;
        @Expose
        @SerializedName("length")
        public int length;
        @Expose
        @SerializedName("profile_id")
        public int profileId;
    }
}
