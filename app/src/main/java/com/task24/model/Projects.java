package com.task24.model;

public class Projects {

    public String title;
    public String daysleft;
    public String bids;
    public String budget;
    public String status;

    public Projects(String title, String daysleft, String bids, String budget, String status) {
        this.title = title;
        this.daysleft = daysleft;
        this.bids = bids;
        this.budget = budget;
        this.status = status;
    }
}
