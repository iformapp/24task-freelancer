package com.task24.model;

public class SelectItem {

    public String title;
    public boolean isSelected;

    public SelectItem(String title, boolean isSelected) {
        this.title = title;
        this.isSelected = isSelected;
    }
}
