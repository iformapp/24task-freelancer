package com.task24.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServicesModel extends GeneralModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data {
        @Expose
        @SerializedName("services")
        public List<Services> services;
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("service_category_id")
        public int serviceCategoryId;
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("name_app")
        public String name;
        @Expose
        @SerializedName("id")
        public int id;

        public String experience;

        public int experienceId;

        public boolean isSelected;
    }

    public static class Services {
        @Expose
        @SerializedName("id")
        public int id;
        @Expose
        @SerializedName("service_category_id")
        public String serviceCategoryId;
        @Expose
        @SerializedName("name")
        public String name;
    }
}
