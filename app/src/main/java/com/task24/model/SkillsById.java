package com.task24.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SkillsById extends GeneralModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data {
        @Expose
        @SerializedName("Skills")
        public Skills skills;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("note")
        public String note;
        @Expose
        @SerializedName("service_id")
        public int serviceId;
        @Expose
        @SerializedName("skill_id")
        public int skillId;
        @Expose
        @SerializedName("id")
        public int id;

        public boolean isSelected;
    }

    public static class Skills {
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("service_id")
        public int serviceId;
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("id")
        public int id;
    }
}
