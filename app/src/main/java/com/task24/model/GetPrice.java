package com.task24.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPrice extends GeneralModel {

    @Expose
    @SerializedName("data")
    public String data;
}
