package com.task24.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserModel extends GeneralModel implements Serializable {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data implements Serializable {
        @Expose
        @SerializedName("profile_type")
        public ProfileType profileType;
        @Expose
        @SerializedName("signup_date")
        public String signupDate;
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("referral_code")
        public String referralCode;
        @Expose
        @SerializedName("pay_rate")
        public int payRate;
        @Expose
        @SerializedName("workbase")
        public String workbase;
        @Expose
        @SerializedName("working_experience")
        public int workingExperience;
        @Expose
        @SerializedName("last_name")
        public String lastName;
        @Expose
        @SerializedName("first_name")
        public String firstName;
        @Expose
        @SerializedName("username")
        public String username;
        @Expose
        @SerializedName("id")
        public int id;
        @Expose
        @SerializedName("profile_id")
        public int profileId;
        @Expose
        @SerializedName("contact")
        public String contact;
        @Expose
        @SerializedName("profile_pic")
        public String profilePic;
        @Expose
        @SerializedName("email")
        public String email;
        @Expose
        @SerializedName("service_id")
        public int serviceId;
        @Expose
        @SerializedName("length")
        public int length;
        @Expose
        @SerializedName("expId")
        public int expId;
    }

    public static class ProfileType {
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("type")
        public String type;
        @Expose
        @SerializedName("id")
        public int id;
    }
}
