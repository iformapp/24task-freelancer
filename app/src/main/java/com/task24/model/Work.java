package com.task24.model;

public class Work {

    public String company;
    public String jobTitle;
    public int serviceId;
    public int length;
    public String experience;
    public String startYear;
    public String endYear;
    public boolean isCurrentlyWorking;
}
