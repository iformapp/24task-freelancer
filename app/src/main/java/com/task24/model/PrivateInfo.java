package com.task24.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrivateInfo extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {
        @Expose
        @SerializedName("contactNo")
        public String contactno;
        @Expose
        @SerializedName("email")
        public String email;
        @Expose
        @SerializedName("profile_pic")
        public String profilePic;
        @Expose
        @SerializedName("jwt")
        public String jwt;
    }
}
