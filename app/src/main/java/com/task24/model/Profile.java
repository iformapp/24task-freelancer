package com.task24.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Profile extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {
        @Expose
        @SerializedName("email")
        public String email;
        @Expose
        @SerializedName("contactNo")
        public String contactno;
        @Expose
        @SerializedName("percentage")
        public Percentage percentage;
        @Expose
        @SerializedName("photo")
        public Photo photo;
        @Expose
        @SerializedName("address")
        public Address address;
        @Expose
        @SerializedName("educations")
        public List<Educations> educations;
        @Expose
        @SerializedName("experiences")
        public List<Experiences> experiences;
        @Expose
        @SerializedName("expertise")
        public List<Expertise> expertise;
        @Expose
        @SerializedName("headline")
        public Headline headline;
        @Expose
        @SerializedName("profile_languages")
        public List<ProfileLanguages> profileLanguages;
        @Expose
        @SerializedName("profile_pay_types")
        public List<ProfilePayTypes> profilePayTypes;
        @Expose
        @SerializedName("resume")
        public Resume resume;
        @Expose
        @SerializedName("profile_skills")
        public List<ProfileSkills> profileSkills;
        @Expose
        @SerializedName("summary")
        public Summary summary;
        @Expose
        @SerializedName("otp")
        public int otp;
        @Expose
        @SerializedName("signup_date")
        public String signupDate;
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("referral_code")
        public String referralCode;
        @Expose
        @SerializedName("pay_rate")
        public double payRate;
        @Expose
        @SerializedName("workbase")
        public String workbase;
        @Expose
        @SerializedName("working_experience")
        public int workingExperience;
        @Expose
        @SerializedName("last_name")
        public String lastName;
        @Expose
        @SerializedName("first_name")
        public String firstName;
        @Expose
        @SerializedName("username")
        public String username;
        @Expose
        @SerializedName("facebook_id")
        public String facebookId;
        @Expose
        @SerializedName("google_id")
        public String googleId;
        @Expose
        @SerializedName("id")
        public int id;
        @Expose
        @SerializedName("trust_verification_points")
        public TrustPoints trustPoints;
    }

    public static class Percentage {
        @Expose
        @SerializedName("total_percentage")
        public int totalPercentage;
        @Expose
        @SerializedName("verification")
        public int verification;
        @Expose
        @SerializedName("professional_info")
        public int professionalInfo;
        @Expose
        @SerializedName("skill")
        public int skill;
        @Expose
        @SerializedName("private_info")
        public int privateInfo;
        @Expose
        @SerializedName("profile")
        public int profile;
    }

    public static class TrustPoints {
        @Expose
        @SerializedName("total_points")
        public int totalPoints;
        @Expose
        @SerializedName("Verify_id")
        public int verifyId;
        @Expose
        @SerializedName("payment")
        public int payment;
        @Expose
        @SerializedName("facebook")
        public int facebook;
        @Expose
        @SerializedName("phone_number")
        public int phoneNumber;
        @Expose
        @SerializedName("email")
        public int email;
    }

    public static class Photo {
        @Expose
        @SerializedName("img")
        public String img;
    }

    public static class Address {
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("city")
        public String city;
        @Expose
        @SerializedName("region")
        public String region;
        @Expose
        @SerializedName("country")
        public String country;
        @Expose
        @SerializedName("profile_id")
        public int profileId;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class Expertise {
        @Expose
        @SerializedName("service_category")
        public Services services;
        @Expose
        @SerializedName("length")
        public int length;
        @Expose
        @SerializedName("service_id")
        public int serviceId;
        @Expose
        @SerializedName("profile_id")
        public int profileId;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class Educations {
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("level")
        public int level;
        @Expose
        @SerializedName("end_date")
        public String endDate;
        @Expose
        @SerializedName("start_date")
        public String startDate;
        @Expose
        @SerializedName("school_name")
        public String schoolName;
        @Expose
        @SerializedName("degree")
        public String degree;
        @Expose
        @SerializedName("profile_id")
        public int profileId;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class Experiences {
        @Expose
        @SerializedName("service_category")
        public Service service;
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("length")
        public int length;
        @Expose
        @SerializedName("end_date")
        public String endDate;
        @Expose
        @SerializedName("start_date")
        public String startDate;
        @Expose
        @SerializedName("company_name")
        public String companyName;
        @Expose
        @SerializedName("service_id")
        public int serviceId;
        @Expose
        @SerializedName("profile_id")
        public int profileId;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class Services {
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("name_app")
        public String nameApp;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class Service {
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("name_app")
        public String nameApp;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class Headline {
        @Expose
        @SerializedName("content")
        public String content;
    }

    public static class ProfileLanguages {
        @Expose
        @SerializedName("language")
        public Language language;
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("level")
        public int level;
        @Expose
        @SerializedName("language_id")
        public int languageId;
        @Expose
        @SerializedName("profile_id")
        public int profileId;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class Language {
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("order_by")
        public int orderBy;
        @Expose
        @SerializedName("is_popular")
        public int isPopular;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class ProfilePayTypes {
        @Expose
        @SerializedName("pay_type")
        public PayType payType;
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("pay_type_id")
        public int payTypeId;
        @Expose
        @SerializedName("profile_id")
        public int profileId;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class PayType {
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("detail")
        public String detail;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class Resume {
        @Expose
        @SerializedName("file")
        public String file;
    }

    public static class ProfileSkills {
        @Expose
        @SerializedName("skill")
        public Skill skill;
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("rating")
        public int rating;
        @Expose
        @SerializedName("skill_id")
        public int skillId;
        @Expose
        @SerializedName("profile_id")
        public int profileId;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class Skill {
        @Expose
        @SerializedName("status")
        public int status;
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("skill_category_id")
        public int skillCategoryId;
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("id")
        public int id;
    }

    public static class Summary {
        @Expose
        @SerializedName("content")
        public String content;
    }
}
