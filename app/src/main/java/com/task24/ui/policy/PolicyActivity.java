package com.task24.ui.policy;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewSFDisplayBold;
import android.textview.TextViewSFDisplayRegular;
import android.view.View;
import android.widget.TextView;

import com.task24.R;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;

public class PolicyActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.tv_right)
    TextViewSFDisplayRegular tvRight;
    @BindView(R.id.rv_policy)
    RecyclerView rvPolicy;

    private RecyclerviewAdapter mAdapter;
    private ArrayList<String> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);
        ButterKnife.bind(this);

        arrayList = fillData();

        tvToolbarTitle.setText(getString(R.string.policy).toUpperCase());
        tvRight.setText(getString(R.string.chat));

        rvPolicy.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RecyclerviewAdapter(arrayList, R.layout.item_policy, this);
        rvPolicy.setAdapter(mAdapter);
        rvPolicy.setNestedScrollingEnabled(false);
    }

    @OnClick({R.id.ll_back, R.id.tv_right, R.id.card_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_right:
                Intercom.client().displayMessageComposer();
                break;
            case R.id.card_location:
                redirectUsingCustomTab("https://www.google.com/maps/place/24+Task+-+Virtual+Solutions/@14.0993311,122.953149,17z/data=!4m5!3m4!1s0x3398aef000000003:0x8e637de8f2c91be0!8m2!3d14.099578!4d122.955035?hl=en-US");
                break;
        }
    }

    @Override
    public void bindView(View view, final int position) {
        TextView textView = view.findViewById(R.id.tv_policy_title);
        View itemView = view.findViewById(R.id.rl_view);
        textView.setText(arrayList.get(position));
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (urlData().get(position) != null) {
                    redirectUsingCustomTab(urlData().get(position));
                } else {
                    showContactUsDialog();
                }
            }
        });
    }

    public ArrayList fillData() {
        arrayList = new ArrayList<>();
        arrayList.add(getString(R.string.privacy_policy));
        arrayList.add(getString(R.string.terms_of_use));
        arrayList.add(getString(R.string.about_us));
        arrayList.add(getString(R.string.contact_us));
        arrayList.add(getString(R.string.faqs));
        return arrayList;
    }

    public ArrayList<String> urlData() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(Constants.PRIVACY);
        arrayList.add(Constants.TERMS_USE);
        arrayList.add(Constants.ABOUTUS);
        arrayList.add(null);
        arrayList.add(Constants.FAQS);
        return arrayList;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
