package com.task24.ui;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.TabHost;
import android.widget.Toast;

import com.task24.R;
import com.task24.api.ApiClient;
import com.task24.api.ApiInterface;
import com.task24.model.GeneralModel;
import com.task24.ui.auth.LoginSignUpActivity;
import com.task24.ui.browse.BrowseSearchActivity;
import com.task24.ui.calculate.CalculateActivity;
import com.task24.ui.chat.ChatActivity;
import com.task24.ui.clientprofile.ClientMoreActivity;
import com.task24.ui.clientprofile.PostJobActivity;
import com.task24.ui.freetrial.FreeTrialActivity;
import com.task24.ui.home.ClientHomeActivity;
import com.task24.ui.home.WorkHomeActivity;
import com.task24.ui.pro.Pro24TaskActivity;
import com.task24.ui.projects.MyProjectsActivity;
import com.task24.ui.workprofile.WorkMoreActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends TabActivity implements TabHost.OnTabChangeListener {

    private TabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTabHost = findViewById(android.R.id.tabhost);

        if (!TextUtils.isEmpty(getJWT()))
            isTokenExpired();

        setTab("home", isClientAccount() ? R.drawable.tab_chat : R.drawable.tab_home,
                isClientAccount() ? ChatActivity.class : WorkHomeActivity.class);
        setTab("chat", isClientAccount() ? R.drawable.tab_project : R.drawable.tab_chat,
                isClientAccount() ? MyProjectsActivity.class : ChatActivity.class);
        setTab("plus", R.drawable.tab_plus, isClientAccount() ? PostJobActivity.class : Pro24TaskActivity.class);
        if (isClientAccount()) {
            setTab("search", R.drawable.tab_search, BrowseSearchActivity.class);
        }
        setTab("profile", R.drawable.tab_profile, isClientAccount() ? ClientMoreActivity.class : WorkMoreActivity.class);

        mTabHost.setOnTabChangedListener(this);

        if (getIntent() != null) {
            int screen = getIntent().getIntExtra(Constants.SCREEN_NAME, 0);
            mTabHost.setCurrentTab(screen);
        }
    }

    public void setTab(String tag, int drawable, Class<?> activityClass) {
        TabHost.TabSpec tabSpec = mTabHost.newTabSpec(tag)
                .setIndicator("", ContextCompat.getDrawable(this, drawable))
                .setContent(new Intent(this, activityClass));
        mTabHost.addTab(tabSpec);
    }

    @Override
    public void onTabChanged(String tabId) {

    }

    public boolean isClientAccount() {
        return Preferences.readBoolean(this, Constants.IS_CLIENT_ACCOUNT, false);
    }

    public void gotoMainActivity(int screen) {
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(Constants.SCREEN_NAME, screen);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public void clearTopActivity(Class<?> activityClass) {
        Intent i = new Intent(this, activityClass);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public void redirectActivity(Class<?> activityClass) {
        startActivity(new Intent(this, activityClass));
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }

    public void goToLoginSignup(boolean isLogin, boolean isNeedToFinish) {
        Intent i = new Intent(this, LoginSignUpActivity.class);
        i.putExtra(Constants.FROM_LOGIN, isLogin);
        i.putExtra(Constants.LOGIN_FINISH, isNeedToFinish);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }

    public void isTokenExpired() {
        if (!isNetworkConnected())
            return;

        ApiInterface service = ApiClient.getClient().create(ApiInterface.class);
        Call<GeneralModel> call = service.isTokenExpired(getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                GeneralModel model = response.body();
                if (checkStatus(model)) {

                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                //Log.e("Fail", t.getMessage());
            }
        });
    }

    public String getJWT() {
        return Preferences.readString(this, Constants.JWT, "");
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null && cm.getActiveNetworkInfo() != null) {
            return true;
        }
        Toast.makeText(this, "connect to internet", Toast.LENGTH_SHORT).show();
        return false;
    }

    public boolean checkStatus(GeneralModel model) {
        if (model == null)
            return false;

        if (model.success != null) {
            switch (model.success) {
                case "1":
                    return true;
            }
        } else if (model.flag == 1) {
            return true;
        } else if (model.flag == 0) {
            if (model.msg != null && model.msg.equalsIgnoreCase("Expired Token.")) {
                Preferences.clearPreferences(this);
                Preferences.saveUserData(this, null);
                Preferences.setProfileData(this, null);
                goToLoginSignup(true, true);
                return false;
            }
        }
        failureError(model.msg);
        return false;
    }

    public void failureError(String message) {
        if (!TextUtils.isEmpty(message))
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
