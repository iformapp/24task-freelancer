package com.task24.ui.freetrial;

import android.app.Dialog;
import android.checkbox.CheckBoxSFTextRegular;
import android.content.DialogInterface;
import android.edittext.EditTextSFTextRegular;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.textview.TextViewSFTextRegular;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.adapter.ServicesAdapter;
import com.task24.ccp.CountryCodePicker;
import com.task24.model.GeneralModel;
import com.task24.model.ServicesModel;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FreeTrialActivity extends BaseActivity {

    @BindView(R.id.et_fullname)
    EditTextSFTextRegular etFullname;
    @BindView(R.id.et_email)
    EditTextSFTextRegular etEmail;
    @BindView(R.id.et_phone)
    EditTextSFTextRegular etPhone;
    @BindView(R.id.et_website)
    EditTextSFTextRegular etWebsite;
    @BindView(R.id.tv_services)
    TextViewSFTextRegular tvServices;
    @BindView(R.id.et_description)
    EditTextSFTextRegular etDescription;
    @BindView(R.id.ll_send)
    LinearLayout llSend;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.chk_tnc)
    CheckBoxSFTextRegular chkTnc;
    @BindView(R.id.tv_countries)
    TextViewSFTextRegular tvCountries;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;

    private ServicesAdapter serviceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free_trial);
        ButterKnife.bind(this);

        ClickableSpan tncClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                redirectUsingCustomTab(Constants.TERMS_USE);
            }
        };

        ClickableSpan ppClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                redirectUsingCustomTab(Constants.PRIVACY);
            }
        };

        etDescription.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etDescription.setRawInputType(InputType.TYPE_CLASS_TEXT);
        Utils.makeLinks(chkTnc, new String[]{"Terms and Conditions", "Privacy Policy"}, new ClickableSpan[]{tncClick, ppClick});

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                tvCountries.setText(ccp.getSelectedCountryName());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        String serviceName = Preferences.readString(this, Constants.SERVICE_NAME, "");
        tvServices.setText(serviceName);
        if (!isEmpty(getServiceName())) {
            llSend.setAlpha(1.0f);
        }
    }

    public boolean isValid() {
        if (isEmpty(getName()) || getName().length() < 3) {
            validationError("Please enter valid first name");
            return false;
        }

        if (!isValidEmail(getEmail())) {
            validationError("Please enter valid email");
            return false;
        }

        if (!isValidMobile(getPhone())) {
            validationError("Please enter valid phone number");
            return false;
        }

        if (!isValidUrl(getWebsite())) {
            validationError("Please enter valid website");
            return false;
        }

        if (isEmpty(getCountry())) {
            validationError("Please select country");
            return false;
        }

        if (isEmpty(getDescription())) {
            validationError("Please enter valid description");
            return false;
        }

        String[] split = getDescription().split(" ");
        if (split.length < 3) {
            validationError("Please enter atleast 3 words");
            return false;
        }

        if (!chkTnc.isChecked()) {
            validationError("Please check terms and condition");
            return false;
        }

        return true;
    }

    public String getName() {
        return etFullname.getText().toString().trim();
    }

    public String getEmail() {
        return etEmail.getText().toString().trim();
    }

    public String getPhone() {
        return etPhone.getText().toString().trim();
    }

    public String getWebsite() {
        return etWebsite.getText().toString().trim();
    }

    public String getCountry() {
        return tvCountries.getText().toString().trim();
    }

    public String getDescription() {
        return etDescription.getText().toString().trim();
    }

    public String getServiceName() {
        return tvServices.getText().toString().trim();
    }

    public void showItemSelectDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select_black);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        final EditText etSearch = dialog.findViewById(R.id.et_search);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.services).toLowerCase()));

        rvTypes.setLayoutManager(new LinearLayoutManager(this));
        List<ServicesModel.Data> mData = Preferences.getTopServices(this);
        if (mData != null && mData.size() > 0) {
            for (int i = 0; i < mData.size(); i++) {
                if (mData.get(i).name.equalsIgnoreCase(getServiceName())) {
                    mData.get(i).isSelected = true;
                }
            }
            serviceAdapter = new ServicesAdapter(this, mData);
            rvTypes.setAdapter(serviceAdapter);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (serviceAdapter != null && serviceAdapter.getSelectedItem() != null) {
                    tvServices.setText(serviceAdapter.getSelectedItem().name);
                    llSend.setAlpha(1.0f);
                    dialog.dismiss();
                } else {
                    Toast.makeText(FreeTrialActivity.this, "Please select service", Toast.LENGTH_SHORT).show();
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (serviceAdapter != null)
                    serviceAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etSearch.post(new Runnable() {
                    @Override
                    public void run() {
                        Utils.openSoftKeyboard(FreeTrialActivity.this, etSearch);
                    }
                });
            }
        });
        etSearch.requestFocus();
    }

    @Override
    public void onBackPressed() {
        if (getParent() != null)
            redirectTab(Constants.TAB_HOME);
        else
            super.onBackPressed();
    }

    @OnClick({R.id.tv_services, R.id.ll_send, R.id.tv_countries})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_countries:
                ccp.getCountryCodeHolderClickListener().onClick(tvCountries);
                break;
            case R.id.tv_services:
                List<ServicesModel.Data> mData = Preferences.getTopServices(this);
                if (mData != null && mData.size() > 0) {
                    showItemSelectDialog();
                } else {
                    getAllServices();
                }
                break;
            case R.id.ll_send:
                if (!isEmpty(getServiceName())) {
                    if (isValid()) {
                        addFreeTrial();
                    }
                }
                break;
        }
    }

    public void getAllServices() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<ServicesModel> call = getService().getServices();
        call.enqueue(new Callback<ServicesModel>() {
            @Override
            public void onResponse(Call<ServicesModel> call, Response<ServicesModel> response) {
                ServicesModel servicesModel = response.body();
                if (servicesModel != null && servicesModel.flag == 1) {
                    Preferences.saveServices(getApplicationContext(), servicesModel.data);
                }
                hideProgress();
                showItemSelectDialog();
            }

            @Override
            public void onFailure(Call<ServicesModel> call, Throwable t) {
                failureError("services load failed");
            }
        });
    }

    public void addFreeTrial() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().addFreeTrial(getName(), getPhone(), getWebsite(),
                getDescription(), getEmail(), getServiceName(), getCountry(), getJWT(), getUserID());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    showSuccessDialog();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("add free trial failed");
                hideProgress();
            }
        });
    }

    public void showSuccessDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_free_trial);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                resetView();
            }
        });
    }

    public void resetView() {
        etFullname.setText("");
        etEmail.setText("");
        etDescription.setText("");
        etPhone.setText("");
        tvCountries.setText("");
        etWebsite.setText("");
        tvServices.setText("");
        llSend.setAlpha(0.5f);
        chkTnc.setChecked(false);
    }
}
