package com.task24.ui.pro;

import android.os.Bundle;
import android.textview.TextViewSFTextBold;
import android.view.View;

import com.task24.R;
import com.task24.ui.BaseActivity;
import com.task24.ui.Pro24TaskDetailActivity;
import com.task24.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Pro24TaskActivity extends BaseActivity {

    @BindView(R.id.btn_24task_pro)
    TextViewSFTextBold btn24taskPro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pro_24task);
        ButterKnife.bind(this);

        btn24taskPro.setText(Utils.getColorString(this, getString(R.string.become_a_24task_pro), "PRO", R.color.red));
    }

    @OnClick({R.id.btn_24task_pro})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_24task_pro:
                redirectActivity(Pro24TaskDetailActivity.class);
                break;
        }
    }
}
