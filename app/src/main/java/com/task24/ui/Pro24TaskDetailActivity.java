package com.task24.ui;

import android.button.ButtonSFTextSemiBold;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ProgressBar;

import com.task24.R;
import com.task24.adapter.Pro24TaskPagerAdapter;
import com.task24.ui.workprofile.WorkProfileActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class Pro24TaskDetailActivity extends BaseActivity {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.btn_next)
    ButtonSFTextSemiBold btnNext;
    @BindView(R.id.btn_complete_profile)
    ButtonSFTextSemiBold btnCompleteProfile;

    private Pro24TaskPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pro24_task_detail);
        ButterKnife.bind(this);

        progress.setVisibility(View.GONE);

        adapter = new Pro24TaskPagerAdapter(this);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == adapter.getCount() - 1) {
                    btnNext.setVisibility(View.GONE);
                    btnCompleteProfile.setVisibility(View.VISIBLE);
                } else {
                    btnNext.setVisibility(View.VISIBLE);
                    btnCompleteProfile.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @OnClick({R.id.img_back, R.id.tv_cancel, R.id.btn_next, R.id.btn_complete_profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                if (viewPager.getCurrentItem() == 0) {
                    onBackPressed();
                } else {
                    int pos = viewPager.getCurrentItem();
                    viewPager.setCurrentItem(pos - 1);
                }
                break;
            case R.id.tv_cancel:
                onBackPressed();
                break;
            case R.id.btn_next:
                int pos = viewPager.getCurrentItem();
                viewPager.setCurrentItem(pos + 1);
                break;
            case R.id.btn_complete_profile:
                redirectActivity(WorkProfileActivity.class);
                finish();
                break;
        }
    }
}
