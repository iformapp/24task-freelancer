package com.task24.ui;

import android.os.Bundle;
import android.textview.TextViewSFDisplayBold;
import android.view.View;

import com.task24.R;
import com.task24.ui.auth.LoginSignUpActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectAccountActivity extends BaseActivity {

    @BindView(R.id.tv_hire)
    TextViewSFDisplayBold tvHire;
    @BindView(R.id.tv_work)
    TextViewSFDisplayBold tvWork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isLogin()) {
            redirectActivity(MainActivity.class);
            finish();
            return;
        }

        setContentView(R.layout.activity_select_account);
        ButterKnife.bind(this);

        tvHire.setText(Utils.getColorString(this, getString(R.string.want_to_hire), "Hire", R.color.colorPrimaryDark));
        tvWork.setText(Utils.getColorString(this, getString(R.string.want_to_work), "Work", R.color.red_dark));
    }

    @OnClick({R.id.rl_hire, R.id.rl_work, R.id.tv_privacy_policy, R.id.tv_terms_of_service})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_hire:
                Preferences.writeBoolean(this, Constants.IS_CLIENT_ACCOUNT, true);
                redirectActivity(LoginSignUpActivity.class);
                break;
            case R.id.rl_work:
                Preferences.writeBoolean(this, Constants.IS_CLIENT_ACCOUNT, false);
                redirectActivity(LoginSignUpActivity.class);
                break;
            case R.id.tv_privacy_policy:
                // TODO :: Change Url
                redirectUsingCustomTab(Constants.PRIVACY);
                break;
            case R.id.tv_terms_of_service:
                // TODO :: Change Url
                redirectUsingCustomTab(Constants.TERMS_USE);
                break;
        }
    }
}
