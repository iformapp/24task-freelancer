package com.task24.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.api.ApiClient;
import com.task24.api.ApiInterface;
import com.task24.model.GeneralModel;
import com.task24.model.Profile;
import com.task24.model.UserModel;
import com.task24.ui.auth.LoginSignUpActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v4.content.FileProvider.getUriForFile;

public class BaseActivity extends AppCompatActivity {

    private Dialog dialog;
    private OnProfileLoadListener onProfileLoadListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public ApiInterface getService() {
        return ApiClient.getClient().create(ApiInterface.class);
    }

    public void failureError(String message) {
        hideProgress();
        if (!isEmpty(message))
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void validationError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void addFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, fragment)
                .disallowAddToBackStack()
                .commit();
    }

    public void replaceFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(fragment.getClass().getName())
                .commit();
    }

    public boolean checkStatus(GeneralModel model) {
        if (model == null)
            return false;

        if (model.success != null) {
            switch (model.success) {
                case "1":
                    return true;
            }
        } else if (model.flag == 1) {
            return true;
        } else if (model.flag == 0) {
            if (model.msg != null && model.msg.equalsIgnoreCase("Expired Token.")) {
                boolean accountType = isClientAccount();
                Preferences.clearPreferences(this);
                Preferences.saveUserData(this, null);
                Preferences.setProfileData(this, null);
                Preferences.writeBoolean(this, Constants.IS_CLIENT_ACCOUNT, accountType);
                goToLoginSignup(true, true);
                return false;
            }
        }
        failureError(model.msg);
        return false;
    }

    public boolean checkStatus(String response) {
        if (isEmpty(response))
            return false;

        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = null;
            if (jsonObject.has("msg")) {
                msg = jsonObject.getString("msg");
            }
            if (jsonObject.has("success")) {
                String success = jsonObject.getString("success");
                if (!isEmpty(success)) {
                    switch (success) {
                        case "0":
                            if (msg != null && !isEmpty(msg)) {
                                failureError(msg);
                            }
                            return false;
                        case "1":
                        case "1.0":
                            return true;
                    }
                }
            } else if (jsonObject.has("flag")) {
                String flag = jsonObject.getString("flag");
                if (flag.equals("1") || flag.equals("1.0")) {
                    return true;
                } else if (flag.equals("0") || flag.equals("0.0")) {
                    if (msg != null && msg.equalsIgnoreCase("Expired Token.")) {
                        boolean accountType = isClientAccount();
                        Preferences.clearPreferences(this);
                        Preferences.saveUserData(this, null);
                        Preferences.setProfileData(this, null);
                        Preferences.writeBoolean(this, Constants.IS_CLIENT_ACCOUNT, accountType);
                        goToLoginSignup(true, true);
                        return false;
                    }
                }
            }
            failureError(msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isClientAccount() {
        return Preferences.readBoolean(this, Constants.IS_CLIENT_ACCOUNT, false);
    }

    public int getProfileTypeId() {
        return isClientAccount() ? Constants.CLIENT_PROFILE : Constants.AGENT_PROFILE;
    }

    public boolean isLogin() {
        return Preferences.readBoolean(this, Constants.IS_LOGIN, false);
    }

    public String getToken() {
        return Preferences.readString(this, Constants.FCM_TOKEN, "");
    }

    public String getJWT() {
        return Preferences.readString(this, Constants.JWT, "");
    }

    public int getUserID() {
        UserModel.Data userData = Preferences.getUserData(this);
        return userData != null ? userData.id : 0;
    }

    public String getDeviceType() {
        return "1"; // for Android
    }

    public String getTimeZone() {
        return TimeZone.getDefault().getID(); // for Android
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null && cm.getActiveNetworkInfo() != null) {
            return true;
        }
        Toast.makeText(this, "connect to internet", Toast.LENGTH_SHORT).show();
        return false;
    }

    public void gotoMainActivity(int screen) {
        if (getParent() != null) {
            ((MainActivity) getParent()).gotoMainActivity(screen);
        } else {
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra(Constants.SCREEN_NAME, screen);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
            finishToRight();
        }
    }

    public void goToLoginSignup(boolean isLogin, boolean isNeedToFinish) {
        Intent i = new Intent(this, LoginSignUpActivity.class);
        i.putExtra(Constants.FROM_LOGIN, isLogin);
        i.putExtra(Constants.LOGIN_FINISH, isNeedToFinish);
        startActivity(i);
        openToTop();
    }

    public void clearTopActivity(Class<?> activityClass) {
        if (getParent() != null) {
            ((MainActivity) getParent()).clearTopActivity(activityClass);
        } else {
            Intent i = new Intent(this, activityClass);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
            finishToRight();
        }
    }

    public void redirectTab(int tabIndex) {
        if (getParent() != null) {
            ((MainActivity) getParent()).getTabHost().setCurrentTab(tabIndex);
        }
    }

    public void redirectActivity(Class<?> activityClass) {
        if (getParent() != null) {
            ((MainActivity) getParent()).redirectActivity(activityClass);
        } else {
            startActivity(new Intent(this, activityClass));
            openToLeft();
        }
    }

    public void openToTop() {
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }

    public void openToLeft() {
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }

    public void finishToBottom() {
        overridePendingTransition(R.anim.stay, R.anim.slide_out_down);
    }

    public void finishToRight() {
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public void showProgress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_loading);
        dialog.setCancelable(false);
        RotateLoading rotateLoading = dialog.findViewById(R.id.rotateloading);
        rotateLoading.start();
        dialog.show();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public void hideProgress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public boolean isValidEmail(String target) {
        return (!isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public boolean isValidMobile(String phone) {
        if (phone.contains("+"))
            phone = phone.replace("+", "");
        return (!isEmpty(phone) && Double.parseDouble(phone) > 0 && Patterns.PHONE.matcher(phone).matches() && phone.length() > 6);
    }

    public boolean isValidUrl(String url) {
        return (!isEmpty(url) && Patterns.WEB_URL.matcher(url.toLowerCase()).matches());
    }

    public boolean isEmpty(String s) {
        return TextUtils.isEmpty(s);
    }

    public void redirectUsingCustomTab(String url) {
        try {
            Uri uri = Uri.parse(url);
            CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
            intentBuilder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            CustomTabsIntent customTabsIntent = intentBuilder.build();
            customTabsIntent.launchUrl(this, uri);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    public void showFeedbackDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_feedback);
        dialog.setCancelable(true);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvSend = dialog.findViewById(R.id.tv_send);
        final EditText etFeedback = dialog.findViewById(R.id.et_feedback);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEmpty(etFeedback.getText().toString().trim())) {
                    sendFeedback(etFeedback.getText().toString());
                    dialog.dismiss();
                } else {
                    Toast.makeText(BaseActivity.this, "Enter message", Toast.LENGTH_SHORT).show();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void sendFeedback(String message) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().addFeedback(getUserID(), message, getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        toastMessage(response.body().msg);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("add feedback failed");
            }
        });
    }

    public void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + getPackageName());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share via..."));
    }

    public void viewFile(File file) {
        try {
            Uri uri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                uri = getUriForFile(this, getPackageName() + ".provider", file);
            } else {
                uri = Uri.fromFile(file);
            }
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
            String mime;
            if (file.getPath().contains(".doc") || file.getPath().contains(".docx")) {
                mime = "application/msword";
            } else if(file.getPath().contains(".pdf")) {
                mime = "application/pdf";
            } else if(file.getPath().contains(".ppt") || file.getPath().contains(".pptx")) {
                mime = "application/vnd.ms-powerpoint";
            } else if(file.getPath().contains(".xls") || file.getPath().contains(".xlsx")) {
                mime = "application/vnd.ms-excel";
            } else if (file.getPath().contains(".jpg") || file.getPath().contains(".png") || file.getPath().contains(".jpeg")) {
                mime = "image/*";
            } else {
                mime = "*/*";
            }
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            String extension = mimeTypeMap.getFileExtensionFromUrl(uri.toString());
            if (mimeTypeMap.hasExtension(extension))
                mime = mimeTypeMap.getMimeTypeFromExtension(extension);
            intent.setDataAndType(uri, mime);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        } catch (Exception e) {
            toastMessage("No application available to view this type of file");
            e.printStackTrace();
        }
    }

    public void showContactUsDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_chat_now);
        dialog.setCancelable(true);
        View call = dialog.findViewById(R.id.rl_call);
        View messanger = dialog.findViewById(R.id.rl_messanger);
        View whatsapp = dialog.findViewById(R.id.rl_whatsapp);
        View email = dialog.findViewById(R.id.rl_email);
        View sms = dialog.findViewById(R.id.rl_sms);
        TextView tvCancel = dialog.findViewById(R.id.btn_cancel);

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(BaseActivity.this)
                        .withPermission(Manifest.permission.CALL_PHONE)
                        .withListener(new PermissionListener() {
                            @SuppressLint("MissingPermission")
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                                try {
                                    Intent intent = new Intent(Intent.ACTION_CALL,
                                            Uri.parse("tel:" + getString(R.string.phone_number)));
                                    startActivity(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(BaseActivity.this, "Failed to invoke call", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                Toast.makeText(BaseActivity.this, "Please Give Permission to make call", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();
            }
        });

        messanger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("m.me/24Task")));
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.messenger.com/t/24Task")));
                }
            }
        });

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String toNumber = getString(R.string.phone_number);
                    toNumber = toNumber.replace("+", "")
                            .replace("(", "")
                            .replace(")", "")
                            .replace("-", "")
                            .replace(" ", "");

                    Intent sendIntent = new Intent("android.intent.action.MAIN");
                    sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "");
                    sendIntent.putExtra("jid", toNumber + "@s.whatsapp.net");
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(BaseActivity.this, "Please install WhatsApp", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.whatsapp")));
                }
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", getString(R.string.email_text).toLowerCase(), null));
                    startActivity(Intent.createChooser(emailIntent, "Send email..."));
                } catch (Exception e) {
                    Toast.makeText(BaseActivity.this, "Mail apps not installed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", getString(R.string.phone_number), null)));
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(BaseActivity.this, "Inbox not found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public interface OnProfileLoadListener {
        public void onProfileLoad(Profile.Data data);
    }

    public void setOnProfileLoadListener(OnProfileLoadListener onProfileLoadListener) {
        this.onProfileLoadListener = onProfileLoadListener;
    }

    public void getProfile() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Profile> call = getService().getProfile(getUserID());
        call.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                Profile profile = response.body();
                if (profile != null && profile.data != null) {
                    Profile.Data data = profile.data;
                    Preferences.setProfileData(BaseActivity.this, data);
                    if (onProfileLoadListener != null) {
                        onProfileLoadListener.onProfileLoad(data);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                failureError("getProfile failed");
            }
        });
    }
}
