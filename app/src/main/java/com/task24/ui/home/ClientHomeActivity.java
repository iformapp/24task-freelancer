package com.task24.ui.home;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.task24.R;
import com.task24.adapter.HomeItemsAdapter;
import com.task24.adapter.HomeItemsAdapter.OnItemClick;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.EqualSpacingItemDecoration;
import com.task24.util.Preferences;
import com.task24.util.WrapContentHeightViewPager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class ClientHomeActivity extends BaseActivity {

    @BindView(R.id.viewpager)
    WrapContentHeightViewPager viewpager;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.rv_hire)
    RecyclerView rvHire;
    @BindView(R.id.rv_why_us)
    RecyclerView rvWhyUs;
    @BindView(R.id.rv_how_it_works)
    RecyclerView rvHowItWorks;

    private ArrayList<HomePagerModel> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_home);
        ButterKnife.bind(this);

        fillPagerData();

        HomePagerAdapter adapter = new HomePagerAdapter(this, arrayList);
        viewpager.setAdapter(adapter);
        indicator.setViewPager(viewpager);
        viewpager.setClipToPadding(false);
        viewpager.setPageMargin(12);

        rvHire.setLayoutManager(new GridLayoutManager(this, 2));
        rvWhyUs.setLayoutManager(new GridLayoutManager(this, 2));
        rvHowItWorks.setLayoutManager(new LinearLayoutManager(this));

        rvHire.addItemDecoration(new EqualSpacingItemDecoration(20));
        rvWhyUs.addItemDecoration(new EqualSpacingItemDecoration(20));
        rvHowItWorks.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));

        HomeItemsAdapter mHireAdapter = new HomeItemsAdapter(this, getList(Constants.HIRE), Constants.HIRE,
                new OnItemClick() {
                    @Override
                    public void onClickItem(HomePagerModel model) {
                        if (!model.title.equalsIgnoreCase("And More…")) {
                            Preferences.writeString(ClientHomeActivity.this, Constants.SERVICE_NAME, model.title);
                            redirectTab(Constants.TAB_FREE_TRIAL);
                        }
                    }
                });

        HomeItemsAdapter mWhyUsAdapter = new HomeItemsAdapter(this, getList(Constants.WHY_US), Constants.WHY_US,
                new OnItemClick() {
                    @Override
                    public void onClickItem(HomePagerModel model) {
                        //Toast.makeText(WorkHomeActivity.this, model.title, Toast.LENGTH_SHORT).show();
                    }
                });

        HomeItemsAdapter mHIWAdapter = new HomeItemsAdapter(this, getList(Constants.HOW_IT_WORKS), Constants.HOW_IT_WORKS,
                new OnItemClick() {
                    @Override
                    public void onClickItem(HomePagerModel model) {
                        //Toast.makeText(WorkHomeActivity.this, model.title, Toast.LENGTH_SHORT).show();
                    }
                });

        rvHire.setAdapter(mHireAdapter);
        rvWhyUs.setAdapter(mWhyUsAdapter);
        rvHowItWorks.setAdapter(mHIWAdapter);

        rvHire.setNestedScrollingEnabled(false);
        rvWhyUs.setNestedScrollingEnabled(false);
        rvHowItWorks.setNestedScrollingEnabled(false);
    }

    public void fillPagerData() {
        arrayList = new ArrayList<>();
        HomePagerModel model = new HomePagerModel();
        model.title = getString(R.string.hire_text);
        model.icon = R.drawable.support;
        model.details = "";
        arrayList.add(model);
        model = new HomePagerModel();
        model.title = getString(R.string.offer_text);
        model.icon = R.drawable.offer;
        model.details = getString(R.string.offer_details);
        arrayList.add(model);
        model = new HomePagerModel();
        model.title = getString(R.string.cheapest_text);
        model.icon = R.drawable.cheapest;
        model.details = getString(R.string.cheapest_details);
        arrayList.add(model);
        model = new HomePagerModel();
        model.title = getString(R.string.data_text);
        model.icon = R.drawable.data;
        model.details = getString(R.string.read_more);
        arrayList.add(model);
        model = new HomePagerModel();
        model.title = getString(R.string.support_text);
        model.icon = R.drawable.guarantee;
        model.details = getString(R.string.support_details);
        arrayList.add(model);
    }

    public ArrayList<HomePagerModel> getList(String type) {
        ArrayList<HomePagerModel> arrayList = new ArrayList<>();
        TypedArray imgs = null;
        String[] stringArray = new String[0];
        switch (type) {
            case Constants.HIRE:
                imgs = getResources().obtainTypedArray(R.array.hire_images);
                stringArray = getResources().getStringArray(R.array.hire);
                break;
            case Constants.WHY_US:
                imgs = getResources().obtainTypedArray(R.array.why_us_images);
                stringArray = getResources().getStringArray(R.array.why_us);
                break;
            case Constants.HOW_IT_WORKS:
                imgs = getResources().obtainTypedArray(R.array.how_it_works_images);
                stringArray = getResources().getStringArray(R.array.how_it_works);
                break;
        }

        for (int i = 0; i < stringArray.length; i++) {
            HomePagerModel model = new HomePagerModel();
            model.title = stringArray[i];
            model.icon = imgs.getResourceId(i, -1);
            arrayList.add(model);
        }
        return arrayList;
    }

    @OnClick({R.id.btn_pricing, R.id.btn_free_trial})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_pricing:
                redirectTab(Constants.TAB_CALCULATE);
                break;
            case R.id.btn_free_trial:
                redirectTab(Constants.TAB_FREE_TRIAL);
                break;
        }
    }
}
