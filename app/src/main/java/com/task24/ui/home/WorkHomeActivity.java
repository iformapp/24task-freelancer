package com.task24.ui.home;

import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.textview.TextViewSFTextBold;
import android.view.View;

import com.task24.R;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.Utils;
import com.task24.util.WrapContentHeightViewPager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class WorkHomeActivity extends BaseActivity {

    @BindView(R.id.viewpager)
    WrapContentHeightViewPager viewpager;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.btn_24task_pro)
    TextViewSFTextBold btn24taskPro;

    private ArrayList<HomePagerModel> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_home);
        ButterKnife.bind(this);

        btn24taskPro.setText(Utils.getColorString(this, getString(R.string.become_a_24task_pro), "PRO", R.color.red));

        fillPagerData();

        HomePagerAdapter adapter = new HomePagerAdapter(this, arrayList);
        viewpager.setAdapter(adapter);
        indicator.setViewPager(viewpager);
        viewpager.setClipToPadding(false);
        viewpager.setPageMargin(12);

        getProfile();
    }

    public void fillPagerData() {
        arrayList = new ArrayList<>();
        HomePagerModel model = new HomePagerModel();
        model.title = getString(R.string.job_text);
        model.icon = R.drawable.support;
        model.details = "";
        arrayList.add(model);
//        model = new HomePagerModel();
//        model.title = getString(R.string.offer_text);
//        model.icon = R.drawable.offer;
//        model.details = getString(R.string.offer_details);
//        arrayList.add(model);
        model = new HomePagerModel();
        model.title = getString(R.string.cheapest_text);
        model.icon = R.drawable.cheapest;
        model.details = getString(R.string.cheapest_details);
        arrayList.add(model);
        model = new HomePagerModel();
        model.title = getString(R.string.data_text);
        model.icon = R.drawable.data;
        model.details = getString(R.string.read_more);
        arrayList.add(model);
        model = new HomePagerModel();
        model.title = getString(R.string.support_text);
        model.icon = R.drawable.guarantee;
        model.details = getString(R.string.support_details);
        arrayList.add(model);
    }

    @OnClick({R.id.btn_24task_pro})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_24task_pro:
                redirectTab(Constants.TAB_TASK_PRO);
                break;
        }
    }
}
