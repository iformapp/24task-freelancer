package com.task24.ui.home;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.task24.R;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;

import java.util.ArrayList;

import io.intercom.android.sdk.Intercom;

public class HomePagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<HomePagerModel> arrayList;

    public HomePagerAdapter(Context mContext, ArrayList<HomePagerModel> arrayList) {
        this.mContext = mContext;
        this.arrayList = arrayList;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup v = (ViewGroup) inflater.inflate(R.layout.home_page_item, collection, false);

        ImageView imgIcon = v.findViewById(R.id.img_icon);
        TextView tvTitle = v.findViewById(R.id.tv_title);
        final TextView tvDetails = v.findViewById(R.id.tv_details);

        HomePagerModel items = arrayList.get(position);
        imgIcon.setImageDrawable(ContextCompat.getDrawable(mContext, items.icon));
        tvTitle.setText(items.title);
        tvDetails.setText(items.details);
        tvDetails.setPaintFlags(tvDetails.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        if (position == 0) {
            tvDetails.setVisibility(View.GONE);
        } else {
            tvDetails.setVisibility(View.VISIBLE);
        }

        tvDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvDetails.getText().toString().equalsIgnoreCase(mContext.getString(R.string.offer_details))) {
                    ((BaseActivity) mContext).redirectTab(Constants.TAB_FREE_TRIAL);
                } else if (tvDetails.getText().toString().equalsIgnoreCase(mContext.getString(R.string.chat_now))) {
                    Intercom.client().displayMessageComposer();
                } else if (tvDetails.getText().toString().equalsIgnoreCase(mContext.getString(R.string.read_more))) {
                    ((BaseActivity) mContext).redirectUsingCustomTab(Constants.PRIVACY);
                }
            }
        });
        collection.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public float getPageWidth(int position) {
        return 0.93f;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }
}
