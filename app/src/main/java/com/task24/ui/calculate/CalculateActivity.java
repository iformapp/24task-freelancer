package com.task24.ui.calculate;

import android.app.Dialog;
import android.edittext.EditTextSFDisplayRegular;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.textview.TextViewSFTextBold;
import android.textview.TextViewSFTextRegular;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.adapter.PricingAdapter;
import com.task24.adapter.ServicesAdapter;
import com.task24.api.ApiClient;
import com.task24.api.ApiInterface;
import com.task24.model.GetPrice;
import com.task24.model.ServicesModel;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.EqualSpacingItemDecoration;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalculateActivity extends BaseActivity {

    @BindView(R.id.tv_services)
    TextViewSFTextRegular tvServices;
    @BindView(R.id.rv_pricing)
    RecyclerView rvPricing;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.card_note)
    CardView cardNote;
    @BindView(R.id.tv_price)
    TextViewSFTextBold tvPrice;
    @BindView(R.id.ll_free_trial)
    LinearLayout llFreeTrial;
    @BindView(R.id.et_hours)
    EditTextSFDisplayRegular etHours;

    private ServicesAdapter serviceAdapter;
    private PricingAdapter mAdapter;
    ArrayList<String> pricingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate);
        ButterKnife.bind(this);

        rvPricing.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvPricing.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.HORIZONTAL));
        rvPricing.setNestedScrollingEnabled(false);

        List<ServicesModel.Data> mData = Preferences.getTopServices(this);
        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i).name.equals(getString(R.string.customer_service_managers))) {
                setServiceUi(mData.get(i).id);
            }
        }

        etHours.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!isEmpty(s.toString()) && Integer.parseInt(s.toString()) > 730) {
                    etHours.setText("730");
                    Toast.makeText(CalculateActivity.this, "Enter hours only upto 730", Toast.LENGTH_LONG).show();
                    return;
                }
                //getPriceCalculation();
                setPriceCalculation();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etHours.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etHours.setFocusable(true);
                etHours.setCursorVisible(true);
                etHours.setSelection(getHours().length());
                return false;
            }
        });
    }

    private void setAdapter(ArrayList<String> pricingList) {
        if (mAdapter == null) {
            mAdapter = new PricingAdapter(this);
        }

        mAdapter.doRefresh(pricingList);

        if (rvPricing.getAdapter() == null) {
            rvPricing.setAdapter(mAdapter);
        }
    }

    public String getHours() {
        return etHours.getText().toString();
    }

    public String getServiceName() {
        return tvServices.getText().toString();
    }

    public void getPriceCalculation() {
        if (!isNetworkConnected())
            return;

        Call<GetPrice> call = getService().getPriceCalculation(isEmpty(getHours()) ? "0" : getHours());
        call.enqueue(new Callback<GetPrice>() {
            @Override
            public void onResponse(Call<GetPrice> call, Response<GetPrice> response) {
                GetPrice getPrice = response.body();
                if (checkStatus(getPrice)) {
                    tvPrice.setText("$" + getPrice.data);
                }
            }

            @Override
            public void onFailure(Call<GetPrice> call, Throwable t) {
                failureError("get price failed");
            }
        });
    }

    private void setPriceCalculation() {
        int hour = isEmpty(getHours()) ? 0 : Integer.parseInt(getHours());
        double price;
        if (hour < 90) {
            price = Double.parseDouble(pricingList.get(2)) * hour;
        } else if (hour < 150) {
            price = Double.parseDouble(pricingList.get(1)) * hour;
        } else {
            price = Double.parseDouble(pricingList.get(0)) * hour;
        }
        tvPrice.setText("$" + Utils.numberFormat(price, 2));
    }

    @OnClick({R.id.img_minus, R.id.img_plus, R.id.img_next, R.id.ll_free_trial, R.id.tv_services})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(this);
        etHours.setCursorVisible(false);
        switch (view.getId()) {
            case R.id.img_minus:
                if (!isEmpty(getHours())) {
                    int page = Integer.parseInt(getHours());
                    if (page != 0) {
                        etHours.setText(String.valueOf(page - 1));
                    }
                } else {
                    etHours.setText("1");
                }
                break;
            case R.id.img_plus:
                if (!isEmpty(getHours())) {
                    int page = Integer.parseInt(getHours());
                    etHours.setText(String.valueOf(page + 1));
                } else {
                    etHours.setText("1");
                }
                break;
            case R.id.ll_free_trial:
                Preferences.writeString(this, Constants.SERVICE_NAME, getServiceName());
                redirectTab(Constants.TAB_FREE_TRIAL);
                break;
            case R.id.tv_services:
                List<ServicesModel.Data> mData = Preferences.getTopServices(this);
                if (mData != null && mData.size() > 0) {
                    showItemSelectDialog();
                } else {
                    getAllServices();
                }
                break;
            case R.id.img_next:
                scrollView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.smoothScrollTo(0, cardNote.getBottom());
                    }
                }, 100);
                break;
        }
    }

    public void getAllServices() {
        if (!isNetworkConnected())
            return;

        showProgress();

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ServicesModel> call = apiInterface.getServices();
        call.enqueue(new Callback<ServicesModel>() {
            @Override
            public void onResponse(Call<ServicesModel> call, Response<ServicesModel> response) {
                ServicesModel servicesModel = response.body();
                if (servicesModel != null && servicesModel.flag == 1) {
                    Preferences.saveServices(getApplicationContext(), servicesModel.data);
                }
                hideProgress();
                showItemSelectDialog();
            }

            @Override
            public void onFailure(Call<ServicesModel> call, Throwable t) {
                failureError("services load failed");
            }
        });
    }

    public void showItemSelectDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select_black);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        final EditText etSearch = dialog.findViewById(R.id.et_search);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.services).toLowerCase()));

        rvTypes.setLayoutManager(new LinearLayoutManager(this));
        List<ServicesModel.Data> mData = Preferences.getTopServices(this);
        if (mData != null && mData.size() > 0) {
            for (int i = 0; i < mData.size(); i++) {
                if (mData.get(i).name.equalsIgnoreCase(getServiceName())) {
                    mData.get(i).isSelected = true;
                }
            }
            serviceAdapter = new ServicesAdapter(this, mData);
            rvTypes.setAdapter(serviceAdapter);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(CalculateActivity.this);
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(CalculateActivity.this);
                if (serviceAdapter != null && serviceAdapter.getSelectedItem() != null) {
                    tvServices.setText(serviceAdapter.getSelectedItem().name);
                    setServiceUi(serviceAdapter.getSelectedItem().id);
                    dialog.dismiss();
                } else {
                    Toast.makeText(CalculateActivity.this, "Please select service", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (serviceAdapter != null)
                    serviceAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
//        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                etSearch.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        Utils.openSoftKeyboard(CalculateActivity.this, etSearch);
//                    }
//                });
//            }
//        });
//        etSearch.requestFocus();
    }

    private void setServiceUi(int id) {
        String[] pricingArray = getResources().getStringArray((id >= 5) ? R.array.high_pricing_array : R.array.low_pricing_array);
        pricingList = new ArrayList<>(Arrays.asList(pricingArray));

        setAdapter(pricingList);
        setPriceCalculation();
    }

    @Override
    public void onBackPressed() {
        if (getParent() != null)
            redirectTab(Constants.TAB_HOME);
        else
            super.onBackPressed();
    }
}
