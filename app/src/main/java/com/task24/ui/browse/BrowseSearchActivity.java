package com.task24.ui.browse;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.adapter.RecommendedFreelancerAdapter;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.fragment.postjob.ChooseSkillsFragment;
import com.task24.model.Projects;
import com.task24.model.Recommended;
import com.task24.model.ServicesModel;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BrowseSearchActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.rv_freelancers)
    RecyclerView rvFreelancers;
    @BindView(R.id.rv_categories)
    RecyclerView rvCategories;

    private List<ServicesModel.Data> servicesList;
    private RecyclerviewAdapter mAdapter;
    private RecommendedFreelancerAdapter mFreelancerAdapter;
    private ArrayList<Recommended> recommendedList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_search);
        ButterKnife.bind(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvFreelancers.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvFreelancers.getContext(),
                linearLayoutManager.getOrientation());
        rvFreelancers.addItemDecoration(dividerItemDecoration);

        rvCategories.setLayoutManager(new LinearLayoutManager(this));

        fillArray();
        setFreelancerAdapter();

        servicesList = Preferences.getTopServices(this);
        setCategoryAdapter();
    }

    @OnClick({R.id.tv_show_more})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_show_more:
                break;
        }
    }

    private void setFreelancerAdapter() {
        if (recommendedList != null && recommendedList.size() > 0) {
            if (mFreelancerAdapter == null) {
                mFreelancerAdapter = new RecommendedFreelancerAdapter(this);
            }
            mFreelancerAdapter.doRefresh(recommendedList);
            if (rvFreelancers.getAdapter() == null) {
                rvFreelancers.setAdapter(mFreelancerAdapter);
            }
        }
    }

    private void setCategoryAdapter() {
        if (servicesList != null && servicesList.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new RecyclerviewAdapter((ArrayList<?>) servicesList, R.layout.item_category_list, this);
            }
            mAdapter.doRefresh((ArrayList<?>) servicesList);
            if (rvCategories.getAdapter() == null) {
                rvCategories.setAdapter(mAdapter);
            }
        }
    }

    private void fillArray() {
        recommendedList = new ArrayList<>();
        recommendedList.add(new Recommended("Don John"));
        recommendedList.add(new Recommended("Don John"));
        recommendedList.add(new Recommended("Don John"));
    }

    @Override
    public void bindView(View view, int position) {
        final ServicesModel.Data service = servicesList.get(position);
        TextView tvService = view.findViewById(R.id.tv_category);
        tvService.setText(service.name);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(BrowseSearchActivity.this, service.name, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
