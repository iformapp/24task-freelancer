package com.task24.ui.chat;

import android.os.Bundle;
import android.view.View;

import com.task24.R;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;

public class ChatActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.img_chat, R.id.btn_chat_here})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_chat:
                showContactUsDialog();
                break;
            case R.id.btn_chat_here:
                Intercom.client().displayMessageComposer();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (getParent() != null)
            redirectTab(Constants.TAB_HOME);
        else
            super.onBackPressed();
    }
}
