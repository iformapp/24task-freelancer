package com.task24.ui.projects;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.textview.TextViewSFDisplayRegular;
import android.textview.TextViewSFTextMedium;
import android.view.View;
import android.widget.ImageView;

import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;
import com.task24.R;
import com.task24.model.SkillsById;
import com.task24.ui.BaseActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProjectDetailsActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_close_project)
    TextViewSFDisplayRegular tvCloseProject;
    @BindView(R.id.tv_project_title)
    TextViewSFTextMedium tvProjectTitle;
    @BindView(R.id.tv_daysleft)
    TextViewSFDisplayRegular tvDaysleft;
    @BindView(R.id.tv_description)
    TextViewSFTextMedium tvDescription;
    @BindView(R.id.tv_project_budget)
    TextViewSFTextMedium tvProjectBudget;
    @BindView(R.id.tv_avg_bid)
    TextViewSFTextMedium tvAvgBid;
    @BindView(R.id.tv_bids)
    TextViewSFTextMedium tvBids;
    @BindView(R.id.skills_tag)
    TagView skillsTag;
    @BindView(R.id.tv_project_id)
    TextViewSFTextMedium tvProjectId;

    private ArrayList<String> skillsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_details);
        ButterKnife.bind(this);

        fillSkill();

        setTagsData();
    }

    private void setTagsData() {
        ArrayList<Tag> tags = new ArrayList<>();
        Tag tag;
        skillsTag.removeAll();
        if (skillsList != null && skillsList.size() > 0) {
            for (int i = 0; i < skillsList.size(); i++) {
                String skill = skillsList.get(i);
                tag = new Tag(skill);
                tag.radius = 10f;
                tag.layoutColor = ContextCompat.getColor(this, R.color.white);
                tag.tagTextColor = ContextCompat.getColor(this, R.color.colorPrimary);
                tag.layoutBorderColor = ContextCompat.getColor(this, R.color.colorPrimary);
                tag.layoutBorderSize = 1f;
                tag.isDeletable = false;
                tags.add(tag);
            }
            skillsTag.addTags(tags);
        }
    }

    private void fillSkill() {
        skillsList = new ArrayList<>();
        skillsList.add("PHP");
        skillsList.add("Website Design");
        skillsList.add("Android");
        skillsList.add("iOS");
    }

    @OnClick({R.id.img_back, R.id.tv_close_project})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_close_project:
                break;
        }
    }
}
