package com.task24.ui.projects;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.textview.TextViewSFDisplayBold;
import android.textview.TextViewSFDisplayRegular;
import android.textview.TextViewSFTextMedium;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.adapter.ProposalsAdapter;
import com.task24.ui.BaseActivity;
import com.task24.ui.workprofile.AvailableForWorkActivity;
import com.task24.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import okhttp3.internal.Util;

public class ProjectCycleActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_title)
    TextViewSFDisplayRegular tvTitle;
    @BindView(R.id.tv_close_project)
    TextViewSFDisplayRegular tvCloseProject;
    @BindView(R.id.img_complete1)
    ImageView imgComplete1;
    @BindView(R.id.details_view_down)
    View detailsViewDown;
    @BindView(R.id.tv_project_title)
    TextViewSFTextMedium tvProjectTitle;
    @BindView(R.id.tv_daysleft)
    TextViewSFDisplayRegular tvDaysleft;
    @BindView(R.id.tv_description)
    TextViewSFTextMedium tvDescription;
    @BindView(R.id.ll_details_info)
    LinearLayout llDetailsInfo;
    @BindView(R.id.ll_project_details)
    LinearLayout llProjectDetails;
    @BindView(R.id.proposal_view_up)
    View proposalViewUp;
    @BindView(R.id.img_complete2)
    ImageView imgComplete2;
    @BindView(R.id.proposal_view_down)
    View proposalViewDown;
    @BindView(R.id.rv_proposals)
    RecyclerView rvProposals;
    @BindView(R.id.ll_proposals_info)
    LinearLayout llProposalsInfo;
    @BindView(R.id.ll_view_proposals)
    LinearLayout llViewProposals;
    @BindView(R.id.management_view_up)
    View managementViewUp;
    @BindView(R.id.img_complete3)
    ImageView imgComplete3;
    @BindView(R.id.management_view_down)
    View managementViewDown;
    @BindView(R.id.ll_management_info)
    LinearLayout llManagementInfo;
    @BindView(R.id.ll_management)
    LinearLayout llManagement;
    @BindView(R.id.review_view_up)
    View reviewViewUp;
    @BindView(R.id.img_complete4)
    ImageView imgComplete4;
    @BindView(R.id.review_view_down)
    View reviewViewDown;
    @BindView(R.id.ll_review)
    LinearLayout llReview;
    @BindView(R.id.congrats_view_up)
    View congratsViewUp;
    @BindView(R.id.img_complete5)
    ImageView imgComplete5;
    @BindView(R.id.ll_congrats)
    LinearLayout llCongrats;
    @BindView(R.id.img_arrow_details)
    ImageView imgArrowDetails;
    @BindView(R.id.img_arrow_proposals)
    ImageView imgArrowProposals;
    @BindView(R.id.img_arrow_management)
    ImageView imgArrowManagement;
    @BindView(R.id.img_user)
    ImageView imgUser;
    @BindView(R.id.tv_name)
    TextViewSFDisplayBold tvName;
    @BindView(R.id.tv_bid_price)
    TextViewSFDisplayRegular tvBidPrice;
    @BindView(R.id.tv_manage_project)
    TextViewSFTextMedium tvManageProject;

    private ProposalsAdapter proposalsAdapter;
    private ArrayList<String> proposalList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_cycle);
        ButterKnife.bind(this);

        rvProposals.setLayoutManager(new LinearLayoutManager(this));

        proposalList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            proposalList.add(i + "");
        }
        setProposalAdapter();

        ClickableSpan termsOfUseClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                redirectActivity(ProjectDetailsActivity.class);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ContextCompat.getColor(ProjectCycleActivity.this, R.color.colorPrimary));
            }
        };

        Utils.makeLinks(tvDescription, new String[]{"Read more"}, new ClickableSpan[]{termsOfUseClick});
    }

    private void setProposalAdapter() {
        if (proposalList != null && proposalList.size() > 0) {
            if (proposalsAdapter == null) {
                proposalsAdapter = new ProposalsAdapter(this);
            }
            proposalsAdapter.doRefresh(proposalList);
            if (rvProposals.getAdapter() == null) {
                rvProposals.setAdapter(proposalsAdapter);
            }
        }
    }

    @OnClick({R.id.img_back, R.id.tv_close_project, R.id.ll_project_details, R.id.ll_view_proposals, R.id.ll_management,
            R.id.ll_review, R.id.ll_congrats, R.id.tv_manage_project})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_close_project:
                showCloseProjectDialog();
                break;
            case R.id.ll_project_details:
                if (llDetailsInfo.getVisibility() == View.GONE) {
                    llDetailsInfo.setVisibility(View.VISIBLE);
                    imgArrowDetails.setRotation(270);
                } else {
                    llDetailsInfo.setVisibility(View.GONE);
                    imgArrowDetails.setRotation(0);
                }
                break;
            case R.id.ll_view_proposals:
                if (llProposalsInfo.getVisibility() == View.GONE) {
                    llProposalsInfo.setVisibility(View.VISIBLE);
                    imgArrowProposals.setRotation(270);
                } else {
                    llProposalsInfo.setVisibility(View.GONE);
                    imgArrowProposals.setRotation(0);
                }
                break;
            case R.id.ll_management:
                if (llManagementInfo.getVisibility() == View.GONE) {
                    llManagementInfo.setVisibility(View.VISIBLE);
                    imgArrowManagement.setRotation(270);
                } else {
                    llManagementInfo.setVisibility(View.GONE);
                    imgArrowManagement.setRotation(0);
                }
                break;
            case R.id.ll_review:
                break;
            case R.id.ll_congrats:
                redirectActivity(CongratsActivity.class);
                break;
            case R.id.tv_manage_project:
                break;
        }
    }

    public void showAwardedDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_awarded);
        dialog.setCancelable(true);

        TextView tvAwardName = dialog.findViewById(R.id.tv_awarded_name);
        TextView tvRequestMilestone = dialog.findViewById(R.id.tv_request_milestone);
        TextView tvMilestoneAmount = dialog.findViewById(R.id.tv_milestone_amount);
        TextView tvTotalAmount = dialog.findViewById(R.id.tv_total_amount);
        TextView tvAcceptProject = dialog.findViewById(R.id.tv_accept_project);
        TextView tvSatisfied = dialog.findViewById(R.id.tv_satisfied);
        TextView tvTnc = dialog.findViewById(R.id.tv_tnc);
        TextView tvPay = dialog.findViewById(R.id.tv_pay);
        ImageView imgClose = dialog.findViewById(R.id.img_close);

        ClickableSpan tncClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.setColor(ContextCompat.getColor(ProjectCycleActivity.this, R.color.colorPrimary));
            }
        };

        Utils.makeLinks(tvTnc, new String[]{"Terms and Conditions"}, new ClickableSpan[]{tncClick});

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.TOP;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    private void showCloseProjectDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_close_project);
        dialog.setCancelable(true);

        TextView tvMessage = dialog.findViewById(R.id.tv_message);
        TextView tvNo = dialog.findViewById(R.id.tv_no);
        TextView tvYes = dialog.findViewById(R.id.tv_yes);

        tvMessage.setText(Utils.fromHtml(getString(R.string.close_this_project)));
        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }
}
