package com.task24.ui.projects;

import android.os.Bundle;

import com.task24.R;
import com.task24.ui.BaseActivity;

public class CongratsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congrats);
    }
}
