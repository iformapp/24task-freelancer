package com.task24.ui.projects;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.task24.R;
import com.task24.fragment.projects.ProjectsListFragment;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyProjectsActivity extends BaseActivity {

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    private String[] tabTexts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_projects);
        ButterKnife.bind(this);

        tabTexts = new String[]{Constants.WORK_IN_PROGRESS, Constants.PAST_PROJECTS};
        setupTabs();
    }

    public void setupTabs() {
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFDISPLAY_BOLD);
                if (text != null) {
                    text.setTypeface(tf);
                    text.setTextColor(Color.BLACK);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFDISPLAY_REGULAR);
                if (text != null) {
                    text.setTypeface(tf);
                    text.setTextColor(ContextCompat.getColor(MyProjectsActivity.this, R.color.textgrayAccent));
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setupTabText();
    }

    private void setupTabText() {
        for (int i = 0; i < tabTexts.length; i++) {
            TextView tv = (TextView) LayoutInflater.from(this)
                    .inflate(i == 0 ? R.layout.custom_tab_text_select : R.layout.custom_tab_text_unselect, null);
            tv.setText(tabTexts[i]);
            tabLayout.getTabAt(i).setCustomView(tv);
        }

        viewPager.setOffscreenPageLimit(tabLayout.getTabCount() - 1);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (String tabText : tabTexts) {
            adapter.addFrag(ProjectsListFragment.newInstance(tabText.equals(Constants.WORK_IN_PROGRESS)), tabText);
        }
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}
