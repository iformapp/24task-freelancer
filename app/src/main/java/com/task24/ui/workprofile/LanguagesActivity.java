package com.task24.ui.workprofile;

import android.app.Dialog;
import android.button.ButtonSFTextBold;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.adapter.SelectItemAdapter;
import com.task24.model.GeneralModel;
import com.task24.model.Language;
import com.task24.model.Profile;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LanguagesActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.rv_languages)
    RecyclerView rvLanguages;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.rl_edit)
    RelativeLayout rlEdit;
    @BindView(R.id.btn_next)
    ButtonSFTextBold btnNext;

    private ArrayList<Language.Data> languageList;
    private ArrayList<Language.Data> languagesArray;
    private RecyclerviewAdapter mAdapter;
    private boolean isEdit = false;
    private SelectItemAdapter itemAdapter;
    private Profile.Data profileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_languages);
        ButterKnife.bind(this);

        languagesArray = new ArrayList<>();
        languageList = new ArrayList<>();

        if (getIntent() != null) {
            isEdit = getIntent().getBooleanExtra(Constants.IS_EDIT, false);
        }
        if (!isEdit)
            languageList.add(new Language.Data());

        profileData = Preferences.getProfileData(this);
        progress.setProgress(80);
        rvLanguages.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RecyclerviewAdapter(languageList, R.layout.item_language_edit, this);
        rvLanguages.setAdapter(mAdapter);

        if (isEdit) {
            rlEdit.setVisibility(View.VISIBLE);
            header.setVisibility(View.GONE);
            btnNext.setVisibility(View.GONE);

            if (profileData != null && profileData.profileLanguages != null && profileData.profileLanguages.size() > 0) {
                for (int i = 0; i < profileData.profileLanguages.size(); i++) {
                    Profile.ProfileLanguages languages = profileData.profileLanguages.get(i);
                    if (languages != null) {
                        Language.Data model = new Language.Data();
                        model.id = languages.languageId;
                        model.name = languages.language.name;
                        model.level = Utils.getLanguageLevel(languages.level);
                        model.levelId = languages.level;
                        languageList.add(model);
                    }
                }
            } else {
                languageList.add(new Language.Data());
            }
            mAdapter.doRefresh(languageList);
        }

        getLanguageList();
    }

    @OnClick({R.id.img_back, R.id.tv_cancel, R.id.tv_add_languages, R.id.btn_next, R.id.tv_edit_cancel, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_cancel:
                gotoMainActivity(Constants.TAB_HOME);
                break;
            case R.id.tv_add_languages:
                languageList.add(new Language.Data());
                mAdapter.doRefresh(languageList);
                break;
            case R.id.tv_save:
            case R.id.btn_next:
                if (isValid()) {
                    StringBuilder languageIds = null;
                    StringBuilder levelsIds = null;
                    for (int i = 0; i < languageList.size(); i++) {
                        languageIds = (languageIds == null ? new StringBuilder("") : languageIds.append(",")).append(languageList.get(i).id);
                        levelsIds = (levelsIds == null ? new StringBuilder("") : levelsIds.append(",")).append(languageList.get(i).levelId);
                    }

                    String languageId = languageIds == null ? "" : languageIds.toString();
                    String levelId = levelsIds == null ? "" : levelsIds.toString();
                    addLanguage(languageId, levelId);
                }
                break;
            case R.id.tv_edit_cancel:
                onBackPressed();
                break;
        }
    }

    public void addLanguage(String languagesId, String levelId) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().addLanguages(getUserID(), languagesId, levelId, getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    if (isEdit) {
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        redirectActivity(PayRateActivity.class);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("add languages failed");
            }
        });
    }

    @Override
    public void bindView(View view, final int position) {
        final TextView tvLanguage = view.findViewById(R.id.tv_language);
        final TextView tvLevel = view.findViewById(R.id.tv_level);
        ImageView imgDelete = view.findViewById(R.id.img_delete);

        if (languageList.get(position) != null) {
            tvLanguage.setText(languageList.get(position).name);
            tvLevel.setText(languageList.get(position).level);
        }

        if (position == 0) {
            imgDelete.setVisibility(View.GONE);
        } else {
            imgDelete.setVisibility(View.VISIBLE);
        }

        tvLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (languagesArray != null) {
                    showLanguageSelectDialog(languagesArray, tvLanguage, languageList.get(position), true);
                }
            }
        });

        tvLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isEmpty(languageList.get(position).name)) {
                    showLanguageSelectDialog(getLevelList(), tvLevel, languageList.get(position), false);
                }
            }
        });

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                languageList.remove(position);
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, languageList.size());
            }
        });
    }

    public void showLanguageSelectDialog(ArrayList<Language.Data> arrayList, final TextView textView,
                                         final Language.Data language, final boolean isLanguage) {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select_black);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        final EditText etSearch = dialog.findViewById(R.id.et_search);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        if (isLanguage) {
            etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.language).toLowerCase()));
        } else {
            etSearch.setVisibility(View.GONE);
            etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.level).toLowerCase()));
        }

        rvTypes.setLayoutManager(new LinearLayoutManager(this));
        if (arrayList != null && arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (isLanguage) {
                    arrayList.get(i).isSelected = arrayList.get(i).name.equalsIgnoreCase(textView.getText().toString());
                } else {
                    arrayList.get(i).isSelected = arrayList.get(i).level.equalsIgnoreCase(textView.getText().toString());
                }
            }
            itemAdapter = new SelectItemAdapter(this, arrayList, isLanguage);
            rvTypes.setAdapter(itemAdapter);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemAdapter != null && itemAdapter.getSelectedItem() != null) {
                    if (isLanguage) {
                        textView.setText(itemAdapter.getSelectedItem().name);
                        language.name = itemAdapter.getSelectedItem().name;
                        language.id = itemAdapter.getSelectedItem().id;
                    } else {
                        textView.setText(itemAdapter.getSelectedItem().level);
                        language.level = itemAdapter.getSelectedItem().level;
                        language.levelId = itemAdapter.getSelectedItem().levelId;
                    }
                    dialog.dismiss();
                } else {
                    Toast.makeText(LanguagesActivity.this, "Please select one item", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (itemAdapter != null)
                    itemAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etSearch.post(new Runnable() {
                    @Override
                    public void run() {
                        Utils.openSoftKeyboard(LanguagesActivity.this, etSearch);
                    }
                });
            }
        });
        etSearch.requestFocus();
    }

    private ArrayList<Language.Data> getLevelList() {
        ArrayList<Language.Data> arrayList = new ArrayList<>();
        Language.Data model = new Language.Data();
        model.level = "Basic";
        model.levelId = 0;
        arrayList.add(model);
        model = new Language.Data();
        model.level = "Conversational";
        model.levelId = 1;
        arrayList.add(model);
        model = new Language.Data();
        model.level = "Fluent";
        model.levelId = 2;
        arrayList.add(model);
        model = new Language.Data();
        model.level = "Native";
        model.levelId = 3;
        arrayList.add(model);
        return arrayList;
    }

    public void getLanguageList() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Language> call = getService().getLanguages();
        call.enqueue(new Callback<Language>() {
            @Override
            public void onResponse(Call<Language> call, Response<Language> response) {
                Language language = response.body();
                if (language != null) {
                    if (checkStatus(language)) {
                        languagesArray = (ArrayList<Language.Data>) language.data;
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Language> call, Throwable t) {
                failureError("get language failed");
            }
        });
    }

    private boolean isValid() {
        if (languageList != null && languageList.size() > 0) {
            for (int i = 0; i < languageList.size(); i++) {
                if (languageList.get(i) == null) {
                    validationError("Please select language");
                    return false;
                }

                if (isEmpty(languageList.get(i).name)) {
                    validationError("Please select language");
                    return false;
                } else if (isEmpty(languageList.get(i).level)) {
                    validationError("Please select level");
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
