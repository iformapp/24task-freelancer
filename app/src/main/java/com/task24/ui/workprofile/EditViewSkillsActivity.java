package com.task24.ui.workprofile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.task24.R;
import com.task24.adapter.SkillsAdapter;
import com.task24.model.Profile;
import com.task24.model.Skill;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.EqualSpacingItemDecoration;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditViewSkillsActivity extends BaseActivity implements BaseActivity.OnProfileLoadListener {

    @BindView(R.id.rv_languages)
    RecyclerView rvLanguages;
    @BindView(R.id.rv_expertise)
    RecyclerView rvExpertise;
    @BindView(R.id.rv_skills)
    RecyclerView rvSkills;

    private ArrayList<Skill> languageList;
    private ArrayList<Skill> expertiseList;
    private ArrayList<Skill> skillList;
    private Profile.Data profileData;
    private static final int REQ_EDIT_LANGUAGE = 101;
    private static final int REQ_EDIT_EXPERTISE = 102;
    private static final int REQ_EDIT_SKILL = 103;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_view_skills);
        ButterKnife.bind(this);

        rvLanguages.setLayoutManager(new LinearLayoutManager(this));
        rvExpertise.setLayoutManager(new LinearLayoutManager(this));
        rvSkills.setLayoutManager(new LinearLayoutManager(this));

        rvLanguages.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));
        rvExpertise.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));
        rvSkills.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));

        refreshViews();

        rvLanguages.setNestedScrollingEnabled(false);
        rvExpertise.setNestedScrollingEnabled(false);
        rvSkills.setNestedScrollingEnabled(false);

        setOnProfileLoadListener(this);
    }

    private void refreshViews() {
        profileData = Preferences.getProfileData(this);
        getLanguageData();
        SkillsAdapter mLanguageAdapter = new SkillsAdapter(this, languageList);
        rvLanguages.setAdapter(mLanguageAdapter);

        getExpertiseData();
        SkillsAdapter mExpertiseAdapter = new SkillsAdapter(this, expertiseList);
        rvExpertise.setAdapter(mExpertiseAdapter);

        getSkillData();
        SkillsAdapter mSkillAdapter = new SkillsAdapter(this, skillList);
        rvSkills.setAdapter(mSkillAdapter);
    }

    private void getLanguageData() {
        languageList = new ArrayList<>();
        if (profileData != null) {
            for (int i = 0; i < profileData.profileLanguages.size(); i++) {
                if (profileData.profileLanguages.get(i).language != null) {
                    languageList.add(new Skill(profileData.profileLanguages.get(i).language.name,
                            Utils.getLanguageLevel(profileData.profileLanguages.get(i).level)));
                }
            }
        }
    }

    private void getExpertiseData() {
        expertiseList = new ArrayList<>();
        if (profileData != null) {
            for (int i = 0; i < profileData.expertise.size(); i++) {
                if (profileData.expertise.get(i).services != null) {
                    expertiseList.add(new Skill(profileData.expertise.get(i).services.nameApp,
                            Utils.getExperienceLevel(profileData.expertise.get(i).length)));
                }
            }
        }
    }

    private void getSkillData() {
        skillList = new ArrayList<>();
        if (profileData != null) {
            for (int i = 0; i < profileData.profileSkills.size(); i++) {
                if (profileData.profileSkills.get(i).skill != null) {
                    skillList.add(new Skill(profileData.profileSkills.get(i).skill.name,
                            Utils.getRatingLevel(profileData.profileSkills.get(i).rating)));
                }
            }
        }
    }

    @OnClick({R.id.img_back, R.id.ll_language, R.id.ll_expertise, R.id.ll_skills})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.ll_language:
                Intent i = new Intent(this, LanguagesActivity.class);
                i.putExtra(Constants.IS_EDIT, true);
                startActivityForResult(i, REQ_EDIT_LANGUAGE);
                openToLeft();
                break;
            case R.id.ll_expertise:
                //i = new Intent(this, ExpertiseActivity.class);
                i = new Intent(this, SelectExpertiseActivity.class);
                i.putExtra(Constants.IS_EDIT, true);
                startActivityForResult(i, REQ_EDIT_EXPERTISE);
                openToLeft();
                break;
            case R.id.ll_skills:
                i = new Intent(this, SelectSkillsActivity.class);
                i.putExtra(Constants.IS_EDIT, true);
                startActivityForResult(i, REQ_EDIT_SKILL);
                openToLeft();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQ_EDIT_LANGUAGE:
                case REQ_EDIT_EXPERTISE:
                case REQ_EDIT_SKILL:
                    getProfile();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }

    @Override
    public void onProfileLoad(Profile.Data data) {
        refreshViews();
    }
}
