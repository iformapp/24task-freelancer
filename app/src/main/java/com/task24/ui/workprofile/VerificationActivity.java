package com.task24.ui.workprofile;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewSFTextBold;
import android.textview.TextViewSFTextRegular;
import android.textview.TextViewSFTextSemiBold;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.task24.R;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.model.GeneralModel;
import com.task24.model.Profile;
import com.task24.model.TrustPoint;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;

public class VerificationActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner, BaseActivity.OnProfileLoadListener {

    @BindView(R.id.tv_title)
    TextViewSFTextSemiBold tvTitle;
    @BindView(R.id.rv_verify)
    RecyclerView rvVerify;
    @BindView(R.id.tv_need_points)
    TextViewSFTextRegular tvNeedPoints;
    @BindView(R.id.tv_current_trust_score)
    TextViewSFTextBold tvCurrentTrustScore;
    @BindView(R.id.fb_login)
    LoginButton fbLogin;

    private ArrayList<TrustPoint> arrayList;
    private RecyclerviewAdapter mAdapter;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private boolean isRefresh = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();

        callbackManager = CallbackManager.Factory.create();
        initFacebook();

        tvTitle.setText(getString(R.string.trust_verification));
        String s = getString(R.string.need_points_text, "70");
        int[] colorList = {R.color.red_dark};
        String[] words = {"PRO"};
        String[] fonts = {Constants.SFTEXT_BOLD};
        tvNeedPoints.setText(Utils.getBoldString(this, s, fonts, colorList, words));

        rvVerify.setLayoutManager(new LinearLayoutManager(this));

        setOnProfileLoadListener(this);
    }

    private void initFacebook() {
        fbLogin.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleFacebookAccessToken(loginResult.getAccessToken());
                        //getGraphRequest(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        System.out.println("onCancel");
                        hideProgress();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e("VerificationActivity", exception.getCause().toString());
                        hideProgress();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Profile.Data profileData = Preferences.getProfileData(this);
        if (profileData != null) {
            onProfileLoad(profileData);
        }

        if (isRefresh)
            getProfile();
    }

    private void getGraphRequest(AccessToken token) {
        GraphRequest request = GraphRequest.newMeRequest(
                token,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        hideProgress();
                        try {
                            if (object != null) {
                                String id = object.getString("id");
                                verifyFacebook(id);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void setAdapter() {
        if (arrayList != null && arrayList.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new RecyclerviewAdapter(arrayList, R.layout.item_verification, this);
            }
            mAdapter.doRefresh(arrayList);
            if (rvVerify.getAdapter() == null) {
                rvVerify.setAdapter(mAdapter);
            }
        }
    }

    private void getList(Profile.TrustPoints trustPoints) {
        arrayList = new ArrayList<>();
        arrayList.add(new TrustPoint(getString(R.string.email), trustPoints.email, 10));
        arrayList.add(new TrustPoint(getString(R.string.phonenumber), trustPoints.phoneNumber, 10));
        arrayList.add(new TrustPoint(getString(R.string.facebook), trustPoints.facebook, 20));
        arrayList.add(new TrustPoint(getString(R.string.payment), trustPoints.payment, 30));
        arrayList.add(new TrustPoint(getString(R.string.verify_id), trustPoints.verifyId, 40));
    }

    @OnClick({R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }

    @Override
    public void bindView(View view, final int position) {
        TextView tvTitle = view.findViewById(R.id.tv_title);
        TextView tvPoints = view.findViewById(R.id.tv_points);
        tvTitle.setText(arrayList.get(position).title);
        tvPoints.setText(arrayList.get(position).totalPoint + " POINTS");
        if (arrayList.get(position).point == 0) {
            tvPoints.setBackground(ContextCompat.getDrawable(this, R.drawable.red_rounded_corner));
        } else {
            tvPoints.setBackground(ContextCompat.getDrawable(this, R.drawable.green_rounded_corner));
        }

        LinearLayout llMain = view.findViewById(R.id.ll_main);
        llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = arrayList.get(position).title;
                if (item.equals(getString(R.string.email))) {
                    if (arrayList.get(position).point == 0) {
                        verifyEmail();
                    } else {
                        toastMessage("You already verify email");
                    }
                } else if (item.equals(getString(R.string.phonenumber))) {
                    redirectActivity(VerifyPhoneNumberActivity.class);
                } else if (item.equals(getString(R.string.facebook))) {
                    if (arrayList.get(position).point == 0) {
                        showDialog();
                    } else {
                        toastMessage("You already verify facebook");
                    }
                } else if (item.equals(getString(R.string.payment))) {

                } else if (item.equals(getString(R.string.verify_id))) {
                    if (arrayList.get(position).point == 0) {
                        checkPermission();
                    } else {
                        toastMessage("You already done VerifyID");
                    }
                }
            }
        });
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("'24task' wants to use 'facebook.com' to Sign In");
        builder.setMessage("This allows the app and website to share information about you");
        builder.setCancelable(false);

        builder.setPositiveButton(
                "Continue",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        showProgress();
                        LoginManager.getInstance().logInWithReadPermissions(VerificationActivity.this, Arrays.asList("public_profile"));
                    }
                });

        builder.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void verifyEmail() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().verifyEmail(getUserID());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    toastMessage(response.body().msg);
                    redirectActivity(VerifyOtpActivity.class);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("verify email failed");
            }
        });
    }

    private void handleFacebookAccessToken(final AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            hideProgress();
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {
                                Log.e("FirebaseFB Login", user.getUid());
                                verifyFacebook(user.getUid());
                                //getGraphRequest(token, user.getUid());
                            }
                        } else {
                            failureError(task.getException().getMessage());
                            Log.e("FirebaseFB Fails", task.getException().getMessage());
                        }
                    }
                });
    }

    public void verifyFacebook(String fbId) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().verifyFacebook(getUserID(), fbId);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                hideProgress();
                if (checkStatus(response.body())) {
                    getProfile();
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("verify email failed");
            }
        });
    }

    @Override
    public void onProfileLoad(Profile.Data data) {
        if (data.trustPoints != null) {
            isRefresh = true;
            getList(data.trustPoints);
            setAdapter();

            int trustScore = 0;
            if (data.trustPoints.email != 0) {
                trustScore = trustScore + 10;
            }
            if (data.trustPoints.phoneNumber != 0) {
                trustScore = trustScore + 10;
            }
            if (data.trustPoints.facebook != 0) {
                trustScore = trustScore + 20;
            }
            if (data.trustPoints.payment != 0) {
                trustScore = trustScore + 30;
            }
            if (data.trustPoints.verifyId != 0) {
                trustScore = trustScore + 40;
            }
            tvCurrentTrustScore.setText("Current Trust Score: " + trustScore + "/110");
        }
    }

    private void checkPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent intent = new Intent(VerificationActivity.this, ImagePickActivity.class);
                            intent.putExtra(IS_NEED_CAMERA, true);
                            intent.putExtra(Constant.MAX_NUMBER, 1);
                            startActivityForResult(intent, Constant.REQUEST_CODE_PICK_IMAGE);
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            toastMessage("Please give permission");
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_IMAGE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    isRefresh = false;
                    ArrayList<ImageFile> imgPaths = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                    if (imgPaths != null && imgPaths.size() > 0) {
                        Log.e("Image Path == > ", imgPaths.get(0).getPath());
                        verifyId(new File(imgPaths.get(0).getPath()));
                    } else {
                        toastMessage("File not selected");
                    }
                }
                break;
        }
    }

    public void verifyId(File file) {
        if (!isNetworkConnected())
            return;

        showProgress();

        MultipartBody.Part body = null;
        if (file != null) {
            Uri selectedUri = Uri.fromFile(file);
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());

            RequestBody requestFile = null;
            if (mimeType != null) {
                requestFile = RequestBody.create(MediaType.parse(mimeType), file);
            }

            if (requestFile != null) {
                body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
            }
        }

        RequestBody type = RequestBody.create(MultipartBody.FORM, "1");
        RequestBody jwt = RequestBody.create(MultipartBody.FORM, getJWT());

        Call<GeneralModel> call = getService().verifyProfileId(getUserID(), type, body, jwt);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                hideProgress();
                if (checkStatus(response.body())) {
                    getProfile();
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("Verify Id failed");
            }
        });
    }
}
