package com.task24.ui.workprofile;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.task24.R;
import com.task24.adapter.AvailabilityAdapter;
import com.task24.model.Available;
import com.task24.model.Profile;
import com.task24.ui.BaseActivity;
import com.task24.util.EqualSpacingItemDecoration;
import com.task24.util.Preferences;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAvailabilityActivity extends BaseActivity {

    @BindView(R.id.rv_work_type)
    RecyclerView rvWorkType;
    @BindView(R.id.rv_availability)
    RecyclerView rvAvailability;

    private ArrayList<Available.Data> workTypeList;
    private ArrayList<Available.Data> availabilityList;
    private Profile.Data profileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_availability);
        ButterKnife.bind(this);

        profileData = Preferences.getProfileData(this);

        rvWorkType.setLayoutManager(new LinearLayoutManager(this));
        rvAvailability.setLayoutManager(new LinearLayoutManager(this));

        rvWorkType.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));
        rvAvailability.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));

        getWorkTypeList();
        AvailabilityAdapter mWorkAdapter = new AvailabilityAdapter(this, workTypeList, true);
        rvWorkType.setAdapter(mWorkAdapter);

        rvWorkType.setNestedScrollingEnabled(false);
        rvAvailability.setNestedScrollingEnabled(false);

        getPayType();
    }

    private void getWorkTypeList() {
        if (profileData == null) {
            return;
        }

        workTypeList = new ArrayList<>();
        Available.Data model = new Available.Data();
        model.name = "Office-base";
        if (!isEmpty(profileData.workbase) && (profileData.workbase.equals("0") || profileData.workbase.equals("2"))) {
            model.isChecked = true;
        }
        workTypeList.add(model);
        model = new Available.Data();
        model.name = "Home-base";
        if (!isEmpty(profileData.workbase) && (profileData.workbase.equals("1") || profileData.workbase.equals("2"))) {
            model.isChecked = true;
        }
        workTypeList.add(model);
    }

    private void getAvailableList() {
        for (int i = 0; i < availabilityList.size(); i++) {
            if (isExistInList(availabilityList.get(i).name)) {
                availabilityList.get(i).isChecked = true;
            }
        }
    }

    private boolean isExistInList(String key) {
        if (profileData.profilePayTypes != null && profileData.profilePayTypes.size() > 0) {
            for (int i = 0; i < profileData.profilePayTypes.size(); i++) {
                if (profileData.profilePayTypes.get(i).payType.name.equals(key)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void getPayType() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Available> call = getService().getPayType();
        call.enqueue(new Callback<Available>() {
            @Override
            public void onResponse(Call<Available> call, Response<Available> response) {
                Available available = response.body();
                if (available != null) {
                    if (checkStatus(available)) {
                        availabilityList = new ArrayList<>();
                        availabilityList = (ArrayList<Available.Data>) available.data;

                        getAvailableList();
                        AvailabilityAdapter mAvailableAdapter = new AvailabilityAdapter(EditAvailabilityActivity.this, availabilityList, false);
                        rvAvailability.setAdapter(mAvailableAdapter);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Available> call, Throwable t) {
                failureError("get paytype failed");
            }
        });
    }

    @OnClick({R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        getProfile();
        super.onBackPressed();
        finishToRight();
    }
}
