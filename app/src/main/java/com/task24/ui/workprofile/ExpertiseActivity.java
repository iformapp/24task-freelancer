package com.task24.ui.workprofile;

import android.app.Dialog;
import android.button.ButtonSFTextBold;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.textview.TextViewSFTextBold;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.adapter.ExpertiseItemAdapter;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.model.GeneralModel;
import com.task24.model.Profile;
import com.task24.model.ServicesModel;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExpertiseActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.rv_expertise)
    RecyclerView rvExpertise;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.rl_edit)
    RelativeLayout rlEdit;
    @BindView(R.id.btn_next)
    ButtonSFTextBold btnNext;
    @BindView(R.id.tv_add_expertise)
    TextViewSFTextBold tvAddExpertise;

    private ArrayList<ServicesModel.Data> expertiseList;
    private ArrayList<ServicesModel.Data> serviceList;
    private RecyclerviewAdapter mAdapter;
    private boolean isEdit = false;
    private ExpertiseItemAdapter itemAdapter;
    private Profile.Data profileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expertise);
        ButterKnife.bind(this);

        expertiseList = new ArrayList<>();
        serviceList = (ArrayList<ServicesModel.Data>) Preferences.getTopServices(this);

        if (getIntent() != null) {
            isEdit = getIntent().getBooleanExtra(Constants.IS_EDIT, false);
        }
        if (!isEdit)
            expertiseList.add(new ServicesModel.Data());

        profileData = Preferences.getProfileData(this);
        progress.setProgress(20);
        rvExpertise.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RecyclerviewAdapter(expertiseList, R.layout.item_expertise_edit, this);
        rvExpertise.setAdapter(mAdapter);

        if (isEdit) {
            rlEdit.setVisibility(View.VISIBLE);
            header.setVisibility(View.GONE);
            btnNext.setVisibility(View.GONE);

            if (profileData != null && profileData.expertise != null && profileData.expertise.size() > 0) {
                for (int i = 0; i < profileData.expertise.size(); i++) {
                    Profile.Expertise expertise = profileData.expertise.get(i);
                    if (expertise.services != null) {
                        ServicesModel.Data model = new ServicesModel.Data();
                        model.id = expertise.services.id;
                        model.name = expertise.services.nameApp;
                        model.experience = Utils.getExperienceLevel(expertise.length);
                        model.experienceId = expertise.length;
                        expertiseList.add(model);
                    }
                }
            } else {
                expertiseList.add(new ServicesModel.Data());
            }
            mAdapter.doRefresh(expertiseList);
            checkLimit();
        }
    }

    @OnClick({R.id.img_back, R.id.tv_cancel, R.id.tv_add_expertise, R.id.btn_next, R.id.tv_edit_cancel, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_cancel:
                gotoMainActivity(Constants.TAB_HOME);
                break;
            case R.id.tv_add_expertise:
                expertiseList.add(new ServicesModel.Data());
                mAdapter.doRefresh(expertiseList);
                checkLimit();
                break;
            case R.id.tv_save:
            case R.id.btn_next:
                if (isValid()) {
                    StringBuilder serviceIds = null;
                    StringBuilder experienceIds = null;
                    for (int i = 0; i < expertiseList.size(); i++) {
                        serviceIds = (serviceIds == null ? new StringBuilder("") : serviceIds.append(",")).append(expertiseList.get(i).id);
                        experienceIds = (experienceIds == null ? new StringBuilder("") : experienceIds.append(",")).append(expertiseList.get(i).experienceId);
                    }

                    String serviceId = serviceIds == null ? "" : serviceIds.toString();
                    String experienceId = experienceIds == null ? "" : experienceIds.toString();
                    addExpertise(serviceId, experienceId);
                }
                break;
            case R.id.tv_edit_cancel:
                onBackPressed();
                break;
        }
    }

    private void checkLimit() {
        if (expertiseList.size() >= 2) {
            tvAddExpertise.setVisibility(View.GONE);
        } else {
            tvAddExpertise.setVisibility(View.VISIBLE);
        }
    }

    public void addExpertise(String serviceId, String experienceId) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().updateExperience(getUserID(), experienceId, serviceId, getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    if (isEdit) {
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        redirectActivity(AvailableForWorkActivity.class);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("Add experience failed");
            }
        });
    }

    @Override
    public void bindView(View view, final int position) {
        final TextView tvExpertise = view.findViewById(R.id.tv_expertise);
        final TextView tvExperience = view.findViewById(R.id.tv_experience);
        ImageView imgDelete = view.findViewById(R.id.img_delete);

        if (expertiseList.get(position) != null) {
            tvExpertise.setText(expertiseList.get(position).name);
            tvExperience.setText(expertiseList.get(position).experience);
        }

        if (position == 0) {
            imgDelete.setVisibility(View.GONE);
        } else {
            imgDelete.setVisibility(View.VISIBLE);
        }

        tvExpertise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (serviceList != null) {
                    showExpertiseSelectDialog(serviceList, tvExpertise, expertiseList.get(position), true);
                }
            }
        });

        tvExperience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isEmpty(serviceList.get(position).name)) {
                    showExpertiseSelectDialog(getServicesList(), tvExperience, expertiseList.get(position), false);
                }
            }
        });

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expertiseList.remove(position);
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, expertiseList.size());
                checkLimit();
            }
        });
    }

    public void showExpertiseSelectDialog(ArrayList<ServicesModel.Data> arrayList, final TextView textView,
                                          final ServicesModel.Data expertise, final boolean isExpertise) {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select_black);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        final EditText etSearch = dialog.findViewById(R.id.et_search);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        if (isExpertise) {
            etSearch.setHint(String.format(getString(R.string.search_for), getString(R.string.expertise).toLowerCase()));
        } else {
            etSearch.setVisibility(View.GONE);
        }

        rvTypes.setLayoutManager(new LinearLayoutManager(this));
        if (arrayList != null && arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (isExpertise) {
                    arrayList.get(i).isSelected = arrayList.get(i).name.equalsIgnoreCase(textView.getText().toString());
                } else {
                    arrayList.get(i).isSelected = arrayList.get(i).experience.equalsIgnoreCase(textView.getText().toString());
                }
            }
            itemAdapter = new ExpertiseItemAdapter(this, arrayList, isExpertise);
            rvTypes.setAdapter(itemAdapter);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemAdapter != null && itemAdapter.getSelectedItem() != null) {
                    if (isExpertise) {
                        textView.setText(itemAdapter.getSelectedItem().name);
                        expertise.name = itemAdapter.getSelectedItem().name;
                        expertise.id = itemAdapter.getSelectedItem().id;
                    } else {
                        textView.setText(itemAdapter.getSelectedItem().experience);
                        expertise.experience = itemAdapter.getSelectedItem().experience;
                        expertise.experienceId = itemAdapter.getSelectedItem().experienceId;
                    }
                    dialog.dismiss();
                } else {
                    Toast.makeText(ExpertiseActivity.this, "Please select one item", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                if (itemAdapter != null)
                    itemAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etSearch.post(new Runnable() {
                    @Override
                    public void run() {
                        Utils.openSoftKeyboard(ExpertiseActivity.this, etSearch);
                    }
                });
            }
        });
        etSearch.requestFocus();
    }

    private ArrayList<ServicesModel.Data> getServicesList() {
        ArrayList<ServicesModel.Data> arrayList = new ArrayList<>();
        ServicesModel.Data model = new ServicesModel.Data();
        model.experience = getString(R.string.less_than_1_year);
        model.experienceId = 0;
        arrayList.add(model);
        model = new ServicesModel.Data();
        model.experience = getString(R.string.year_1_3);
        model.experienceId = 1;
        arrayList.add(model);
        model = new ServicesModel.Data();
        model.experience = getString(R.string.year_4_6);
        model.experienceId = 2;
        arrayList.add(model);
        model = new ServicesModel.Data();
        model.experience = getString(R.string.year_7_9);
        model.experienceId = 3;
        arrayList.add(model);
        model = new ServicesModel.Data();
        model.experience = getString(R.string.year_10_12);
        model.experienceId = 4;
        arrayList.add(model);
        model = new ServicesModel.Data();
        model.experience = getString(R.string.year_13);
        model.experienceId = 5;
        arrayList.add(model);
        return arrayList;
    }

    private boolean isValid() {
        if (expertiseList != null && expertiseList.size() > 0) {
            for (int i = 0; i < expertiseList.size(); i++) {
                if (expertiseList.get(i) == null) {
                    validationError("Please select expertise");
                    return false;
                }

                if (isEmpty(expertiseList.get(i).name)) {
                    validationError("Please select expertise");
                    return false;
                } else if (isEmpty(expertiseList.get(i).experience)) {
                    validationError("Please select experience");
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
