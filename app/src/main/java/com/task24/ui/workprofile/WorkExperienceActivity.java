package com.task24.ui.workprofile;

import android.button.ButtonSFTextBold;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.task24.R;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.model.GeneralModel;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.EqualSpacingItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkExperienceActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.rv_experience)
    RecyclerView rvExperience;
    @BindView(R.id.btn_next)
    ButtonSFTextBold btnNext;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.rl_edit)
    RelativeLayout rlEdit;

    private List<String> expList;
    private RecyclerviewAdapter mAdapter;
    private SparseBooleanArray selectedExpArray;
    private String serviceIds;
    private boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_experience);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            serviceIds = getIntent().getStringExtra(Constants.SERVICE_IDS);
            isEdit = getIntent().getBooleanExtra(Constants.IS_EDIT, false);
        }

        if (isEdit) {
            rlEdit.setVisibility(View.VISIBLE);
            header.setVisibility(View.GONE);
            btnNext.setVisibility(View.GONE);
        }

        progress.setProgress(40);

        selectedExpArray = new SparseBooleanArray();
        expList = new ArrayList<>();
        expList.add(getString(R.string.less_than_1_year));
        expList.add(getString(R.string.year_1_3));
        expList.add(getString(R.string.year_4_6));
        expList.add(getString(R.string.year_7_9));
        expList.add(getString(R.string.year_10_12));
        expList.add(getString(R.string.year_13));

        rvExperience.setLayoutManager(new LinearLayoutManager(this));
        rvExperience.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));

        mAdapter = new RecyclerviewAdapter((ArrayList<?>) expList, R.layout.item_experience, this);
        rvExperience.setAdapter(mAdapter);
    }

    @OnClick({R.id.img_back, R.id.tv_cancel, R.id.btn_next, R.id.tv_edit_cancel, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_cancel:
                gotoMainActivity(Constants.TAB_HOME);
                break;
            case R.id.tv_save:
            case R.id.btn_next:
                if (isEmpty(serviceIds))
                    return;

                if (selectedExpArray.size() == 1) {
                    String[] split = serviceIds.split(",");
                    String experience;
                    if (split.length == 2) {
                        experience = selectedExpArray.keyAt(0) + "," + selectedExpArray.keyAt(0);
                    } else {
                        experience = selectedExpArray.keyAt(0) + "";
                    }
                    updateExperience(experience);
                } else {
                    validationError("Please select experience");
                }
                break;
            case R.id.tv_edit_cancel:
                onBackPressed();
                break;
        }
    }

    public void updateExperience(String experience) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().updateExperience(getUserID(), experience, serviceIds, getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    if (isEdit) {
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        redirectActivity(AvailableForWorkActivity.class);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("Add experience failed");
            }
        });
    }

    @Override
    public void bindView(View view, final int position) {
        final TextView textView = view.findViewById(R.id.tv_skill);
        textView.setText(expList.get(position));

        if (selectedExpArray.get(position)) {
            textView.setBackground(ContextCompat.getDrawable(WorkExperienceActivity.this, R.drawable.blue_button_bg));
            textView.setTextColor(Color.WHITE);
            textView.setTypeface(Typeface.createFromAsset(getAssets(), Constants.SFTEXT_BOLD));
        } else {
            textView.setBackground(ContextCompat.getDrawable(WorkExperienceActivity.this, R.drawable.white_button_bg));
            textView.setTextColor(Color.BLACK);
            textView.setTypeface(Typeface.createFromAsset(getAssets(), Constants.SFTEXT_REGULAR));
        }

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedExpArray.clear();
                selectedExpArray.put(position, true);
                if (!isEdit)
                    btnNext.setVisibility(View.VISIBLE);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
