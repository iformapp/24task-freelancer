package com.task24.ui.workprofile;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.edittext.EditTextSFTextRegular;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.task24.R;
import com.task24.model.GeneralModel;
import com.task24.model.Profile;
import com.task24.ui.BaseActivity;
import com.task24.util.LocationAddress;
import com.task24.util.Preferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateLocationActivity extends BaseActivity implements ConnectionCallbacks,
        OnConnectionFailedListener, LocationListener {

    @BindView(R.id.et_country)
    EditTextSFTextRegular etCountry;
    @BindView(R.id.et_state)
    EditTextSFTextRegular etState;
    @BindView(R.id.et_city)
    EditTextSFTextRegular etCity;

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private double latitude;
    private double longitude;
    private Profile.Data profileData;
    private boolean isNeedToUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_location);
        ButterKnife.bind(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        profileData = Preferences.getProfileData(this);
        if (profileData != null && profileData.address != null) {
            etCountry.setText(profileData.address.country);
            etCity.setText(profileData.address.city);
            etState.setText(profileData.address.region);
        }

        addTextChangeEvent(etState, etCountry, etCity);
    }

    public void addTextChangeEvent(EditText... editTexts) {
        for (EditText edittext : editTexts) {
            edittext.addTextChangedListener(textWatcher);
        }
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            isNeedToUpdate = true;
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @OnClick({R.id.img_back, R.id.btn_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                if (isNeedToUpdate) {
                    updateLocation();
                } else {
                    onBackPressed();
                }
                break;
            case R.id.btn_location:
                if (mGoogleApiClient.isConnected())
                    getLocation();
                else
                    mGoogleApiClient.connect();
                break;
        }
    }

    public void updateLocation() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().updateLocation(getUserID(), getCountry(), getState(), getCity(),
                longitude, latitude, getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                isNeedToUpdate = false;
                Profile.Address address = new Profile.Address();
                address.city = getCity();
                address.region = getState();
                address.country = getCountry();
                profileData.address = address;
                Preferences.setProfileData(UpdateLocationActivity.this, profileData);
                hideProgress();
                toastMessage("Location updated successfully");
                onBackPressed();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update location failed");
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isNeedToUpdate)
            updateLocation();
        else {
            super.onBackPressed();
            finishToRight();
        }
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        settingRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "Connection Suspended!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection Failed!", Toast.LENGTH_SHORT).show();
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, 90000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("Current Location", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    public void settingRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);    // 10 seconds, in milliseconds
        mLocationRequest.setFastestInterval(1000);   // 1 second, in milliseconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(UpdateLocationActivity.this, 1000);
                        } catch (IntentSender.SendIntentException ignored) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1000:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(this, "Location Service not Enabled", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    public void getLocation() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            if (ActivityCompat.checkSelfPermission(UpdateLocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                    ActivityCompat.checkSelfPermission(UpdateLocationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                            if (mLastLocation != null) {
                                latitude = mLastLocation.getLatitude();
                                longitude = mLastLocation.getLongitude();
                                LocationAddress.getAddressFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(),
                                        getApplicationContext(), new GeocoderHandler());
                                //Toast.makeText(getApplicationContext(), "Longitude:" + String.valueOf(mLastLocation.getLongitude()) + "\nLatitude:" + String.valueOf(mLastLocation.getLatitude()), Toast.LENGTH_SHORT).show();
                            } else {
                                if (!mGoogleApiClient.isConnected())
                                    mGoogleApiClient.connect();

                                if (mGoogleApiClient.isConnected())
                                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
                                            UpdateLocationActivity.this);
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            toastMessage("Please give permission");
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            Address locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getParcelable("address");
                    if (locationAddress != null) {
                        Log.e("Address", locationAddress.getAddressLine(0));
                        etCountry.setText(locationAddress.getCountryName());
                        etState.setText(locationAddress.getAdminArea());
                        if (isEmpty(locationAddress.getSubLocality()) || locationAddress.getSubLocality().equals("null")) {
                            etCity.setText(locationAddress.getSubAdminArea());
                        } else {
                            etCity.setText(locationAddress.getSubLocality() + ", " + locationAddress.getSubAdminArea());
                        }
                    }
                    break;
            }
        }
    }

    public String getCountry() {
        return etCountry.getText().toString().trim();
    }

    public String getState() {
        return etState.getText().toString().trim();
    }

    public String getCity() {
        return etCity.getText().toString().trim();
    }
}
