package com.task24.ui.workprofile;

import android.edittext.EditTextSFDisplayRegular;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.task24.R;
import com.task24.model.GeneralModel;
import com.task24.ui.BaseActivity;
import com.task24.util.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HeadlinesActivity extends BaseActivity {

    @BindView(R.id.et_headline)
    EditTextSFDisplayRegular etHeadline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_headlines);
        ButterKnife.bind(this);

        if (Preferences.getProfileData(this).headline != null)
            etHeadline.setText(Preferences.getProfileData(this).headline.content);

        etHeadline.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etHeadline.setRawInputType(InputType.TYPE_CLASS_TEXT);
    }

    public String getHeadline() {
        return etHeadline.getText().toString().trim();
    }

    @OnClick({R.id.img_back, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_save:
                if (isValid()) {
                    updateHeadlines();
                }
                break;
        }
    }

    public void updateHeadlines() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().addHeadlines(getUserID(), getHeadline(), getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    onBackPressed();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update headline failed");
            }
        });
    }

    private boolean isValid() {
        if (isEmpty(getHeadline())) {
            validationError(getString(R.string.enter_professional_headline));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
