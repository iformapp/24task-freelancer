package com.task24.ui.workprofile;

import android.edittext.EditTextSFTextRegular;
import android.os.Bundle;
import android.view.View;

import com.task24.R;
import com.task24.ccp.CountryCodePicker;
import com.task24.ui.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerifyPhoneNumberActivity extends BaseActivity {

    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.et_mobile)
    EditTextSFTextRegular etMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verify);
        ButterKnife.bind(this);

        ccp.registerCarrierNumberEditText(etMobile);
    }

    public String getMobile() {
        return etMobile.getText().toString().trim();
    }

    public String getMobilePrefix() {
        return ccp.getSelectedCountryCodeWithPlus();
    }

    @OnClick({R.id.img_back, R.id.tv_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_submit:
                if (isValid()) {
                    varifyPhoneNumber();
                }
                break;
        }
    }

    public void varifyPhoneNumber() {
        if (!isNetworkConnected())
            return;

//        showProgress();
//
//        Call<GeneralModel> call = getService().addHeadlines(getUserID(), getEmail(), getJWT());
//        call.enqueue(new Callback<GeneralModel>() {
//            @Override
//            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
//                if (checkStatus(response.body())) {
//                    onBackPressed();
//                }
//                hideProgress();
//            }
//
//            @Override
//            public void onFailure(Call<GeneralModel> call, Throwable t) {
//                failureError("update headline failed");
//            }
//        });
    }

    private boolean isValid() {
        if (!ccp.isValidFullNumber()) {
            validationError(getString(R.string.valid_mobile_no));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
