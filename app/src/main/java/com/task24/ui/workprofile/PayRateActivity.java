package com.task24.ui.workprofile;

import android.checkbox.CheckBoxSFTextRegular;
import android.edittext.EditTextSFTextBold;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.model.GeneralModel;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayRateActivity extends BaseActivity {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.img_minus)
    ImageView imgMinus;
    @BindView(R.id.img_plus)
    ImageView imgPlus;
    @BindView(R.id.chk_tnc)
    CheckBoxSFTextRegular chkTnc;
    @BindView(R.id.et_rate)
    EditTextSFTextBold etRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_rate);
        ButterKnife.bind(this);

        progress.setProgress(100);

        etRate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!isEmpty(s.toString()) && !s.toString().equals(".00")) {
                    if (Double.parseDouble(s.toString()) >= 100) {
                        etRate.setText("99.99");
                        Toast.makeText(PayRateActivity.this, "Enter Rate only upto 99.99", Toast.LENGTH_LONG).show();
                    } else if (Double.parseDouble(s.toString()) < 1) {
                        etRate.setText("1.00");
                        Toast.makeText(PayRateActivity.this, "Enter Rate only above 1.00", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etRate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etRate.setFocusable(true);
                etRate.setCursorVisible(true);
                etRate.setSelection(getRate().length());
                return false;
            }
        });

        etRate.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Utils.hideSoftKeyboard(PayRateActivity.this);
                    etRate.setCursorVisible(false);
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick({R.id.img_back, R.id.tv_cancel, R.id.img_minus, R.id.img_plus, R.id.btn_submit})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(this);
        etRate.setCursorVisible(false);
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_cancel:
                gotoMainActivity(Constants.TAB_HOME);
                break;
            case R.id.img_minus:
                if (!isEmpty(getRate())) {
                    double rate = Double.parseDouble(getRate());
                    if (rate != 1) {
                        etRate.setText(Utils.numberFormat(String.valueOf(rate - 1), 2));
                    }
                } else {
                    etRate.setText("1.00");
                }
                break;
            case R.id.img_plus:
                if (!isEmpty(getRate())) {
                    double rate = Double.parseDouble(getRate());
                    etRate.setText(Utils.numberFormat(String.valueOf(rate + 1), 2));
                } else {
                    etRate.setText("1.00");
                }
                break;
            case R.id.btn_submit:
                if (chkTnc.isChecked()) {
                    addPayRate();
                } else {
                    validationError("Please check terms & conditions");
                }
                break;
        }
    }

    public void addPayRate() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().addPayRate(getUserID(), getRate(), getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    gotoMainActivity(Constants.TAB_HOME);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("Pay Rate failed");
            }
        });
    }

    public String getRate() {
        return etRate.getText().toString();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
