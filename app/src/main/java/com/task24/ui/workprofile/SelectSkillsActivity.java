package com.task24.ui.workprofile;

import android.app.Dialog;
import android.edittext.EditTextSFTextRegular;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.textview.TextViewSFTextBold;
import android.textview.TextViewSFTextRegular;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.adapter.SingleSelectionItemAdapter;
import com.task24.adapter.SkillListAdapter;
import com.task24.model.GeneralModel;
import com.task24.model.Skill;
import com.task24.model.UserSkillsModel;
import com.task24.model.UserSkillsModel.SkillLists;
import com.task24.model.UserSkillsModel.Userskills;
import com.task24.ui.BaseActivity;
import com.task24.util.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectSkillsActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFTextBold tvToolbarTitle;
    @BindView(R.id.rl_edit)
    RelativeLayout rlEdit;
    @BindView(R.id.tv_skill_no)
    TextViewSFTextRegular tvSkillNo;
    @BindView(R.id.rv_selected_skills)
    RecyclerView rvSelectedSkills;
    @BindView(R.id.ll_selected_skills)
    LinearLayout llSelectedSkills;
    @BindView(R.id.rv_skills)
    RecyclerView rvSkills;
    @BindView(R.id.et_search)
    EditTextSFTextRegular etSearch;
    @BindView(R.id.tv_total_skill)
    TextViewSFTextRegular tvTotalSkill;

    private ArrayList<Skill> skillArrayList;
    private SkillListAdapter skillListAdapter;
    private ArrayList<Skill> selectedSkillList;
    private RecyclerviewAdapter selectedAdapter;
    private SingleSelectionItemAdapter itemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_skills);
        ButterKnife.bind(this);

        selectedSkillList = new ArrayList<>();

        rlEdit.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(getString(R.string.select_your_skills));

        rvSkills.setLayoutManager(new LinearLayoutManager(this));
        rvSelectedSkills.setLayoutManager(new LinearLayoutManager(this));

        rvSkills.setNestedScrollingEnabled(false);
        rvSelectedSkills.setNestedScrollingEnabled(false);

        getSkillsList();

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                skillListAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void refreshSelectedSkills(ArrayList<Skill> skillArrayList, int position) {
        this.skillArrayList = skillArrayList;
        if (skillArrayList.get(position).isSelected) {
            selectedSkillList.add(skillArrayList.get(position));
        } else {
            for (int i = 0; i < selectedSkillList.size(); i++) {
                if (selectedSkillList.get(i).skillTitle.equals(skillArrayList.get(position).skillTitle)) {
                    selectedSkillList.remove(i);
                    break;
                }
            }
        }
        selectedAdapter.doRefresh(selectedSkillList);
        tvSkillNo.setText(selectedSkillList.size() + "");
    }

    @OnClick({R.id.tv_edit_cancel, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_edit_cancel:
                onBackPressed();
                break;
            case R.id.tv_save:
                if (selectedSkillList != null && selectedSkillList.size() > 0) {
                    for (int i = 0; i < selectedSkillList.size(); i++) {
                        if (isEmpty(selectedSkillList.get(i).skillValue)) {
                            validationError("Please select experience level");
                            return;
                        }
                    }

                    StringBuilder skillIds = null;
                    StringBuilder ratingIds = null;
                    for (int i = 0; i < selectedSkillList.size(); i++) {
                        skillIds = (skillIds == null ? new StringBuilder("") : skillIds.append(",")).append(selectedSkillList.get(i).skillId);
                        ratingIds = (ratingIds == null ? new StringBuilder("") : ratingIds.append(",")).append(Utils.getRatingId(selectedSkillList.get(i).skillValue));
                    }

                    String skillsId = skillIds == null ? "" : skillIds.toString();
                    String ratingsId = ratingIds == null ? "" : ratingIds.toString();
                    addSkills(skillsId, ratingsId);
                } else {
                    toastMessage("Please select a skill first");
                }
                break;
        }
    }

    public void addSkills(String skillsIds, String ratingIds) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().addSkills(getUserID(), skillsIds, ratingIds, getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    setResult(RESULT_OK);
                    finish();
                    finishToRight();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("add skills failed");
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }

    @Override
    public void bindView(View view, final int position) {
        TextView txtSkill = view.findViewById(R.id.tv_skill_name);
        final TextView txtSkillLevel = view.findViewById(R.id.tv_skill_level);
        ImageView imgRemove = view.findViewById(R.id.img_add_remove);

        txtSkill.setText(selectedSkillList.get(position).skillTitle);
        txtSkillLevel.setText(selectedSkillList.get(position).skillValue);
        imgRemove.setImageResource(R.drawable.close_red);

        txtSkillLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSingleSelectionDialog(txtSkillLevel, selectedSkillList.get(position));
            }
        });

        imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < skillArrayList.size(); i++) {
                    if (skillArrayList.get(i).skillTitle.equals(selectedSkillList.get(position).skillTitle)) {
                        skillArrayList.get(i).isSelected = false;
                    }
                }
                selectedSkillList.remove(position);
                selectedAdapter.doRefresh(selectedSkillList);
                skillListAdapter.doRefresh(skillArrayList);
                tvSkillNo.setText(selectedSkillList.size() + "");
            }
        });
    }

    public void getSkillsList() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<UserSkillsModel> call = getService().getSkills(getUserID());
        call.enqueue(new Callback<UserSkillsModel>() {
            @Override
            public void onResponse(Call<UserSkillsModel> call, Response<UserSkillsModel> response) {
                UserSkillsModel userSkillsModel = response.body();
                if (userSkillsModel != null) {
                    if (checkStatus(userSkillsModel)) {
                        List<Userskills> userskillsList = userSkillsModel.data.userskills;
                        List<SkillLists> skillLists = userSkillsModel.data.skillLists;

                        skillArrayList = new ArrayList<>();
                        selectedSkillList = new ArrayList<>();
                        for (int i = 0; i < skillLists.size(); i++) {
                            SkillLists item = skillLists.get(i);
                            boolean isSelected = false;
                            for (int j = 0; j < userskillsList.size(); j++) {
                                if (skillLists.get(i).id == userskillsList.get(j).skill.id) {
                                    isSelected = true;
                                    break;
                                }
                            }
                            skillArrayList.add(new Skill(item.id, item.name, "", isSelected));
                        }

                        skillListAdapter = new SkillListAdapter(SelectSkillsActivity.this, skillArrayList);
                        rvSkills.setAdapter(skillListAdapter);

                        for (int i = 0; i < userskillsList.size(); i++) {
                            Userskills item = userskillsList.get(i);
                            selectedSkillList.add(new Skill(item.skill.id, item.skill.name, Utils.getRatingLevel(item.rating)));
                        }

                        selectedAdapter = new RecyclerviewAdapter(selectedSkillList, R.layout.item_selected_skills,
                                SelectSkillsActivity.this);
                        rvSelectedSkills.setAdapter(selectedAdapter);
                        tvSkillNo.setText(selectedSkillList.size() + "");
                        tvTotalSkill.setText("/" + skillArrayList.size() + "");
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<UserSkillsModel> call, Throwable t) {
                failureError("get skills failed");
            }
        });
    }

    private void showSingleSelectionDialog(final TextView tvLevel, final Skill item) {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select_black);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        EditText etSearch = dialog.findViewById(R.id.et_search);
        etSearch.setVisibility(View.GONE);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        rvTypes.setLayoutManager(new LinearLayoutManager(this));
        String[] experience = getResources().getStringArray(R.array.experience_rate);
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(experience));
        int selectedPosition = -1;
        if (arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).equalsIgnoreCase(tvLevel.getText().toString())) {
                    selectedPosition = i;
                }
            }
            itemAdapter = new SingleSelectionItemAdapter(this, arrayList, selectedPosition);
            rvTypes.setAdapter(itemAdapter);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(SelectSkillsActivity.this);
                if (itemAdapter != null && itemAdapter.getSelectedItem() != null) {
                    tvLevel.setText(itemAdapter.getSelectedItem());
                    item.skillValue = itemAdapter.getSelectedItem();
                    dialog.dismiss();
                } else {
                    Toast.makeText(SelectSkillsActivity.this, "Please select one item", Toast.LENGTH_SHORT).show();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }
}
