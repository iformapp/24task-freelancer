package com.task24.ui.workprofile;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.style.ClickableSpan;
import android.textview.TextViewSFTextRegular;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.model.GeneralModel;
import com.task24.segment.SegmentedButton;
import com.task24.segment.SegmentedButtonGroup;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectWorkPlaceActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.tv_read_this)
    TextViewSFTextRegular tvReadThis;
    @BindView(R.id.rv_work_place)
    RecyclerView rvWorkPlace;

    private List<String> workPlaceList;
    private RecyclerviewAdapter mAdapter;
    private SparseBooleanArray workArrayIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_work_place);
        ButterKnife.bind(this);

        progress.setProgress(60);

        workArrayIds = new SparseBooleanArray();

        workPlaceList = new ArrayList<>();
        workPlaceList.add("Office-base");
        workPlaceList.add("Home-base");

        rvWorkPlace.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new RecyclerviewAdapter((ArrayList<?>) workPlaceList, R.layout.item_select_work_place, this);
        rvWorkPlace.setAdapter(mAdapter);

        ClickableSpan termsOfUseClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SelectWorkPlaceActivity.this, "Read this", Toast.LENGTH_SHORT).show();
            }
        };

        Utils.makeLinks(tvReadThis, new String[]{"Read this"}, new ClickableSpan[]{termsOfUseClick});
    }

    @OnClick({R.id.img_back, R.id.tv_cancel, R.id.btn_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_cancel:
                gotoMainActivity(Constants.TAB_HOME);
                break;
            case R.id.btn_next:
                if (workArrayIds != null && workArrayIds.size() > 0) {
                    if (workArrayIds.size() == 2) {
                        updateWorkbase("2");
                    } else {
                        if (workArrayIds.size() == 0) {
                            updateWorkbase("3");
                        } else {
                            updateWorkbase(String.valueOf(workArrayIds.keyAt(0)));
                        }
                    }
                } else {
                    validationError("Please select atleast one workplace");
                }
                break;
        }
    }

    public void updateWorkbase(String workbaseIds) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().updateWorkBase(getUserID(), workbaseIds, getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    redirectActivity(LanguagesActivity.class);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update workbase failed");
            }
        });
    }

    @Override
    public void bindView(View view, final int position) {
        TextView tvPlace = view.findViewById(R.id.tv_place);
        tvPlace.setText(workPlaceList.get(position));

        final SegmentedButtonGroup segmentedButtonGroup = view.findViewById(R.id.segmentGroup);
        final SegmentedButton tabNo = view.findViewById(R.id.tab_no);
        final SegmentedButton tabYes = view.findViewById(R.id.tab_yes);

        segmentedButtonGroup.setOnPositionChangedListener(new SegmentedButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(int btnPosition) {
                if (btnPosition == 0) {
                    workArrayIds.delete(position);
                    tabYes.setTypeface(Constants.SFTEXT_REGULAR);
                    tabNo.setTypeface(Constants.SFTEXT_BOLD);
                    segmentedButtonGroup.setSelectorColor(ContextCompat.getColor(SelectWorkPlaceActivity.this, R.color.red_dark));
                } else {
                    workArrayIds.put(position, true);
                    tabNo.setTypeface(Constants.SFTEXT_REGULAR);
                    tabYes.setTypeface(Constants.SFTEXT_BOLD);
                    segmentedButtonGroup.setSelectorColor(ContextCompat.getColor(SelectWorkPlaceActivity.this, R.color.colorPrimaryDark));
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
