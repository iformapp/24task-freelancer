package com.task24.ui.workprofile;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.textview.TextViewSFTextBold;
import android.textview.TextViewSFTextRegular;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.task24.R;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectExperienceActivity extends BaseActivity {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.ll_beginner)
    LinearLayout llBeginner;
    @BindView(R.id.ll_intermediate)
    LinearLayout llIntermediate;
    @BindView(R.id.ll_advanced)
    LinearLayout llAdvanced;
    @BindView(R.id.btn_next)
    ImageView btnNext;
    @BindView(R.id.tv_beginner)
    TextViewSFTextBold tvBeginner;
    @BindView(R.id.tv_beginner_text)
    TextViewSFTextRegular tvBeginnerText;
    @BindView(R.id.tv_intermediate)
    TextViewSFTextBold tvIntermediate;
    @BindView(R.id.tv_intermediate_text)
    TextViewSFTextRegular tvIntermediateText;
    @BindView(R.id.tv_advanced)
    TextViewSFTextBold tvAdvanced;
    @BindView(R.id.tv_advanced_text)
    TextViewSFTextRegular tvAdvancedText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_experience);
        ButterKnife.bind(this);

        progress.setProgress(30);
    }

    @OnClick({R.id.img_back, R.id.tv_cancel, R.id.ll_beginner, R.id.ll_intermediate, R.id.ll_advanced, R.id.btn_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_cancel:
                gotoMainActivity(Constants.TAB_HOME);
                break;
            case R.id.ll_beginner:
                btnNext.setVisibility(View.VISIBLE);
                whiteText(tvBeginner, tvBeginnerText);
                blackText(tvIntermediate, tvIntermediateText, tvAdvanced, tvAdvancedText);
                setBlueBg(llBeginner);
                setWhiteBg(llAdvanced, llIntermediate);
                break;
            case R.id.ll_intermediate:
                btnNext.setVisibility(View.VISIBLE);
                whiteText(tvIntermediate, tvIntermediateText);
                blackText(tvBeginner, tvBeginnerText, tvAdvanced, tvAdvancedText);
                setBlueBg(llIntermediate);
                setWhiteBg(llAdvanced, llBeginner);
                break;
            case R.id.ll_advanced:
                btnNext.setVisibility(View.VISIBLE);
                whiteText(tvAdvanced, tvAdvancedText);
                blackText(tvIntermediate, tvIntermediateText, tvBeginner, tvBeginnerText);
                setBlueBg(llAdvanced);
                setWhiteBg(llBeginner, llIntermediate);
                break;
            case R.id.btn_next:
                redirectActivity(UpdateLocationActivity.class);
                break;
        }
    }

    private void blackText(TextView... textViews) {
        for (TextView textView : textViews) {
            textView.setTextColor(ContextCompat.getColor(this, R.color.black));
        }
    }

    private void whiteText(TextView... textViews) {
        for (TextView textView : textViews) {
            textView.setTextColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    private void setBlueBg(View... views) {
        for (View v : views) {
            v.setBackground(ContextCompat.getDrawable(this, R.drawable.blue_rounded_corner_10));
        }
    }

    private void setWhiteBg(View... views) {
        for (View v : views) {
            v.setBackground(ContextCompat.getDrawable(this, R.drawable.white_rounded_corner_10));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
