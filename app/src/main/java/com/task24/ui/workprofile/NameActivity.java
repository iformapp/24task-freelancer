package com.task24.ui.workprofile;

import android.button.ButtonSFTextBold;
import android.edittext.EditTextSFTextRegular;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.task24.R;
import com.task24.model.GeneralModel;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NameActivity extends BaseActivity {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.et_firstname)
    EditTextSFTextRegular etFirstname;
    @BindView(R.id.et_lastname)
    EditTextSFTextRegular etLastname;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.btn_next)
    ButtonSFTextBold btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        ButterKnife.bind(this);

        progress.setProgress(0);

        imgBack.setVisibility(View.GONE);
    }

    @OnClick({R.id.img_back, R.id.tv_cancel, R.id.btn_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_cancel:
                gotoMainActivity(Constants.TAB_HOME);
                break;
            case R.id.btn_next:
                if (isValid())
                    updateName();
                break;
        }
    }

    public String getFirstName() {
        return etFirstname.getText().toString().trim();
    }

    public String getLastName() {
        return etLastname.getText().toString().trim();
    }

    public void updateName() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().updateName(getUserID(), getFirstName(), getLastName(), getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    Preferences.getUserData(NameActivity.this).firstName = getFirstName();
                    Preferences.getUserData(NameActivity.this).lastName = getLastName();
                    redirectActivity(SelectExpertiseActivity.class);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update name failed");
            }
        });
    }

    public boolean isValid() {
        if (isEmpty(getFirstName())) {
            validationError("Please enter first name");
            return false;
        }

        if (isEmpty(getLastName())) {
            validationError("Please enter last name");
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
