package com.task24.ui.workprofile;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewSFTextRegular;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

import com.task24.R;
import com.task24.adapter.SkillsAdapter;
import com.task24.model.GeneralModel;
import com.task24.model.Profile;
import com.task24.model.Skill;
import com.task24.ui.BaseActivity;
import com.task24.util.EqualSpacingItemDecoration;
import com.task24.util.MyDownloadManager;
import com.task24.util.Preferences;
import com.task24.util.Utils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v4.content.FileProvider.getUriForFile;

public class ProfessionalInfoActivity extends BaseActivity implements BaseActivity.OnProfileLoadListener {

    @BindView(R.id.tv_headline)
    TextViewSFTextRegular tvHeadline;
    @BindView(R.id.tv_summary)
    TextViewSFTextRegular tvSummary;
    @BindView(R.id.rv_employment)
    RecyclerView rvEmployment;
    @BindView(R.id.rv_education)
    RecyclerView rvEducation;
    @BindView(R.id.tv_resume)
    TextViewSFTextRegular tvResume;

    private ArrayList<Skill> employmentList;
    private ArrayList<Skill> educationList;
    private Profile.Data profileData;
    private String resumeUrl;
    private boolean isUploadFile = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professional_info);
        ButterKnife.bind(this);

        rvEmployment.setLayoutManager(new LinearLayoutManager(this));
        rvEducation.setLayoutManager(new LinearLayoutManager(this));

        rvEmployment.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));
        rvEducation.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));

        refreshViews();

        rvEmployment.setNestedScrollingEnabled(false);
        rvEducation.setNestedScrollingEnabled(false);

        setOnProfileLoadListener(this);
    }

    private void refreshViews() {
        profileData = Preferences.getProfileData(this);
        if (profileData.headline != null)
            tvHeadline.setText(profileData.headline.content);
        if (profileData.summary != null)
            tvSummary.setText(profileData.summary.content);

        getEmploymentData();
        SkillsAdapter mLanguageAdapter = new SkillsAdapter(this, employmentList);
        rvEmployment.setAdapter(mLanguageAdapter);

        getEducationData();
        SkillsAdapter mExpertiseAdapter = new SkillsAdapter(this, educationList);
        rvEducation.setAdapter(mExpertiseAdapter);

        if (profileData.resume != null) {
            resumeUrl = profileData.resume.file;
            tvResume.setText("Resume Uploaded");
        }
    }

    private void getEmploymentData() {
        employmentList = new ArrayList<>();
        if (profileData.experiences != null && profileData.experiences.size() > 0) {
            for (int i = 0; i < profileData.experiences.size(); i++) {
                if (!isEmpty(profileData.experiences.get(i).companyName)) {
                    Profile.Experiences experiences = profileData.experiences.get(i);
                    if (!isEmpty(experiences.startDate)) {
                        String sDate = Utils.changeDateFormat("yyyy-MM-dd'T'HH:mm:ss", "MMM yyyy", experiences.startDate);
                        if (isEmpty(experiences.endDate)) {
                            employmentList.add(new Skill(experiences.companyName, sDate + " - Present"));
                        } else {
                            String eDate = Utils.changeDateFormat("yyyy-MM-dd'T'HH:mm:ss", "MMM yyyy", experiences.endDate);
                            employmentList.add(new Skill(experiences.companyName, sDate + " - " + eDate));
                        }
                    }
                }
            }
        }
    }

    private void getEducationData() {
        educationList = new ArrayList<>();
        if (profileData.educations != null && profileData.educations.size() > 0) {
            for (int i = 0; i < profileData.educations.size(); i++) {
                Profile.Educations educations = profileData.educations.get(i);
                if (!isEmpty(educations.startDate) && !isEmpty(educations.endDate)) {
                    String sDate = Utils.changeDateFormat("yyyy-MM-dd'T'HH:mm:ss", "MMM yyyy", educations.startDate);
                    String eDate = Utils.changeDateFormat("yyyy-MM-dd'T'HH:mm:ss", "MMM yyyy", educations.endDate);
                    educationList.add(new Skill(educations.schoolName + " - " + educations.degree, sDate + " - " + eDate));
                }
            }
        }
    }

    @OnClick({R.id.img_back, R.id.ll_headline, R.id.ll_summary, R.id.ll_employment, R.id.ll_education, R.id.ll_resume})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.ll_headline:
                redirectActivity(HeadlinesActivity.class);
                break;
            case R.id.ll_summary:
                redirectActivity(SummaryActivity.class);
                break;
            case R.id.ll_employment:
                redirectActivity(EmploymentEditActivity.class);
                break;
            case R.id.ll_education:
                redirectActivity(EducationEditActivity.class);
                break;
            case R.id.ll_resume:
                showOptionDialog();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isUploadFile)
            getProfile();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }

    @Override
    public void onProfileLoad(Profile.Data data) {
        if (data != null) {
            refreshViews();
        }
    }

    private void showOptionDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_file_option_menu);
        dialog.setCancelable(true);

        View llView = dialog.findViewById(R.id.ll_view);
        View llDownload = dialog.findViewById(R.id.ll_download);
        View llUpload = dialog.findViewById(R.id.ll_upload);
        TextView btnCancel = dialog.findViewById(R.id.btn_cancel);

        if (isEmpty(resumeUrl)) {
            llDownload.setVisibility(View.GONE);
            llView.setVisibility(View.GONE);
        }

        llView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(false, false);
            }
        });

        llDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(true, false);
            }
        });

        llUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(false, true);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    private void downloadFile(final boolean isDownload) {
        File folder = new File(Environment.getExternalStorageDirectory(), "/Download/" + getString(R.string.app_name));
        if (!folder.exists())
            folder.mkdir();

        String fileName = Utils.getFileNameFromUrl(resumeUrl);
        final File file = new File(folder, fileName);
        if (!file.exists()) {
            showProgress();

            MyDownloadManager downloadManager = new MyDownloadManager(this)
                    .setDownloadUrl(resumeUrl)
                    .setTitle(fileName)
                    .setDestinationUri(file)
                    .setDownloadCompleteListener(new MyDownloadManager.DownloadCompleteListener() {
                        @Override
                        public void onDownloadComplete() {
                            hideProgress();
                            showOutput("Download complete", isDownload, file);
                        }

                        @Override
                        public void onDownloadFailure() {
                            hideProgress();
                            toastMessage("Download failed");
                        }
                    });
            downloadManager.startDownload();
        } else {
            showOutput("Already Downloaded", isDownload, file);
        }
    }

    private void showOutput(String message, boolean isDownload, File file) {
        if (!isDownload) {
            viewFile(file);
        } else {
            toastMessage(message);
        }
    }

    private void checkPermission(final boolean isDownload, final boolean isUpload) {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            if (isUpload) {
                                isUploadFile = true;
                                Intent intent = new Intent(ProfessionalInfoActivity.this, NormalFilePickActivity.class);
                                intent.putExtra(Constant.MAX_NUMBER, 1);
                                intent.putExtra(NormalFilePickActivity.SUFFIX, new String[]{"doc", "docx", "ppt", "pptx", "pdf"});
                                startActivityForResult(intent, Constant.REQUEST_CODE_PICK_FILE);
                            } else {
                                downloadFile(isDownload);
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            toastMessage("Please give permission");
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_FILE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<NormalFile> docPaths = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                    if (docPaths != null && docPaths.size() > 0) {
                        Log.e("Doc Path == > ", docPaths.get(0).getPath());
                        updateResume(new File(docPaths.get(0).getPath()));
                    } else {
                        toastMessage("File not selected");
                    }
                }
                break;
        }
    }

    public void updateResume(File file) {
        if (!isNetworkConnected())
            return;

        showProgress();

        MultipartBody.Part body = null;
        if (file != null) {
            Uri selectedUri = Uri.fromFile(file);
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());

            RequestBody requestFile = null;
            if (mimeType != null) {
                requestFile = RequestBody.create(MediaType.parse(mimeType), file);
            }

            if (requestFile != null) {
                body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
            }
        }

        RequestBody profileId = RequestBody.create(MultipartBody.FORM, String.valueOf(getUserID()));
        RequestBody jwt = RequestBody.create(MultipartBody.FORM, getJWT());

        Call<GeneralModel> call = getService().addResume(profileId, body, jwt);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                hideProgress();
                if (checkStatus(response.body())) {
                    getProfile();
                    isUploadFile = false;
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update Resume failed");
            }
        });
    }
}
