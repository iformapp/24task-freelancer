package com.task24.ui.workprofile;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.textview.TextViewSFTextRegular;
import android.textview.TextViewSFTextSemiBold;
import android.view.View;
import android.widget.TextView;

import com.task24.R;
import com.task24.model.Profile;
import com.task24.ui.BaseActivity;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WorkProfileActivity extends BaseActivity implements BaseActivity.OnProfileLoadListener {

    @BindView(R.id.tv_title)
    TextViewSFTextSemiBold tvTitle;
    @BindView(R.id.tv_private_info)
    TextViewSFTextRegular tvPrivateInfo;
    @BindView(R.id.tv_skill)
    TextViewSFTextRegular tvSkill;
    @BindView(R.id.tv_professional_info)
    TextViewSFTextRegular tvProfessionalInfo;
    @BindView(R.id.tv_availability)
    TextViewSFTextRegular tvAvailability;
    @BindView(R.id.tv_verifications)
    TextViewSFTextRegular tvVerifications;
    @BindView(R.id.tv_profile_complete)
    TextViewSFTextSemiBold tvProfileComplete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_profile);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.profile));

        setOnProfileLoadListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Profile.Data profileData = Preferences.getProfileData(this);

        if (profileData != null) {
            onProfileLoad(profileData);
        }

        getProfile();
    }

    private void setPercentage(int percentage, TextView textView) {
        if (percentage >= 50) {
            textView.setBackground(ContextCompat.getDrawable(this, R.drawable.orange_rounded_corner));
        } else {
            textView.setBackground(ContextCompat.getDrawable(this, R.drawable.red_rounded_corner));
        }
        textView.setText(percentage + "%");
    }

    @OnClick({R.id.img_back, R.id.ll_private_info, R.id.ll_skill, R.id.ll_professional_info, R.id.ll_availability, R.id.ll_verifications})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.ll_private_info:
                redirectActivity(PrivateInfoActivity.class);
                break;
            case R.id.ll_skill:
                redirectActivity(EditViewSkillsActivity.class);
                break;
            case R.id.ll_professional_info:
                redirectActivity(ProfessionalInfoActivity.class);
                break;
            case R.id.ll_availability:
                redirectActivity(EditAvailabilityActivity.class);
                break;
            case R.id.ll_verifications:
                redirectActivity(VerificationActivity.class);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }

    @Override
    public void onProfileLoad(Profile.Data profileData) {
        if (profileData.percentage != null) {
            String profilePercentage = profileData.percentage.totalPercentage + "%";
            tvProfileComplete.setText(Utils.getColorString(this,
                    getString(R.string.your_profile_is, profilePercentage), profilePercentage, R.color.red));

            setPercentage(profileData.percentage.privateInfo, tvPrivateInfo);
            setPercentage(profileData.percentage.professionalInfo, tvProfessionalInfo);
            setPercentage(profileData.percentage.skill, tvSkill);
            setPercentage(profileData.percentage.verification, tvVerifications);
        }
    }
}
