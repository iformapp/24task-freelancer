package com.task24.ui.workprofile;

import android.os.Bundle;
import android.textview.TextViewSFDisplayBold;
import android.textview.TextViewSFDisplayRegular;
import android.textview.TextViewSFTextBold;
import android.textview.TextViewSFTextRegular;
import android.view.View;

import com.bumptech.glide.Glide;
import com.task24.R;
import com.task24.model.Profile;
import com.task24.ui.BaseActivity;
import com.task24.ui.BaseActivity.OnProfileLoadListener;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class WorkMoreActivity extends BaseActivity implements OnProfileLoadListener {

    @BindView(R.id.tv_username)
    TextViewSFDisplayBold tvUsername;
    @BindView(R.id.tv_email)
    TextViewSFDisplayRegular tvEmail;
    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    @BindView(R.id.tv_profile_complete)
    TextViewSFTextRegular tvProfileComplete;
    @BindView(R.id.tv_rate)
    TextViewSFTextBold tvRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_more);
        ButterKnife.bind(this);

        setOnProfileLoadListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Profile.Data profileData = Preferences.getProfileData(this);
        if (profileData != null) {
            onProfileLoad(profileData);
        }

        getProfile();
    }

    @OnClick({R.id.rl_profile, R.id.img_profile, R.id.ll_profile, R.id.rl_hourly_rate, R.id.rl_task_rpo, R.id.rl_feedback, R.id.rl_setting})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_profile:
            case R.id.rl_profile:
            case R.id.ll_profile:
                redirectActivity(WorkProfileActivity.class);
                break;
            case R.id.rl_hourly_rate:
                redirectActivity(EditRateActivity.class);
                break;
            case R.id.rl_task_rpo:
                redirectTab(Constants.TAB_TASK_PRO);
                break;
            case R.id.rl_feedback:
                showFeedbackDialog();
                break;
            case R.id.rl_setting:
                redirectActivity(WorkSettingActivity.class);
                break;
        }
    }

    @Override
    public void onProfileLoad(Profile.Data data) {
        tvUsername.setText("Hi, " + data.username);
        if (data.email != null)
            tvEmail.setText(data.email);
        if (data.photo != null)
            Glide.with(this).load(data.photo.img).into(imgProfile);
        tvRate.setText("$" + Utils.numberFormat(String.valueOf(data.payRate), 2));

        if (data.percentage != null) {
            String profilePercentage = data.percentage.totalPercentage + "%";
            tvProfileComplete.setText(Utils.getColorString(this,
                    getString(R.string.percent_complete, profilePercentage), profilePercentage, R.color.red));
        }
    }
}
