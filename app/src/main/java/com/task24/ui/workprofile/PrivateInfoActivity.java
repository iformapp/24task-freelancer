package com.task24.ui.workprofile;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.edittext.EditTextSFTextRegular;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.textview.TextViewSFTextBold;
import android.textview.TextViewSFTextRegular;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.task24.R;
import com.task24.ccp.CountryCodePicker;
import com.task24.model.PrivateInfo;
import com.task24.model.Profile;
import com.task24.ui.BaseActivity;
import com.task24.ui.auth.UpdatePasswordActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;

public class PrivateInfoActivity extends BaseActivity {

    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    @BindView(R.id.et_firstname)
    EditTextSFTextRegular etFirstname;
    @BindView(R.id.et_lastname)
    EditTextSFTextRegular etLastname;
    @BindView(R.id.et_username)
    TextView etUsername;
    @BindView(R.id.et_email)
    EditTextSFTextRegular etEmail;
    @BindView(R.id.tv_phone_prefix)
    TextViewSFTextRegular tvPhonePrefix;
    @BindView(R.id.et_mobile)
    EditTextSFTextRegular etMobile;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.tv_save)
    TextViewSFTextBold tvSave;

    private File profileFile;
    private Profile.Data profileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_info);
        ButterKnife.bind(this);

        profileData = Preferences.getProfileData(this);
        tvSave.setVisibility(View.VISIBLE);

        if (profileData != null) {
            if (profileData.firstName != null)
                etFirstname.setText(profileData.firstName);
            if (profileData.lastName != null)
                etLastname.setText(profileData.lastName);
            if (profileData.username != null)
                etUsername.setText(profileData.username);
            if (profileData.email != null)
                etEmail.setText(profileData.email);
            if (profileData.photo != null)
                Glide.with(this).load(profileData.photo.img).into(imgProfile);
            if (profileData.contactno != null) {
                String[] split = profileData.contactno.split("\\.");
                if (split.length == 2) {
                    etMobile.setText(split[1]);
                    tvPhonePrefix.setText(split[0]);
                    String code = split[0].replace("+", "");
                    ccp.setCountryForPhoneCode(Integer.parseInt(code));
                }
            }
        }

        ccp.registerCarrierNumberEditText(etMobile);
        addTextChangeEvent(etFirstname, etLastname, etEmail, etMobile);

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                tvSave.setVisibility(View.VISIBLE);
                tvPhonePrefix.setText(ccp.getSelectedCountryCodeWithPlus());
            }
        });
    }

    public void addTextChangeEvent(EditText... editTexts) {
        for (EditText edittext : editTexts) {
            edittext.addTextChangedListener(textWatcher);
        }
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            tvSave.setVisibility(View.VISIBLE);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public String getFirstName() {
        return etFirstname.getText().toString();
    }

    public String getLastName() {
        return etLastname.getText().toString();
    }

    public String getEmail() {
        return etEmail.getText().toString();
    }

    public String getMobile() {
        return etMobile.getText().toString();
    }

    public String getMobilePrefix() {
        return ccp.getSelectedCountryCodeWithPlus();
    }

    public String getCountryName() {
        return ccp.getSelectedCountryName().toLowerCase();
    }

    public String getCountryISOCode() {
        return ccp.getSelectedCountryNameCode().toLowerCase();
    }

    @OnClick({R.id.img_profile, R.id.tv_changepassword, R.id.img_back, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_save:
                updateProfile();
                break;
            case R.id.img_profile:
                checkPermission();
                break;
            case R.id.tv_changepassword:
                redirectActivity(UpdatePasswordActivity.class);
                break;
        }
    }

    public void checkPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent intent = new Intent(PrivateInfoActivity.this, ImagePickActivity.class);
                            intent.putExtra(IS_NEED_CAMERA, true);
                            intent.putExtra(Constant.MAX_NUMBER, 1);
                            startActivityForResult(intent, Constant.REQUEST_CODE_PICK_IMAGE);
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            toastMessage("Please give permission");
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_IMAGE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<ImageFile> imgPath = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                    if (imgPath != null && imgPath.size() > 0) {
                        tvSave.setVisibility(View.VISIBLE);
                        profileFile = new File(imgPath.get(0).getPath());
                        Glide.with(this).load(profileFile).into(imgProfile);
                    } else {
                        toastMessage("Image not selected");
                    }
                }
                break;
        }
    }

    public void updateProfile() {
        if (!isNetworkConnected())
            return;

        showProgress();

        MultipartBody.Part body = null;
        if (profileFile != null) {
            Uri selectedUri = Uri.fromFile(profileFile);
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());

            RequestBody requestFile = null;
            if (mimeType != null) {
                requestFile = RequestBody.create(MediaType.parse(mimeType), profileFile);
            }

            if (requestFile != null) {
                body = MultipartBody.Part.createFormData("profile", profileFile.getName(), requestFile);
            }
        }

        RequestBody first_name = RequestBody.create(MultipartBody.FORM, getFirstName());
        RequestBody last_name = RequestBody.create(MultipartBody.FORM, getLastName());
        RequestBody email = RequestBody.create(MultipartBody.FORM, getEmail());
        RequestBody contactNo = RequestBody.create(MultipartBody.FORM, getMobile());
        RequestBody mobilePrefix = RequestBody.create(MultipartBody.FORM, getMobilePrefix());
        RequestBody jwt = RequestBody.create(MultipartBody.FORM, getJWT());

        Call<Object> call = getService().updateProfile(getUserID(), first_name, last_name, email, contactNo, mobilePrefix, jwt, body);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                if (checkStatus(json)) {
                    PrivateInfo privateInfo = new Gson().fromJson(json, PrivateInfo.class);
                    if (privateInfo != null && checkStatus(privateInfo)) {
                        if (!isEmpty(privateInfo.data.profilePic)) {
                            Profile.Photo photo = new Profile.Photo();
                            photo.img = privateInfo.data.profilePic;
                            profileData.photo = photo;
                        }
                        profileData.email = getEmail();
                        profileData.contactno = getMobilePrefix() + "." + getMobile();
                        Preferences.setProfileData(PrivateInfoActivity.this, profileData);
                        Preferences.writeString(PrivateInfoActivity.this, Constants.JWT, privateInfo.data.jwt);
                        Toast.makeText(PrivateInfoActivity.this, "PrivateInfo Update Successfully", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("Update PrivateInfo failed");
            }
        });
    }

    public boolean validData() {
//        if (isEmpty(getFirstName())) {
//            validationError("Enter Name");
//            return false;
//        }
//
//        if (isEmpty(getMobile())) {
//            validationError("Enter Mobile no");
//            return false;
//        }
//
//        if (!ccp.isValidFullNumber()) {
//            validationError("Enter Valid Mobile no");
//            return false;
//        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
