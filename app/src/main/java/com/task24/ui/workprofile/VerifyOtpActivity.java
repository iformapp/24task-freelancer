package com.task24.ui.workprofile;

import android.edittext.EditTextSFDisplayRegular;
import android.os.Bundle;
import android.textview.TextViewSFTextRegular;
import android.view.View;

import com.task24.R;
import com.task24.model.GeneralModel;
import com.task24.model.Profile;
import com.task24.ui.BaseActivity;
import com.task24.util.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyOtpActivity extends BaseActivity {

    @BindView(R.id.et_otp)
    EditTextSFDisplayRegular etOtp;
    @BindView(R.id.tv_email)
    TextViewSFTextRegular tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);
        ButterKnife.bind(this);

        Profile.Data profileData = Preferences.getProfileData(this);
        if (profileData.email != null)
            tvEmail.setText(profileData.email);
    }

    public String getOtp() {
        return etOtp.getText().toString().trim();
    }

    @OnClick({R.id.img_back, R.id.tv_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_submit:
                if (isValid()) {
                    verifyOtp();
                }
                break;
        }
    }

    public void verifyOtp() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().verifyEmailOtp(getUserID(), getOtp());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null && checkStatus(response.body())) {
                    toastMessage(response.body().msg);
                    onBackPressed();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("verify email failed");
            }
        });
    }

    private boolean isValid() {
        if (isEmpty(getOtp())) {
            validationError(getString(R.string.enter_code));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
