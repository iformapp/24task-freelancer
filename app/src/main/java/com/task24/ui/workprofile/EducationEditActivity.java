package com.task24.ui.workprofile;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.textview.TextViewSFTextBold;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.task24.R;
import com.task24.adapter.EducationAdapter;
import com.task24.model.Education;
import com.task24.model.GeneralModel;
import com.task24.model.Profile;
import com.task24.ui.BaseActivity;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EducationEditActivity extends BaseActivity {

    @BindView(R.id.tv_save)
    TextViewSFTextBold tvSave;
    @BindView(R.id.rv_education)
    RecyclerView rvEducation;
    @BindView(R.id.rl_edit)
    RelativeLayout rlEdit;

    private List<Education> educationList;
    private EducationAdapter educationAdapter;
    private String degree, schoolName, startDate, endDate, levelIds;
    private Profile.Data profileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_education_edit);
        ButterKnife.bind(this);

        rlEdit.setVisibility(View.VISIBLE);

        educationList = new ArrayList<>();
        rvEducation.setLayoutManager(new LinearLayoutManager(this));
        rvEducation.setItemAnimator(new DefaultItemAnimator());

        educationAdapter = new EducationAdapter(this);
        rvEducation.setAdapter(educationAdapter);

        profileData = Preferences.getProfileData(this);
        if (profileData != null && profileData.educations != null && profileData.educations.size() > 0) {
            for (int i = 0; i < profileData.educations.size(); i++) {
                Profile.Educations educations = profileData.educations.get(i);
                Education model = new Education();
                model.college = educations.schoolName;
                model.degree = educations.degree;
                model.level = Utils.getEducationLevel(educations.level);
                if (!isEmpty(educations.startDate)) {
                    model.startYear = Utils.changeDateFormat("yyyy-MM-dd'T'HH:mm:ss", "MMM yyyy", educations.startDate);
                } else {
                    model.startYear = "";
                }

                if (!isEmpty(educations.endDate)) {
                    model.endYear = Utils.changeDateFormat("yyyy-MM-dd'T'HH:mm:ss", "MMM yyyy", educations.endDate);
                } else {
                    model.endYear = "";
                }
                educationList.add(model);
            }
        } else {
            educationList.add(new Education());
        }
        educationAdapter.doRefresh(educationList);
    }

    @OnClick({R.id.tv_edit_cancel, R.id.tv_save, R.id.tv_add_education})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_edit_cancel:
                onBackPressed();
                break;
            case R.id.tv_save:
                if (isValid()) {
                    addEducation();
                }
                break;
            case R.id.tv_add_education:
                if (educationAdapter != null) {
                    educationAdapter.addEducation(new Education());
                }
                break;
        }
    }

    public void addEducation() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().addEducations(getUserID(), degree, schoolName, startDate, endDate, levelIds, getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    finish();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("add education failed");
            }
        });
    }

    private boolean isValid() {
        StringBuilder degreeBuilder = null;
        StringBuilder schoolBuilder = null;
        StringBuilder startDateBuilder = null;
        StringBuilder endDateBuilder = null;
        StringBuilder levelBuilder = null;

        for (int i = 0; i < educationList.size(); i++) {
            View view = rvEducation.getChildAt(i);
            EditText school = view.findViewById(R.id.et_college);
            EditText degree = view.findViewById(R.id.et_degree);
            TextView tvLevel = view.findViewById(R.id.tv_level);
            TextView tvStartYear = view.findViewById(R.id.tv_start_year);
            TextView tvEndYear = view.findViewById(R.id.tv_end_year);

            if (isEmpty(degree.getText().toString())) {
                validationError("Please enter degree");
                return false;
            }
            if (isEmpty(school.getText().toString())) {
                validationError("Please enter college");
                return false;
            }
            if (isEmpty(tvLevel.getText().toString())) {
                validationError("Please select level");
                return false;
            }
            if (isEmpty(tvStartYear.getText().toString())) {
                validationError("Please select start date");
                return false;
            }
            if (isEmpty(tvEndYear.getText().toString())) {
                validationError("Please select end date");
                return false;
            }

            degreeBuilder = (degreeBuilder == null ? new StringBuilder("") : degreeBuilder.append("$")).append(degree.getText().toString());
            schoolBuilder = (schoolBuilder == null ? new StringBuilder("") : schoolBuilder.append("$")).append(school.getText().toString());
            String sDate, eDate;
            int syear = 0, smonth = 0, eyear = 0, emonth = 0;
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yyyy");
            Date fDate, tDate;
            try {
                fDate = dateFormat.parse(tvStartYear.getText().toString());
                tDate = dateFormat.parse(tvEndYear.getText().toString());
                smonth = Integer.parseInt((String) DateFormat.format("MM", fDate));
                syear = Integer.parseInt((String) DateFormat.format("yyyy", fDate));
                emonth = Integer.parseInt((String) DateFormat.format("MM", tDate));
                eyear = Integer.parseInt((String) DateFormat.format("yyyy", tDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            sDate = syear + "/" + smonth + "/12";
            eDate = eyear + "/" + emonth + "/12";
            startDateBuilder = (startDateBuilder == null ? new StringBuilder("") : startDateBuilder.append(",")).append(sDate);
            endDateBuilder = (endDateBuilder == null ? new StringBuilder("") : endDateBuilder.append(",")).append(eDate);
            int level = Utils.getEducationLevel(tvLevel.getText().toString());
            levelBuilder = (levelBuilder == null ? new StringBuilder("") : levelBuilder.append(",")).append(level);
        }
        degree = degreeBuilder != null ? degreeBuilder.toString() : "";
        schoolName = schoolBuilder != null ? schoolBuilder.toString() : "";
        startDate = startDateBuilder != null ? startDateBuilder.toString() : "";
        endDate = endDateBuilder != null ? endDateBuilder.toString() : "";
        levelIds = levelBuilder != null ? levelBuilder.toString() : "";
        return true;
    }

    @Override
    public void onBackPressed() {
        showDialog();
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Changes won't be save");
        builder.setCancelable(false);

        builder.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });

        builder.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
