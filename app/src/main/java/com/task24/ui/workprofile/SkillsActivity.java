package com.task24.ui.workprofile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.task24.R;
import com.task24.model.Skill;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SkillsActivity extends BaseActivity {

    @BindView(R.id.progress)
    ProgressBar progress;
//    @BindView(R.id.tag_group)
//    TagView tagGroup;
    @BindView(R.id.btn_next)
    ImageView btnNext;

    private int REQ_SKILL = 121;
    private ArrayList<Skill> selectedSkillList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skills);
        ButterKnife.bind(this);
        selectedSkillList = new ArrayList<>();
        progress.setProgress(20);

//        tagGroup.setOnTagDeleteListener(new TagView.OnTagDeleteListener() {
//            @Override
//            public void onTagDeleted(TagView tagView, Tag tag, int position) {
//                tagView.remove(position);
//            }
//        });
    }

    @OnClick({R.id.img_back, R.id.tv_cancel, R.id.btn_next, R.id.rl_select_skill})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_cancel:
                gotoMainActivity(Constants.TAB_HOME);
                break;
            case R.id.rl_select_skill:
                Intent i = new Intent(this, SelectSkillsActivity.class);
                startActivityForResult(i, REQ_SKILL);
                openToLeft();
                break;
            case R.id.btn_next:
                // TODO :: make api call and redirect
                redirectActivity(SelectExperienceActivity.class);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQ_SKILL) {
            if (data != null) {
                selectedSkillList = (ArrayList<Skill>) data.getSerializableExtra(Constants.SKILL_DATA);
//                ArrayList<Tag> tags = new ArrayList<>();
//                Tag tag;
//                if (selectedSkillList != null && selectedSkillList.size() > 0) {
//                    for (int i = 0; i < selectedSkillList.size(); i++) {
//                        tag = new Tag(selectedSkillList.get(i).skillTitle);
//                        tag.radius = 30f;
//                        tag.layoutColor = ContextCompat.getColor(SkillsActivity.this, R.color.colorPrimary);
//                        tag.isDeletable = true;
//                        tags.add(tag);
//                    }
//                    tagGroup.addTags(tags);
//
//                    btnNext.setVisibility(View.VISIBLE);
//                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
