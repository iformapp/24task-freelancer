package com.task24.ui.workprofile;

import android.edittext.EditTextSFDisplayRegular;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.task24.R;
import com.task24.model.GeneralModel;
import com.task24.ui.BaseActivity;
import com.task24.util.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SummaryActivity extends BaseActivity {

    @BindView(R.id.et_summary)
    EditTextSFDisplayRegular etSummary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        ButterKnife.bind(this);

        if (Preferences.getProfileData(this).summary != null)
            etSummary.setText(Preferences.getProfileData(this).summary.content);

        etSummary.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etSummary.setRawInputType(InputType.TYPE_CLASS_TEXT);
    }

    public String getSummary() {
        return etSummary.getText().toString().trim();
    }

    @OnClick({R.id.img_back, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_save:
                if (isValid()) {
                    updateSummay();
                }
                break;
        }
    }

    public void updateSummay() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().addSummay(getUserID(), getSummary(), getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    onBackPressed();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update summary failed");
            }
        });
    }

    private boolean isValid() {
        if (isEmpty(getSummary())) {
            validationError(getString(R.string.enter_your_summary));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
