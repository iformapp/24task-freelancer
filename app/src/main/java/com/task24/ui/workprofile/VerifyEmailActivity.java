package com.task24.ui.workprofile;

import android.edittext.EditTextSFDisplayRegular;
import android.os.Bundle;
import android.view.View;

import com.task24.R;
import com.task24.model.GeneralModel;
import com.task24.ui.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyEmailActivity extends BaseActivity {

    @BindView(R.id.et_email)
    EditTextSFDisplayRegular etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verify);
        ButterKnife.bind(this);
    }

    public String getEmail() {
        return etEmail.getText().toString().trim();
    }

    @OnClick({R.id.img_back, R.id.tv_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_submit:
                if (isValid()) {
                    varifyEmail();
                }
                break;
        }
    }

    public void varifyEmail() {
        if (!isNetworkConnected())
            return;

//        showProgress();
//
//        Call<GeneralModel> call = getService().addHeadlines(getUserID(), getEmail(), getJWT());
//        call.enqueue(new Callback<GeneralModel>() {
//            @Override
//            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
//                if (checkStatus(response.body())) {
//                    onBackPressed();
//                }
//                hideProgress();
//            }
//
//            @Override
//            public void onFailure(Call<GeneralModel> call, Throwable t) {
//                failureError("update headline failed");
//            }
//        });
    }

    private boolean isValid() {
        if (!isValidEmail(getEmail())) {
            validationError(getString(R.string.enter_valid_email));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
