package com.task24.ui.workprofile;

import android.button.ButtonSFTextBold;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewSFTextBold;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.task24.R;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.model.Language;
import com.task24.model.Profile;
import com.task24.model.ServicesModel;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.EqualSpacingItemDecoration;
import com.task24.util.Preferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectExpertiseActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.rv_skills)
    RecyclerView rvSkills;
    @BindView(R.id.btn_next)
    ButtonSFTextBold btnNext;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.rl_edit)
    RelativeLayout rlEdit;
    @BindView(R.id.tv_save)
    TextViewSFTextBold tvSave;

    //private List<String> skillList;
    private List<ServicesModel.Data> servicesList;
    private RecyclerviewAdapter mAdapter;
    private boolean isEdit = false;
    private SparseBooleanArray booleanArray;
    private static final int REQ_EDIT_EXPERIENCE = 102;
    private Profile.Data profileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_expertise);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            isEdit = getIntent().getBooleanExtra(Constants.IS_EDIT, false);
        }
        profileData = Preferences.getProfileData(this);
        booleanArray = new SparseBooleanArray();
        servicesList = Preferences.getTopServices(this);

        progress.setProgress(20);

        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        rvSkills.setLayoutManager(manager);
        rvSkills.addItemDecoration(new EqualSpacingItemDecoration(16));

        if (servicesList != null && servicesList.size() > 0) {
            setData();
        } else {
            getTopServiceList();
        }
    }

    private void setData() {
        if (isEdit) {
            rlEdit.setVisibility(View.VISIBLE);
            tvSave.setVisibility(View.GONE);
            header.setVisibility(View.GONE);
            btnNext.setVisibility(View.VISIBLE);

            if (profileData != null && profileData.expertise != null) {
                for (int i = 0; i < servicesList.size(); i++) {
                    for (int j = 0; j < profileData.expertise.size(); j++) {
                        Profile.Expertise experiences = profileData.expertise.get(j);
                        if (experiences.services.id == servicesList.get(i).id) {
                            servicesList.get(i).isSelected = true;
                        }
                    }
                }
            }
        }

        mAdapter = new RecyclerviewAdapter((ArrayList<?>) servicesList, R.layout.item_skills_edit, this);
        rvSkills.setAdapter(mAdapter);
        rvSkills.setFocusable(false);
    }

    @OnClick({R.id.img_back, R.id.tv_cancel, R.id.btn_next, R.id.tv_edit_cancel, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_cancel:
                gotoMainActivity(Constants.TAB_HOME);
                break;
            case R.id.tv_save:
            case R.id.btn_next:
                if (booleanArray.size() >= 1) {
                    StringBuilder serviceIds = null;
                    for (int i = 0; i < booleanArray.size(); i++) {
                        int key = booleanArray.keyAt(i);
                        serviceIds = (serviceIds == null ? new StringBuilder("") : serviceIds.append(",")).append(key);
                    }
                    Log.e("Service Ids", serviceIds == null ? "Blank" : serviceIds.toString());
                    Intent i = new Intent(this, WorkExperienceActivity.class);
                    i.putExtra(Constants.SERVICE_IDS, serviceIds == null ? "" : serviceIds.toString());
                    i.putExtra(Constants.IS_EDIT, isEdit);
                    if (isEdit)
                        startActivityForResult(i, REQ_EDIT_EXPERIENCE);
                    else
                        startActivity(i);
                    openToLeft();
                } else {
                    validationError("Please select your expertise");
                }
                break;
            case R.id.tv_edit_cancel:
                onBackPressed();
                break;
        }
    }

    @Override
    public void bindView(View view, final int position) {
        final TextView textView = view.findViewById(R.id.tv_skill);
        textView.setText(servicesList.get(position).name);

        if (servicesList.get(position).isSelected) {
            booleanArray.put(servicesList.get(position).id, true);
            textView.setBackground(ContextCompat.getDrawable(SelectExpertiseActivity.this, R.drawable.blue_button_bg));
            textView.setTextColor(Color.WHITE);
            textView.setTypeface(Typeface.createFromAsset(getAssets(), Constants.SFTEXT_BOLD));
        } else {
            booleanArray.delete(servicesList.get(position).id);
            textView.setBackground(ContextCompat.getDrawable(SelectExpertiseActivity.this, R.drawable.white_button_bg));
            textView.setTextColor(Color.BLACK);
            textView.setTypeface(Typeface.createFromAsset(getAssets(), Constants.SFTEXT_REGULAR));
        }

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (booleanArray.get(servicesList.get(position).id)) {
                    booleanArray.delete(servicesList.get(position).id);
                    textView.setBackground(ContextCompat.getDrawable(SelectExpertiseActivity.this, R.drawable.white_button_bg));
                    textView.setTextColor(Color.BLACK);
                    textView.setTypeface(Typeface.createFromAsset(getAssets(), Constants.SFTEXT_REGULAR));
                } else {
                    if (booleanArray.size() == 2) {
                        toastMessage("You can select only 2 expertise");
                        return;
                    }
                    booleanArray.put(servicesList.get(position).id, true);
                    textView.setBackground(ContextCompat.getDrawable(SelectExpertiseActivity.this, R.drawable.blue_button_bg));
                    textView.setTextColor(Color.WHITE);
                    textView.setTypeface(Typeface.createFromAsset(getAssets(), Constants.SFTEXT_BOLD));
                }
//                if (!isEdit)
                btnNext.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQ_EDIT_EXPERIENCE:
                    setResult(RESULT_OK);
                    finish();
                    break;
            }
        }
    }

    public void getTopServiceList() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<ServicesModel> call = getService().getTopServices();
        call.enqueue(new Callback<ServicesModel>() {
            @Override
            public void onResponse(Call<ServicesModel> call, Response<ServicesModel> response) {
                ServicesModel servicesModel = response.body();
                if (checkStatus(servicesModel)) {
                    Preferences.saveTopServices(getApplicationContext(), servicesModel.data);
                    servicesList = servicesModel.data;
                    setData();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ServicesModel> call, Throwable t) {
                failureError("get services failed");
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
