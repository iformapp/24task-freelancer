package com.task24.ui.workprofile;

import android.button.ButtonSFTextBold;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.style.ClickableSpan;
import android.textview.TextViewSFTextRegular;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.model.Available;
import com.task24.model.GeneralModel;
import com.task24.segment.SegmentedButton;
import com.task24.segment.SegmentedButtonGroup;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.EqualSpacingItemDecoration;
import com.task24.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AvailableForWorkActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.rv_available)
    RecyclerView rvAvailable;
    @BindView(R.id.tv_read_this)
    TextViewSFTextRegular tvReadThis;
    @BindView(R.id.btn_next)
    ButtonSFTextBold btnNext;

    private List<Available.Data> availableList;
    private RecyclerviewAdapter mAdapter;
    private SparseBooleanArray availableArrayIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_for_work);
        ButterKnife.bind(this);

        progress.setProgress(50);

        availableArrayIds = new SparseBooleanArray();

        getPayType();

        rvAvailable.setLayoutManager(new LinearLayoutManager(this));
        rvAvailable.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));

        ClickableSpan termsOfUseClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Toast.makeText(AvailableForWorkActivity.this, "Read this", Toast.LENGTH_SHORT).show();
            }
        };

        Utils.makeLinks(tvReadThis, new String[]{"Read this"}, new ClickableSpan[]{termsOfUseClick});
    }

    @OnClick({R.id.img_back, R.id.tv_cancel, R.id.btn_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_cancel:
                gotoMainActivity(Constants.TAB_HOME);
                break;
            case R.id.btn_next:
//                if (availableArrayIds != null && availableArrayIds.size() > 0) {
//                    StringBuilder availableIds = null;
//                    for (int i = 0; i < availableArrayIds.size(); i++) {
//                        int key = availableArrayIds.keyAt(i);
//                        availableIds = (availableIds == null ? new StringBuilder("") : availableIds.append(",")).append(key);
//                    }
//                    addPayTypes(availableIds == null ? "" : availableIds.toString());
//                } else {
//                    validationError("Please select atleast one availibility");
//                }
                redirectActivity(SelectWorkPlaceActivity.class);
                break;
        }
    }

    private void updatePayTypes(int payTypeId, int status) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().updatePayTypes(getUserID(), payTypeId, status, getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    btnNext.setVisibility(View.VISIBLE);
                }
                hideProgress();
        }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update paytype failed");
            }
        });
    }

    public void addPayTypes(String availableIds) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().addPayTypes(getUserID(), availableIds, getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    redirectActivity(SelectWorkPlaceActivity.class);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("add paytype failed");
            }
        });
    }

    public void getPayType() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Available> call = getService().getPayType();
        call.enqueue(new Callback<Available>() {
            @Override
            public void onResponse(Call<Available> call, Response<Available> response) {
                Available available = response.body();
                if (available != null) {
                    if (checkStatus(available)) {
                        availableList = available.data;

                        mAdapter = new RecyclerviewAdapter((ArrayList<?>) availableList,
                                R.layout.item_available_for_work, AvailableForWorkActivity.this);
                        rvAvailable.setAdapter(mAdapter);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Available> call, Throwable t) {
                failureError("get paytype failed");
            }
        });
    }

    @Override
    public void bindView(View view, final int position) {
        View line = view.findViewById(R.id.view);
        TextView tvSchedule = view.findViewById(R.id.tv_schedule);
        TextView tvAvailable = view.findViewById(R.id.tv_available);
        final SegmentedButtonGroup segmentedButtonGroup = view.findViewById(R.id.segmentGroup);
        final SegmentedButton tabNo = view.findViewById(R.id.tab_no);
        final SegmentedButton tabYes = view.findViewById(R.id.tab_yes);

        if (position == 0) {
            line.setVisibility(View.VISIBLE);
        } else {
            line.setVisibility(View.GONE);
        }

        tvSchedule.setText(availableList.get(position).name);
        tvAvailable.setText("(" + availableList.get(position).detail + ")");

        segmentedButtonGroup.setOnPositionChangedListener(new SegmentedButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(int btnPosition) {
                if (btnPosition == 0) {
                    //availableArrayIds.delete(availableList.get(position).id);
                    tabYes.setTypeface(Constants.SFTEXT_REGULAR);
                    tabNo.setTypeface(Constants.SFTEXT_BOLD);
                    segmentedButtonGroup.setSelectorColor(ContextCompat.getColor(AvailableForWorkActivity.this, R.color.red_dark));
                } else {
                    //availableArrayIds.put(availableList.get(position).id, true);
                    tabNo.setTypeface(Constants.SFTEXT_REGULAR);
                    tabYes.setTypeface(Constants.SFTEXT_BOLD);
                    segmentedButtonGroup.setSelectorColor(ContextCompat.getColor(AvailableForWorkActivity.this, R.color.colorPrimaryDark));
                }
                updatePayTypes(availableList.get(position).id, btnPosition);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
