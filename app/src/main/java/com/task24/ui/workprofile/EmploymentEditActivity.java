package com.task24.ui.workprofile;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.textview.TextViewSFTextBold;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.task24.R;
import com.task24.adapter.WorkAdapter;
import com.task24.model.GeneralModel;
import com.task24.model.Profile;
import com.task24.model.Work;
import com.task24.ui.BaseActivity;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmploymentEditActivity extends BaseActivity {

    @BindView(R.id.rv_work)
    RecyclerView rvWork;
    @BindView(R.id.rl_edit)
    RelativeLayout rlEdit;
    @BindView(R.id.tv_save)
    TextViewSFTextBold tvSave;

    private List<Work> workList;
    private WorkAdapter workAdapter;
    private Profile.Data profileData;
    private String companyName, jobTitle, startDate, endDate, experience;
    private boolean isNeedToUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employment_edit);
        ButterKnife.bind(this);

        rlEdit.setVisibility(View.VISIBLE);

        workList = new ArrayList<>();
        rvWork.setLayoutManager(new LinearLayoutManager(this));
        rvWork.setItemAnimator(new DefaultItemAnimator());

        workAdapter = new WorkAdapter(this);
        rvWork.setAdapter(workAdapter);

        profileData = Preferences.getProfileData(this);
        if (profileData != null && profileData.experiences != null && profileData.experiences.size() > 0) {
            for (int i = 0; i < profileData.experiences.size(); i++) {
                Profile.Experiences experiences = profileData.experiences.get(i);
                if (!isEmpty(experiences.companyName)) {
                    Work model = new Work();
                    model.company = experiences.companyName;
                    model.jobTitle = experiences.service.nameApp;
                    model.experience = Utils.getExperienceLevel(experiences.length);
                    if (!isEmpty(experiences.startDate)) {
                        model.startYear = Utils.changeDateFormat("yyyy-MM-dd'T'HH:mm:ss", "MMM yyyy", experiences.startDate);
                    } else {
                        model.startYear = "";
                    }

                    if (!isEmpty(experiences.endDate)) {
                        model.endYear = Utils.changeDateFormat("yyyy-MM-dd'T'HH:mm:ss", "MMM yyyy", experiences.endDate);
                        model.isCurrentlyWorking = false;
                    } else {
                        model.endYear = "";
                        model.isCurrentlyWorking = true;
                    }
                    workList.add(model);
                } else {
                    break;
                }
            }
        }

        if (workList.size() == 0) {
            workList.add(new Work());
        }
        workAdapter.doRefresh(workList);
    }

    @OnClick({R.id.tv_edit_cancel, R.id.tv_save, R.id.tv_add_work})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_edit_cancel:
                onBackPressed();
                break;
            case R.id.tv_save:
                if (isValid()) {
                    addWork();
                }
                break;
            case R.id.tv_add_work:
                if (workAdapter != null) {
                    workAdapter.addWork(new Work());
                }
                break;
        }
    }

    public void addWork() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().addEmployment(getUserID(), experience, jobTitle, startDate, endDate, companyName, getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    finish();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("add employment failed");
            }
        });
    }

    private boolean isValid() {
        StringBuilder companyBuilder = null;
        StringBuilder serviceIdBuilder = null;
        StringBuilder startDateBuilder = null;
        StringBuilder endDateBuilder = null;
        StringBuilder levelBuilder = null;

        for (int i = 0; i < workList.size(); i++) {
            View view = rvWork.getChildAt(i);
            EditText tvCompany = view.findViewById(R.id.et_company);
            TextView tvJobTitle = view.findViewById(R.id.tv_job_title);
            TextView tvExperience = view.findViewById(R.id.tv_experience);
            TextView tvStartYear = view.findViewById(R.id.tv_start_year);
            TextView tvEndYear = view.findViewById(R.id.tv_end_year);
            CheckBox chkWorking = view.findViewById(R.id.chk_working);

            if (isEmpty(tvCompany.getText().toString())) {
                validationError("Please enter company name");
                return false;
            }
            if (isEmpty(tvJobTitle.getText().toString())) {
                validationError("Please select job");
                return false;
            }
            if (isEmpty(tvExperience.getText().toString())) {
                validationError("Please select experience");
                return false;
            }
            if (isEmpty(tvStartYear.getText().toString())) {
                validationError("Please select start date");
                return false;
            }
            if (!chkWorking.isChecked() && isEmpty(tvEndYear.getText().toString())) {
                validationError("Please select end date");
                return false;
            }

            companyBuilder = (companyBuilder == null ? new StringBuilder("") : companyBuilder.append(",")).append(tvCompany.getText().toString());
            serviceIdBuilder = (serviceIdBuilder == null ? new StringBuilder("") : serviceIdBuilder.append(","))
                    .append(Utils.getServiceId(this, tvJobTitle.getText().toString()));
            String sDate, eDate;
            int syear = 0, smonth = 0, eyear = 0, emonth = 0;
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yyyy");
            Date fDate, tDate;
            try {
                fDate = dateFormat.parse(tvStartYear.getText().toString());
                smonth = Integer.parseInt((String) DateFormat.format("MM", fDate));
                syear = Integer.parseInt((String) DateFormat.format("yyyy", fDate));
                if (!chkWorking.isChecked()) {
                    tDate = dateFormat.parse(tvEndYear.getText().toString());
                    emonth = Integer.parseInt((String) DateFormat.format("MM", tDate));
                    eyear = Integer.parseInt((String) DateFormat.format("yyyy", tDate));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            sDate = syear + "/" + smonth + "/12";
            if (eyear == 0 && emonth == 0) {
                eDate = "";
            } else {
                eDate = eyear + "/" + emonth + "/12";
            }
            startDateBuilder = (startDateBuilder == null ? new StringBuilder("") : startDateBuilder.append(",")).append(sDate);
            endDateBuilder = (endDateBuilder == null ? new StringBuilder("") : endDateBuilder.append(",")).append(eDate);
            int level = Utils.getExperienceLevel(tvExperience.getText().toString());
            levelBuilder = (levelBuilder == null ? new StringBuilder("") : levelBuilder.append(",")).append(level);
        }
        companyName = companyBuilder != null ? companyBuilder.toString() : "";
        jobTitle = serviceIdBuilder != null ? serviceIdBuilder.toString() : "";
        startDate = startDateBuilder != null ? startDateBuilder.toString() : "";
        endDate = endDateBuilder != null ? endDateBuilder.toString() : "";
        experience = levelBuilder != null ? levelBuilder.toString() : "";
        return true;
    }

    @Override
    public void onBackPressed() {
        showDialog();
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Changes won't be save");
        builder.setCancelable(false);

        builder.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });

        builder.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
