package com.task24.ui.clientprofile;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewSFTextBold;
import android.view.View;
import android.widget.TextView;

import com.task24.R;
import com.task24.adapter.HomeItemsAdapter;
import com.task24.ui.BaseActivity;
import com.task24.ui.home.HomePagerModel;
import com.task24.util.Constants;
import com.task24.util.EqualSpacingItemDecoration;
import com.task24.util.Preferences;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;

public class ServicesActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.tv_right)
    TextView tvRight;
    @BindView(R.id.rv_services)
    RecyclerView rvServices;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.btn_pricing)
    TextViewSFTextBold btnPricing;
    @BindView(R.id.btn_free_trial)
    TextViewSFTextBold btnFreeTrial;

    private HomeItemsAdapter mAdapter;
    private String type;
    private ArrayList<HomePagerModel> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            type = getIntent().getStringExtra(Constants.SERVICE_TYPE);
        }

        tvRight.setText(getString(R.string.chat));

        switch (type) {
            case Constants.HIRE:
                tvToolbarTitle.setText(getString(R.string.our_service).toUpperCase());
                rvServices.setLayoutManager(new GridLayoutManager(this, 2));
                rvServices.addItemDecoration(new EqualSpacingItemDecoration(20));
                break;
            case Constants.WHY_US:
                tvToolbarTitle.setText(getString(R.string.why_us).toUpperCase());
                rvServices.setLayoutManager(new GridLayoutManager(this, 2));
                rvServices.addItemDecoration(new EqualSpacingItemDecoration(20));
                break;
            case Constants.HOW_IT_WORKS:
                tvToolbarTitle.setText(getString(R.string.how_it_works_).toUpperCase());
                rvServices.setLayoutManager(new LinearLayoutManager(this));
                rvServices.addItemDecoration(new EqualSpacingItemDecoration(20, EqualSpacingItemDecoration.VERTICAL));
                break;
        }

        arrayList = getList();
        mAdapter = new HomeItemsAdapter(this, arrayList, type, new HomeItemsAdapter.OnItemClick() {
            @Override
            public void onClickItem(HomePagerModel model) {
                switch (type) {
                    case Constants.HIRE:
                        Preferences.writeString(ServicesActivity.this, Constants.SERVICE_NAME, model.title);
                        gotoMainActivity(Constants.TAB_FREE_TRIAL);
                        break;
                    case Constants.WHY_US:
                        break;
                    case Constants.HOW_IT_WORKS:
                        break;
                }
            }
        });
        rvServices.setAdapter(mAdapter);
        rvServices.setNestedScrollingEnabled(false);

        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, 0);
            }
        }, 200);
    }

    @OnClick({R.id.ll_back, R.id.tv_right, R.id.btn_pricing, R.id.btn_free_trial})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_right:
                Intercom.client().displayMessageComposer();
                break;
            case R.id.btn_pricing:
                Preferences.writeString(ServicesActivity.this, Constants.SERVICE_NAME, "");
                gotoMainActivity(Constants.TAB_CALCULATE);
                break;
            case R.id.btn_free_trial:
                Preferences.writeString(ServicesActivity.this, Constants.SERVICE_NAME, "");
                gotoMainActivity(Constants.TAB_FREE_TRIAL);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }

    public ArrayList<HomePagerModel> getList() {
        ArrayList<HomePagerModel> arrayList = new ArrayList<>();
        TypedArray imgs = null;
        String[] stringArray = new String[0];
        switch (type) {
            case Constants.HIRE:
                imgs = getResources().obtainTypedArray(R.array.hire_images);
                stringArray = getResources().getStringArray(R.array.hire);
                break;
            case Constants.WHY_US:
                imgs = getResources().obtainTypedArray(R.array.why_us_images);
                stringArray = getResources().getStringArray(R.array.why_us);
                break;
            case Constants.HOW_IT_WORKS:
                imgs = getResources().obtainTypedArray(R.array.how_it_works_images);
                stringArray = getResources().getStringArray(R.array.how_it_works);
                break;
        }

        for (int i = 0; i < stringArray.length; i++) {
            HomePagerModel model = new HomePagerModel();
            model.title = stringArray[i];
            model.icon = imgs.getResourceId(i, -1);
            arrayList.add(model);
        }
        return arrayList;
    }
}
