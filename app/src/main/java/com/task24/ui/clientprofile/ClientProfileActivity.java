package com.task24.ui.clientprofile;

import android.edittext.EditTextSFTextRegular;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.textview.TextViewSFTextBold;
import android.textview.TextViewSFTextRegular;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.task24.R;
import com.task24.ccp.CountryCodePicker;
import com.task24.model.PrivateInfo;
import com.task24.model.Profile;
import com.task24.ui.BaseActivity;
import com.task24.ui.auth.UpdatePasswordActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientProfileActivity extends BaseActivity {

    @BindView(R.id.et_firstname)
    EditTextSFTextRegular etFirstname;
    @BindView(R.id.et_lastname)
    EditTextSFTextRegular etLastname;
    @BindView(R.id.tv_phone_prefix)
    TextViewSFTextRegular tvPhonePrefix;
    @BindView(R.id.et_mobile)
    EditTextSFTextRegular etMobile;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.tv_save)
    TextViewSFTextBold tvSave;
    @BindView(R.id.et_email)
    EditTextSFTextRegular etEmail;
    @BindView(R.id.tv_toolbar_title)
    TextViewSFTextBold tvToolbarTitle;
    @BindView(R.id.tv_changepassword)
    TextViewSFTextRegular tvChangepassword;

    private Profile.Data profileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_profile);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.edit_profile));
        tvSave.setVisibility(View.VISIBLE);

        profileData = Preferences.getProfileData(this);
        if (profileData == null) {
            toastMessage("data not found");
            finish();
            return;
        }

        if (profileData.firstName != null)
            etFirstname.setText(profileData.firstName);
        if (profileData.lastName != null)
            etLastname.setText(profileData.lastName);
        if (profileData.email != null)
            etEmail.setText(profileData.email);
        if (profileData.contactno != null) {
            String[] split = profileData.contactno.split("\\.");
            if (split.length == 2) {
                etMobile.setText(split[1]);
                tvPhonePrefix.setText(split[0]);
                String code = split[0].replace("+", "");
                try {
                    ccp.setCountryForPhoneCode(Integer.parseInt(code));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }

        ccp.registerCarrierNumberEditText(etMobile);
        addTextChangeEvent(etFirstname, etLastname, etEmail, etMobile);

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                tvSave.setVisibility(View.VISIBLE);
                tvPhonePrefix.setText(ccp.getSelectedCountryCodeWithPlus());
            }
        });
    }

    public String getFirstName() {
        return etFirstname.getText().toString();
    }

    public String getLastName() {
        return etLastname.getText().toString();
    }

    public String getEmail() {
        return etEmail.getText().toString();
    }

    public String getMobile() {
        return etMobile.getText().toString();
    }

    public String getMobilePrefix() {
        return ccp.getSelectedCountryCodeWithPlus();
    }

    public String getCountryName() {
        return ccp.getSelectedCountryName().toLowerCase();
    }

    public String getCountryISOCode() {
        return ccp.getSelectedCountryNameCode().toLowerCase();
    }

    public void addTextChangeEvent(EditText... editTexts) {
        for (EditText edittext : editTexts) {
            edittext.addTextChangedListener(textWatcher);
        }
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (tvSave.getVisibility() == View.GONE) {
                tvSave.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @OnClick({R.id.tv_changepassword, R.id.img_back, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_save:
                if (validData()) {
                    updateProfile();
                }
                break;
            case R.id.tv_changepassword:
                redirectActivity(UpdatePasswordActivity.class);
                break;
        }
    }

    public void updateProfile() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Object> call = getService().updateClientProfile(getUserID(), getFirstName(), getLastName(), getEmail(),
                getMobile(), getMobilePrefix(), getJWT());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String json = new Gson().toJson(response.body());
                if (checkStatus(json)) {
                    PrivateInfo privateInfo = new Gson().fromJson(json, PrivateInfo.class);
                    if (privateInfo != null && checkStatus(privateInfo)) {
                        if (!isEmpty(privateInfo.data.profilePic)) {
                            Profile.Photo photo = new Profile.Photo();
                            photo.img = privateInfo.data.profilePic;
                            profileData.photo = photo;
                        }
                        profileData.email = getEmail();
                        profileData.contactno = getMobilePrefix() + "." + getMobile();
                        Preferences.setProfileData(ClientProfileActivity.this, profileData);
                        Preferences.writeString(ClientProfileActivity.this, Constants.JWT, privateInfo.data.jwt);
                        Toast.makeText(ClientProfileActivity.this, "Profile Update Successfully", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("Update PrivateInfo failed");
            }
        });
    }

    public boolean validData() {
//        if (isEmpty(getFirstName())) {
//            validationError("Enter First Name");
//            return false;
//        }
//
//        if (isEmpty(getLastName())) {
//            validationError("Enter Last Name");
//            return false;
//        }
//
//        if (!isValidEmail(getEmail())) {
//            validationError("Enter Valid Email");
//            return false;
//        }
//
//        if (!ccp.isValidFullNumber()) {
//            validationError("Enter Valid Mobile no");
//            return false;
//        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
