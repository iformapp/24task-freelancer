package com.task24.ui.clientprofile;

import android.content.Intent;
import android.os.Bundle;
import android.textview.TextViewSFDisplayBold;
import android.textview.TextViewSFDisplayRegular;
import android.view.View;

import com.task24.R;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;

public class WhatWeDoActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.tv_right)
    TextViewSFDisplayRegular tvRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_what_we_do);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.what_we_do).toUpperCase());
        tvRight.setText(getString(R.string.chat));
    }

    @OnClick({R.id.rl_whatwecan, R.id.rl_whyus, R.id.rl_howitworks, R.id.ll_back, R.id.tv_right, R.id.card_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_whatwecan:
                Intent i = new Intent(this, ServicesActivity.class);
                i.putExtra(Constants.SERVICE_TYPE, Constants.HIRE);
                startActivity(i);
                openToLeft();
                break;
            case R.id.rl_whyus:
                i = new Intent(this, ServicesActivity.class);
                i.putExtra(Constants.SERVICE_TYPE, Constants.WHY_US);
                startActivity(i);
                openToLeft();
                break;
            case R.id.rl_howitworks:
                i = new Intent(this, ServicesActivity.class);
                i.putExtra(Constants.SERVICE_TYPE, Constants.HOW_IT_WORKS);
                startActivity(i);
                openToLeft();
                break;
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_right:
                Intercom.client().displayMessageComposer();
                break;
            case R.id.card_location:
                redirectUsingCustomTab("https://www.google.com/maps/place/24+Task+-+Virtual+Solutions/@14.0993311,122.953149,17z/data=!4m5!3m4!1s0x3398aef000000003:0x8e637de8f2c91be0!8m2!3d14.099578!4d122.955035?hl=en-US");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
