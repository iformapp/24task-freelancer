package com.task24.ui.clientprofile;

import android.os.Bundle;
import android.textview.TextViewSFDisplayBold;
import android.textview.TextViewSFDisplayRegular;
import android.textview.TextViewSFTextRegular;
import android.view.View;

import com.bumptech.glide.Glide;
import com.task24.R;
import com.task24.model.Profile;
import com.task24.ui.BaseActivity;
import com.task24.util.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ClientMoreActivity extends BaseActivity implements BaseActivity.OnProfileLoadListener {

    @BindView(R.id.tv_username)
    TextViewSFDisplayBold tvUsername;
    @BindView(R.id.tv_email)
    TextViewSFDisplayRegular tvEmail;
    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    @BindView(R.id.tv_profile_complete)
    TextViewSFTextRegular tvProfileComplete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_more);
        ButterKnife.bind(this);

        setOnProfileLoadListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Profile.Data profileData = Preferences.getProfileData(this);
        if (profileData != null) {
            onProfileLoad(profileData);
        }

        getProfile();
    }

    @OnClick({R.id.ll_profile, R.id.img_profile, R.id.rl_profile, R.id.rl_discount, R.id.rl_feedback, R.id.rl_what_we_do, R.id.rl_setting})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_profile:
            case R.id.img_profile:
            case R.id.rl_profile:
                redirectActivity(ClientProfileActivity.class);
                break;
            case R.id.rl_discount:
                redirectActivity(ShareDiscountActivity.class);
                break;
            case R.id.rl_feedback:
                showFeedbackDialog();
                break;
            case R.id.rl_what_we_do:
                redirectActivity(WhatWeDoActivity.class);
                break;
            case R.id.rl_setting:
                redirectActivity(ClientSettingActivity.class);
                break;
        }
    }

    @Override
    public void onProfileLoad(Profile.Data data) {
        tvUsername.setText("Hi, " + data.username);
        if (data.email != null)
            tvEmail.setText(data.email);
        if (data.photo != null)
            Glide.with(this).load(data.photo.img).into(imgProfile);

        if (data.percentage != null) {
            String profilePercentage = data.percentage.totalPercentage + "%";
            tvProfileComplete.setText(getString(R.string.percent_complete, profilePercentage));
        }
    }
}
