package com.task24.ui.clientprofile;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.textview.TextViewSFDisplayBold;
import android.textview.TextViewSFDisplayRegular;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.ui.BaseActivity;
import com.task24.ui.SelectAccountActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;

public class ClientSettingActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.tv_right)
    TextViewSFDisplayRegular tvRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_setting);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.setting).toUpperCase());
        tvRight.setText(getString(R.string.chat));
    }

    @OnClick({R.id.ll_back, R.id.rl_privacy_policy, R.id.rl_terms_of_use, R.id.rl_contact_us, R.id.rl_about_us, R.id.rl_faqs,
            R.id.rl_share_app, R.id.btn_signout, R.id.tv_right})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_right:
                Intercom.client().displayMessageComposer();
                break;
            case R.id.rl_privacy_policy:
                redirectUsingCustomTab(Constants.PRIVACY);
                break;
            case R.id.rl_terms_of_use:
                redirectUsingCustomTab(Constants.TERMS_USE);
                break;
            case R.id.rl_contact_us:
                showContactUsDialog();
                break;
            case R.id.rl_about_us:
                redirectUsingCustomTab(Constants.ABOUTUS);
                break;
            case R.id.rl_faqs:
                redirectUsingCustomTab(Constants.FAQS);
                break;
            case R.id.rl_share_app:
                shareApp();
                break;
            case R.id.btn_signout:
                showLogoutDialog();
                break;
        }
    }

    private void showLogoutDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.setCancelable(true);

        TextView tvMessage = dialog.findViewById(R.id.tv_message);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = dialog.findViewById(R.id.tv_chat_now);

        String s = getString(R.string.logout_msg);
        String[] words = {"Sign out?"};
        String[] fonts = {Constants.SFTEXT_BOLD};
        tvMessage.setText(Utils.getBoldString(this, s, fonts, null, words));

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(ClientSettingActivity.this, "Sign out successfully", Toast.LENGTH_SHORT).show();
                Intercom.client().logout();
                Preferences.saveUserData(ClientSettingActivity.this, null);
                Preferences.setProfileData(ClientSettingActivity.this, null);
                Preferences.writeBoolean(ClientSettingActivity.this, Constants.IS_LOGIN, false);

                Intent i = new Intent(ClientSettingActivity.this, SelectAccountActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                openToLeft();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
