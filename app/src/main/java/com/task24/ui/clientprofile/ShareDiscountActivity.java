package com.task24.ui.clientprofile;

import android.button.ButtonSFTextBold;
import android.content.Intent;
import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.textview.TextViewSFTextRegular;
import android.view.View;

import com.task24.R;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShareDiscountActivity extends BaseActivity {

    @BindView(R.id.btn_share_code)
    ButtonSFTextBold btnShareCode;
    @BindView(R.id.tv_discount_info)
    TextViewSFTextRegular tvDiscountInfo;

    private String shareCode = "R3RT2KK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_discount);
        ButterKnife.bind(this);

        ClickableSpan termsOfUseClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                redirectUsingCustomTab(Constants.TERMS_USE);
            }
        };

        Utils.makeLinks(tvDiscountInfo, new String[]{ "Terms of use." }, new ClickableSpan[]{ termsOfUseClick });
    }

    @OnClick({R.id.img_back, R.id.btn_share_code})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_share_code:
                // TODO :: remove sample content
                if (!isEmpty(shareCode)) {
                    String textBody = "24task Freelancer App";
                    shareReferral(textBody);
                }
                break;
        }
    }

    private void shareReferral(String textBody) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, textBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via..."));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
