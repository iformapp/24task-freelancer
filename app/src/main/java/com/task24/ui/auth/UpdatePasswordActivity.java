package com.task24.ui.auth;

import android.edittext.EditTextSFDisplayRegular;
import android.os.Bundle;
import android.view.View;

import com.task24.R;
import com.task24.model.GeneralModel;
import com.task24.ui.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePasswordActivity extends BaseActivity {

    @BindView(R.id.et_old_password)
    EditTextSFDisplayRegular etOldPassword;
    @BindView(R.id.et_new_password)
    EditTextSFDisplayRegular etNewPassword;
    @BindView(R.id.et_confirm_password)
    EditTextSFDisplayRegular etConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);
        ButterKnife.bind(this);
    }

    public String getOldPassword() {
        return etOldPassword.getText().toString().trim();
    }

    public String getNewPassword() {
        return etNewPassword.getText().toString().trim();
    }

    public String getConfirmPassword() {
        return etConfirmPassword.getText().toString().trim();
    }

    @OnClick({R.id.img_back, R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_save:
                if (isValid()) {
                    updatePassword();
                }
                break;
        }
    }

    public void updatePassword() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().updatePassword(getUserID(), getOldPassword(), getNewPassword(), getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                hideProgress();
                if (checkStatus(response.body())) {
                    toastMessage(response.body().msg);
                    onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update location failed");
            }
        });
    }

    private boolean isValid() {
        if (isEmpty(getOldPassword())) {
            validationError(getString(R.string.enter_your_old_password));
            return false;
        }

        if (isEmpty(getNewPassword())) {
            validationError(getString(R.string.enter_your_new_password));
            return false;
        }

        if (isEmpty(getConfirmPassword())) {
            validationError(getString(R.string.enter_confirm_password));
            return false;
        }

        if (!getNewPassword().equals(getConfirmPassword())) {
            validationError(getString(R.string.doesnt_match_password));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
