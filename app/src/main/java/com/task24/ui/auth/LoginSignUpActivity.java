package com.task24.ui.auth;

import android.app.Dialog;
import android.content.Intent;
import android.edittext.EditTextSFTextRegular;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.Gson;
import com.task24.R;
import com.task24.model.GeneralModel;
import com.task24.model.JWTData;
import com.task24.model.UserModel;
import com.task24.segment.SegmentedButton;
import com.task24.segment.SegmentedButtonGroup;
import com.task24.ui.BaseActivity;
import com.task24.ui.workprofile.NameActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginSignUpActivity extends BaseActivity {

    private static final int LOGIN = 1;
    private static final int SIGNUP = 0;

    @BindView(R.id.tab_signup)
    SegmentedButton tabSignup;
    @BindView(R.id.tab_login)
    SegmentedButton tabLogin;
    @BindView(R.id.segmentLoginGroup)
    SegmentedButtonGroup segmentLoginGroup;
    @BindView(R.id.et_email)
    EditTextSFTextRegular etEmail;
    @BindView(R.id.et_password)
    EditTextSFTextRegular etPassword;
    @BindView(R.id.ll_login)
    LinearLayout llLogin;
    @BindView(R.id.et_username)
    EditTextSFTextRegular etUsername;
    @BindView(R.id.et_s_password)
    EditTextSFTextRegular etSPassword;
    @BindView(R.id.ll_signup)
    LinearLayout llSignup;
    @BindView(R.id.fb_login)
    LoginButton fbLogin;
    @BindView(R.id.et_s_username)
    EditTextSFTextRegular etSUsername;
    @BindView(R.id.img_password)
    ImageView imgPassword;
    @BindView(R.id.img_l_password)
    ImageView imgLPassword;

    private boolean isLoginForm = false;
    private boolean isNeedToFinish = false;
    private CallbackManager callbackManager;
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 9001;
    private FirebaseAuth mAuth;
    private boolean isPasswordVisible = false;
    private boolean isLoginPasswordVisible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_sign_up);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();

        // For Fb Login
        callbackManager = CallbackManager.Factory.create();
        initFacebook();

        // For Google Login
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.google_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        if (getIntent() != null) {
            isLoginForm = getIntent().getBooleanExtra(Constants.FROM_LOGIN, false);
            isNeedToFinish = getIntent().getBooleanExtra(Constants.LOGIN_FINISH, false);
        }

        if (isLoginForm) {
            loginVisible();
        } else {
            signupVisible();
        }

        segmentLoginGroup.setOnPositionChangedListener(new SegmentedButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(int position) {
                if (position == LOGIN) {
                    isLoginForm = true;
                    loginVisible();
                } else if (position == SIGNUP) {
                    isLoginForm = false;
                    signupVisible();
                }
            }
        });

        segmentLoginGroup.setPosition(isLoginForm ? 1 : 0);
    }

    private void signupVisible() {
        tabLogin.setTypeface(Constants.SFTEXT_REGULAR);
        tabSignup.setTypeface(Constants.SFTEXT_BOLD);
        llLogin.setVisibility(View.GONE);
        llSignup.setVisibility(View.VISIBLE);
    }

    private void loginVisible() {
        tabLogin.setTypeface(Constants.SFTEXT_BOLD);
        tabSignup.setTypeface(Constants.SFTEXT_REGULAR);
        llLogin.setVisibility(View.VISIBLE);
        llSignup.setVisibility(View.GONE);
    }

    private void initFacebook() {
        fbLogin.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        System.out.println("onCancel");
                        hideProgress();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e("LoginActivity", exception.getCause().toString());
                        hideProgress();
                    }
                });
    }

    public String getEmail() {
        return etEmail.getText().toString();
    }

    public String getPassword() {
        return isLoginForm ? etPassword.getText().toString() : etSPassword.getText().toString();
    }

    private String getName() {
        return isLoginForm ? etUsername.getText().toString() : etSUsername.getText().toString();
    }

    public void login() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Object> call = getService().login(getName(), getPassword());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                afterLoginDataSetup(response, false);
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("login failed");
            }
        });
    }

    public void register() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Object> call = getService().register(getName(), getPassword(), getEmail(), getProfileTypeId());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                afterLoginDataSetup(response, true);
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("register failed");
            }
        });
    }

    public void socialLogin(String fbUserId, String googleUserId, String username, String firstName, String lastName, String email) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Object> call = getService().socialLogin(googleUserId, fbUserId, username, firstName, lastName, email, getProfileTypeId());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                afterLoginDataSetup(response, false);
                googleSignOut();
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("login failed");
                googleSignOut();
            }
        });
    }

    private void afterLoginDataSetup(Response<Object> response, boolean isSignUp) {
        String json = new Gson().toJson(response.body());
        if (checkStatus(json)) {
            JWTData jwtData = new Gson().fromJson(json, JWTData.class);
            if (jwtData != null && checkStatus(jwtData) && !isEmpty(jwtData.data)) {
                saveData(jwtData.data, isSignUp);
            } else {
                if (jwtData != null) {
                    failureError(jwtData.msg);
                }
            }
        }
    }

    private void saveData(String data, boolean isSignUp) {
        UserModel userModel = new Gson().fromJson(Utils.decode(data), UserModel.class);
        if (userModel.data != null && userModel.data.profileType != null && userModel.data.profileType.id != getProfileTypeId()) {
            if (userModel.data.profileType.id == Constants.CLIENT_PROFILE) {
                failureError("You are trying to login with client profile.");
            } else {
                failureError("You are trying to login with agent profile.");
            }
            return;
        }
        Preferences.writeBoolean(LoginSignUpActivity.this, Constants.IS_LOGIN, true);
        Preferences.saveUserData(LoginSignUpActivity.this, userModel.data);
        Preferences.writeString(LoginSignUpActivity.this, Constants.JWT, data);

        Intercom.client().setUserHash(generateHMACKey("sh_B0K7q5gnhvzj46rrYIZ_abGkTrP1cYjXydg09", String.valueOf(userModel.data.id)));
        Intercom.client().registerIdentifiedUser(Registration.create().withUserId(String.valueOf(userModel.data.id)));

        if (isSignUp) {
            if (isClientAccount()) {
                gotoMainActivity(Constants.TAB_HOME);
            } else {
                redirectActivity(NameActivity.class);
            }
        } else {
            if (isNeedToFinish)
                finish();
            else
                gotoMainActivity(Constants.TAB_HOME);
        }
    }

    private String generateHMACKey(String secret, String message) {
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            byte[] hash = (sha256_HMAC.doFinal(message.getBytes()));
            StringBuffer result = new StringBuffer();
            for (byte b : hash) {
                result.append(String.format("%02x", b));
            }
            Log.e("HMAC Key", result.toString());
            return result.toString();
        } catch (Exception e) {
            System.out.println("Error");
        }
        return "";
    }

    @OnClick({R.id.img_back, R.id.btn_login, R.id.btn_signup, R.id.tv_forgot_password, R.id.tv_google_login,
            R.id.tv_fb_login, R.id.tv_google_sign_up, R.id.tv_fb_sign_up, R.id.img_password, R.id.img_l_password})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_forgot_password:
                showForgotPasswordDialog();
                break;
            case R.id.btn_login:
                if (validLoginData()) {
                    login();
                }
                break;
            case R.id.btn_signup:
                if (validSignUpData()) {
                    register();
                }
                break;
            case R.id.tv_google_login:
            case R.id.tv_google_sign_up:
                showProgress();
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.tv_fb_login:
            case R.id.tv_fb_sign_up:
                showProgress();
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
                break;
            case R.id.img_password:
                if (!isPasswordVisible) {
                    // show password
                    etSPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    etSPassword.setSelection(getPassword().length());
                    imgPassword.setImageResource(R.drawable.show_password);
                } else {
                    // hide password
                    etSPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    etSPassword.setSelection(getPassword().length());
                    imgPassword.setImageResource(R.drawable.hide_password);
                }
                isPasswordVisible = !isPasswordVisible;
                break;
            case R.id.img_l_password:
                if (!isLoginPasswordVisible) {
                    // show password
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    etPassword.setSelection(getPassword().length());
                    imgLPassword.setImageResource(R.drawable.show_password);
                } else {
                    // hide password
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    etPassword.setSelection(getPassword().length());
                    imgLPassword.setImageResource(R.drawable.hide_password);
                }
                isLoginPasswordVisible = !isLoginPasswordVisible;
                break;
        }
    }

    public boolean validLoginData() {
        if (isEmpty(getName())) {
            validationError("Enter Username");
            return false;
        }

        if (isEmpty(getPassword())) {
            validationError("Enter Password");
            return false;
        }

        return true;
    }

    public boolean validSignUpData() {
        if (isEmpty(getName())) {
            validationError("Enter Username");
            return false;
        }

        if (!isValidEmail(getEmail())) {
            validationError("Enter Valid Email");
            return false;
        }

        if (isEmpty(getPassword())) {
            validationError("Enter Password");
            return false;
        }

        return true;
    }

    public void showForgotPasswordDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_forgot_password);
        dialog.setCancelable(true);
        Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        Button btnReset = dialog.findViewById(R.id.btn_reset);
        final EditText etEmail = dialog.findViewById(R.id.et_email);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidEmail(etEmail.getText().toString())) {
                    Utils.hideSoftKeyboard(LoginSignUpActivity.this);
                    forgotPassword(etEmail.getText().toString(), false);
                    dialog.dismiss();
                } else {
                    Toast.makeText(LoginSignUpActivity.this, "Enter valid email", Toast.LENGTH_SHORT).show();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void forgotPassword(final String email, final boolean isResend) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<JWTData> call = getService().forgotPassword(email);
        call.enqueue(new Callback<JWTData>() {
            @Override
            public void onResponse(Call<JWTData> call, Response<JWTData> response) {
                JWTData jwtData = response.body();
                if (checkStatus(jwtData) && !isEmpty(jwtData.data)) {
                    toastMessage(jwtData.msg);
                    //tempJwt = jwtData.data;
                    if (!isResend)
                        showSecurityCodeDialog(email);
                } else {
                    if (jwtData != null) {
                        failureError(jwtData.msg);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<JWTData> call, Throwable t) {
                failureError("forgot password failed");
            }
        });
    }

    public void showSecurityCodeDialog(final String email) {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_security_code);
        dialog.setCancelable(true);
        Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        Button btnReset = dialog.findViewById(R.id.btn_reset);
        final EditText etSecurityCode = dialog.findViewById(R.id.et_security_code);
        final EditText etNewPassword = dialog.findViewById(R.id.et_new_password);
        TextView tvResendCode = dialog.findViewById(R.id.tv_resend_code);
        tvResendCode.setPaintFlags(tvResendCode.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPassword(email, true);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(etSecurityCode.getText().toString())) {
                    validationError(getString(R.string.enter_code));
                    return;
                }

                if (isEmpty(etNewPassword.getText().toString())) {
                    validationError("Please enter password");
                    return;
                }

                Utils.hideSoftKeyboard(LoginSignUpActivity.this);
                resetPassword(email, etSecurityCode.getText().toString(), etNewPassword.getText().toString(), dialog);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void resetPassword(String email, String otp, String password, final Dialog dialog) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().resetPassword(password, otp, email);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        toastMessage(response.body().msg);
                        dialog.dismiss();
                    } else {
                        failureError(response.body().msg);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("reset password failed");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                if (account != null) {
                    firebaseAuthWithGoogle(account);
                }
            } catch (ApiException e) {
                Log.e("Google fails", e.getMessage());
                hideProgress();
            }
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {
                                Log.e("FirebaseGoogle Login", user.getUid());
                                String json = new Gson().toJson(acct);
                                Log.e("Google Response", json);
                                hideProgress();
                                socialLogin("", user.getUid(), acct.getDisplayName(), acct.getGivenName(),
                                        acct.getFamilyName(), acct.getEmail());
                            }
                        } else {
                            failureError(task.getException().getMessage());
                            Log.e("FirebaseGoogle Fails", task.getException().getMessage());
                        }
                    }
                });
    }

    private void handleFacebookAccessToken(final AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {
                                Log.e("FirebaseFB Login", user.getUid());
                                getGraphRequest(token, user.getUid());
                            }
                        } else {
                            failureError(task.getException().getMessage());
                            Log.e("FirebaseFB Fails", task.getException().getMessage());
                        }
                    }
                });
    }

    private void getGraphRequest(AccessToken token, final String uid) {
        GraphRequest request = GraphRequest.newMeRequest(
                token,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        hideProgress();
                        try {
                            if (object != null) {
                                String id = object.getString("id");
                                String name = "", first_name = "", last_name = "", email = "";
                                if (object.has("name")) {
                                    name = object.getString("name");
                                }
                                if (object.has("first_name")) {
                                    first_name = object.getString("first_name");
                                }
                                if (object.has("last_name")) {
                                    last_name = object.getString("last_name");
                                }
                                if (object.has("email")) {
                                    email = object.getString("email");
                                }
                                Log.e("Fb Response", object.toString());

                                socialLogin(uid, "", name, first_name, last_name, email);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,first_name,last_name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void googleSignOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
