package com.task24;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.task24.model.ServicesModel;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Task24Application extends MultiDexApplication {

    private BaseActivity activity;

    @Override
    public void onCreate() {
        super.onCreate();

        activity = new BaseActivity();

        Intercom.initialize(this, "android_sdk-59492598d6d65b76b06a22396d76da69dc6d3431", "ck9u1r6v");
        Registration registration = Registration.create();
        if (Preferences.getUserData(this) != null) {
            registration.withEmail(Preferences.getUserData(this).email);
            registration.withUserId(String.valueOf(Preferences.getUserData(this).id));
            Intercom.client().registerIdentifiedUser(registration);
        } else {
            Intercom.client().registerUnidentifiedUser();
        }

        Preferences.writeString(this, Constants.SERVICE_NAME, "");
        getAllServices();
        getTopServiceList();
        printHashKey();
    }

    public void getAllServices() {
        if (!isNetworkConnected())
            return;

        Call<ServicesModel> call = activity.getService().getServices();
        call.enqueue(new Callback<ServicesModel>() {
            @Override
            public void onResponse(Call<ServicesModel> call, Response<ServicesModel> response) {
                ServicesModel servicesModel = response.body();
                if (activity.checkStatus(servicesModel)) {
                    Preferences.saveServices(getApplicationContext(), servicesModel.data);
                }
            }

            @Override
            public void onFailure(Call<ServicesModel> call, Throwable t) {
                //Log.e("Fail", t.getMessage());
            }
        });
    }

    public void getTopServiceList() {
        if (!isNetworkConnected())
            return;

        Call<ServicesModel> call = activity.getService().getTopServices();
        call.enqueue(new Callback<ServicesModel>() {
            @Override
            public void onResponse(Call<ServicesModel> call, Response<ServicesModel> response) {
                ServicesModel servicesModel = response.body();
                if (activity.checkStatus(servicesModel)) {
                    Preferences.saveTopServices(getApplicationContext(), servicesModel.data);
                }
            }

            @Override
            public void onFailure(Call<ServicesModel> call, Throwable t) {
                //failureError("get services failed");
            }
        });
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null && cm.getActiveNetworkInfo() != null) {
            return true;
        }
        Toast.makeText(this, "connect to internet", Toast.LENGTH_SHORT).show();
        return false;
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e("Hash Key: ", hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("Hash Key: ", e.getMessage());
        } catch (Exception e) {
            Log.e("Hash Key: ", e.getMessage());
        }
    }
}
