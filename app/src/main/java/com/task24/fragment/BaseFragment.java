package com.task24.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.task24.fragment.postjob.PostJobFragment;
import com.task24.ui.BaseActivity;

public class BaseFragment extends Fragment {

    public BaseActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActivity) getActivity();
    }

    public void popOffSubscreens() {
        while (activity.getSupportFragmentManager().getBackStackEntryCount() > 0) {
            activity.getSupportFragmentManager().popBackStackImmediate();
        }
    }

    public void goBack() {
        activity.getSupportFragmentManager().popBackStack();
    }

    public void goTo(String className) {
        activity.getSupportFragmentManager().popBackStack(className, 0);
    }
}
