package com.task24.fragment.projects;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.task24.R;
import com.task24.adapter.ProjectsAdapter;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.fragment.BaseFragment;
import com.task24.model.Projects;
import com.task24.util.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectsListFragment extends BaseFragment {

    @BindView(R.id.rv_projects)
    RecyclerView rvProjects;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private boolean isWorkInProgress;
    private ArrayList<Projects> projectList;
    private ProjectsAdapter mAdapter;

    public static ProjectsListFragment newInstance(boolean isWorkInProgress) {
        ProjectsListFragment fragment = new ProjectsListFragment();
        Bundle args = new Bundle();
        args.putBoolean(Constants.IS_WORK_INPROGRESS, isWorkInProgress);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_projects_list, container, false);
        ButterKnife.bind(this, v);

        if (getArguments() != null) {
            isWorkInProgress = getArguments().getBoolean(Constants.IS_WORK_INPROGRESS);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        rvProjects.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvProjects.getContext(),
                linearLayoutManager.getOrientation());
        rvProjects.addItemDecoration(dividerItemDecoration);

        fillArray();
        setAdapter();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setAdapter();
            }
        });

        return v;
    }

    private void setAdapter() {
        if (projectList != null && projectList.size() > 0) {
            swipeRefreshLayout.setRefreshing(false);
            if (mAdapter == null) {
                mAdapter = new ProjectsAdapter(activity);
            }
            mAdapter.doRefresh(projectList);
            if (rvProjects.getAdapter() == null) {
                rvProjects.setAdapter(mAdapter);
            }
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void fillArray() {
        projectList = new ArrayList<>();
        projectList.add(new Projects("Write My articles", "6 days left", "35 bids", "$ 60", "Bidding"));
        projectList.add(new Projects("I need a professional Dev", "9 days left", "70 bids", "$ 500", "In progress"));
    }
}
