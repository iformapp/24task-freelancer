package com.task24.fragment.postjob;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.edittext.EditTextSFTextRegular;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.radiobox.RadioButtonTextRegular;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.textview.TextViewSFTextMedium;
import android.textview.TextViewSFTextRegular;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.task24.R;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.adapter.UploadFileAdapter;
import com.task24.fragment.BaseFragment;
import com.task24.model.Attachment;
import com.task24.model.GeneralModel;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.hoang8f.android.segmented.SegmentedGroup;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;

public class PostJobFragment extends BaseFragment implements RecyclerviewAdapter.OnViewBindListner, UploadFileAdapter.OnFileDeleteListener {

    @BindView(R.id.et_job_title)
    EditTextSFTextRegular etJobTitle;
    @BindView(R.id.et_describe)
    EditTextSFTextRegular etDescribe;
    @BindView(R.id.tv_attach_file)
    TextViewSFTextRegular tvAttachFile;
    @BindView(R.id.radio_fixed)
    RadioButtonTextRegular radioFixed;
    @BindView(R.id.radio_hourly)
    RadioButtonTextRegular radioHourly;
    @BindView(R.id.sg_project_type)
    SegmentedGroup sgProjectType;
    @BindView(R.id.tv_rates)
    TextViewSFTextMedium tvRates;
    @BindView(R.id.rv_skills)
    RecyclerView rvSkills;
    @BindView(R.id.rv_files)
    RecyclerView rvFiles;

    private View view;
    private boolean isFixedPrice;
    private ArrayList<String> skillsList;
    private ArrayList<String> skillsIdList;
    private RecyclerviewAdapter mAdapter;
    private ArrayList<Attachment> fileList;
    private String skillIds;
    private String skillNames;
    private String clientRate;
    private int clientRateId;
    private String budget;
    private String describe;
    private String attachFile;
    private UploadFileAdapter uploadFileAdapter;

    public static PostJobFragment newInstance() {
        PostJobFragment fragment = new PostJobFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            return view;
        }

        view = inflater.inflate(R.layout.fragment_post_job, container, false);
        ButterKnife.bind(this, view);

        fileList = new ArrayList<>();

        isFixedPrice = Preferences.readBoolean(activity, Constants.IS_FIXED_PRICE, false);
        if (isFixedPrice)
            radioFixed.setChecked(true);
        else
            radioHourly.setChecked(true);

        sgProjectType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = group.findViewById(checkedId);
                if (checkedRadioButton.getText().toString().equals(getString(R.string.fixed_price))) {
                    String[] budgetArray = getResources().getStringArray(R.array.budget_array);
                    tvRates.setText(budgetArray[0]);
                    Preferences.writeBoolean(activity, Constants.IS_FIXED_PRICE, true);
                    isFixedPrice = true;
                } else {
                    String[] budgetArray = getResources().getStringArray(R.array.hourly_array);
                    tvRates.setText(budgetArray[0]);
                    Preferences.writeBoolean(activity, Constants.IS_FIXED_PRICE, false);
                    isFixedPrice = false;
                }
            }
        });

        rvSkills.setLayoutManager(new LinearLayoutManager(activity));
        rvFiles.setLayoutManager(new LinearLayoutManager(activity));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        skillIds = Preferences.readString(activity, Constants.SKILL_IDS, "");
        skillNames = Preferences.readString(activity, Constants.SKILL_NAMES, "");
        clientRate = Preferences.readString(activity, Constants.CLIENT_RATE, "");
        clientRateId = Preferences.readInteger(activity, Constants.CLIENT_RATE_ID, 0);
        budget = Preferences.readString(activity, Constants.BUDGET, "");
        describe = Preferences.readString(activity, Constants.DESCRIBE, "");
        attachFile = Preferences.readString(activity, Constants.ATTACH_FILE, "");

        etDescribe.setText(describe);
        if (TextUtils.isEmpty(clientRate)) {
            tvRates.setText(isFixedPrice ? budget : budget + "/hr");
        } else {
            tvRates.setText(isFixedPrice ? clientRate : clientRate + "/hr");
        }

        if (!TextUtils.isEmpty(skillNames)) {
            String[] split = skillNames.split(",");
            skillsList = new ArrayList<>(Arrays.asList(split));
            setAdapter();
        }

        if (!TextUtils.isEmpty(skillIds)) {
            String[] split = skillIds.split(",");
            skillsIdList = new ArrayList<>(Arrays.asList(split));
        }

        if (!TextUtils.isEmpty(attachFile)) {
            String[] filesSplit = attachFile.split(",");
            for (String aFilesSplit : filesSplit) {
                if (aFilesSplit.contains("png") || aFilesSplit.contains("jpg") || aFilesSplit.contains("jpeg")) {
                    fileList.add(new Attachment(aFilesSplit, true));
                } else {
                    fileList.add(new Attachment(aFilesSplit, false));
                }
            }
            setFileAdapter(fileList);
        }
    }

    private String getDescription() {
        return etDescribe.getText().toString().trim();
    }

    private String getJobTitle() {
        return etJobTitle.getText().toString().trim();
    }

    private void setAdapter() {
        if (skillsList != null && skillsList.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new RecyclerviewAdapter(skillsList, R.layout.item_skills_post, this);
            }
            mAdapter.doRefresh(skillsList);
            if (rvSkills.getAdapter() == null) {
                rvSkills.setAdapter(mAdapter);
            }
        }
    }

    @Override
    public void bindView(View view, final int position) {
        TextView tvSkillName = view.findViewById(R.id.tv_skill_name);
        ImageView imgRemove = view.findViewById(R.id.img_remove);

        tvSkillName.setText(skillsList.get(position));
        imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skillsList.remove(position);
                skillsIdList.remove(position);
                mAdapter.doRefresh(skillsList);

                if (skillsList != null && skillsList.size() > 0) {
                    Preferences.writeString(activity, Constants.SKILL_NAMES, TextUtils.join(",", skillsList));
                    Preferences.writeString(activity, Constants.SKILL_IDS, TextUtils.join(",", skillsIdList));
                } else {
                    Preferences.writeString(activity, Constants.SKILL_NAMES, "");
                    Preferences.writeString(activity, Constants.SKILL_IDS, "");
                }
            }
        });
    }

    public void setFileAdapter(ArrayList<Attachment> uploadedfiles) {
        this.fileList = uploadedfiles;
        if (uploadedfiles != null && uploadedfiles.size() > 0) {
            if (uploadFileAdapter == null) {
                uploadFileAdapter = new UploadFileAdapter(activity);
                uploadFileAdapter.setOnFileDeleteListener(this);
            }

            uploadFileAdapter.doRefresh(uploadedfiles);

            if (rvFiles.getAdapter() == null) {
                rvFiles.setAdapter(uploadFileAdapter);
            }
        }
    }

    @Override
    public void onFileDelete(ArrayList<Attachment> mDataset) {
        setFileAdapter(mDataset);
    }

    @OnClick({R.id.img_back, R.id.tv_post_job, R.id.tv_attach_file, R.id.rl_budget, R.id.tv_add_skill})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(activity);
        switch (view.getId()) {
            case R.id.img_back:
                goBack();
                break;
            case R.id.tv_post_job:
                if (isValid()) {
                    addJobPost();
                }
                break;
            case R.id.tv_attach_file:
                selectFileDialog();
                break;
            case R.id.rl_budget:
                activity.replaceFragment(PriceRateFragment.newInstance(true));
                break;
            case R.id.tv_add_skill:
                activity.replaceFragment(ChooseSkillsFragment.newInstance(true));
                break;
        }
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(getJobTitle())) {
            activity.validationError("Please enter job title");
            return false;
        }

        if (TextUtils.isEmpty(getDescription())) {
            activity.validationError("Please enter job description");
            return false;
        }

        if (skillsList == null || skillsList.isEmpty()) {
            activity.validationError("Please select skill");
            return false;
        }
        return true;
    }

    private void addJobPost() {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        MultipartBody.Part[] body = null;
        if (fileList != null && fileList.size() > 0) {
            body = new MultipartBody.Part[fileList.size()];
            for (int i = 0; i < fileList.size(); i++) {
                File file = new File(fileList.get(i).filepath);
                Uri selectedUri = Uri.fromFile(file);
                String fileExtension = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());

                RequestBody requestFile = null;
                if (mimeType != null) {
                    requestFile = RequestBody.create(MediaType.parse(mimeType), file);
                }

                if (requestFile != null) {
                    body[i] = MultipartBody.Part.createFormData("file[]", file.getName(), requestFile);
                }
            }
        }

        RequestBody profileId = RequestBody.create(MultipartBody.FORM, String.valueOf(activity.getUserID()));
        RequestBody jwt = RequestBody.create(MultipartBody.FORM, activity.getJWT());
        RequestBody description = RequestBody.create(MultipartBody.FORM, getDescription());
        RequestBody title = RequestBody.create(MultipartBody.FORM, getJobTitle());
        RequestBody rateId;
        if (clientRateId == 0) {
            rateId = RequestBody.create(MultipartBody.FORM, "");
        } else {
            rateId = RequestBody.create(MultipartBody.FORM, String.valueOf(clientRateId));
        }
        RequestBody skillid = RequestBody.create(MultipartBody.FORM, skillIds);
        RequestBody price = RequestBody.create(MultipartBody.FORM, budget);
        RequestBody payTypeId = RequestBody.create(MultipartBody.FORM, isFixedPrice ? "1" : "2");

        Call<GeneralModel> call = activity.getService().addJobPost(profileId, body, jwt, description, rateId,
                skillid, price, payTypeId, title);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                activity.hideProgress();
                if (activity.checkStatus(response.body())) {
                    Toast.makeText(activity, response.body().msg, Toast.LENGTH_SHORT).show();
                    activity.gotoMainActivity(Constants.TAB_FREE_TRIAL);
                }
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("add job post failed");
            }
        });
    }

    public void selectFileDialog() {
        final Dialog dialog = new Dialog(activity, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_camera_document_select);
        dialog.setCancelable(true);
        TextView tvCancel = dialog.findViewById(R.id.btn_cancel);
        LinearLayout llCamera = dialog.findViewById(R.id.ll_camera);
        LinearLayout llDocument = dialog.findViewById(R.id.ll_document);

        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(false);
                dialog.dismiss();
            }
        });

        llDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(true);
                dialog.dismiss();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void checkPermission(final boolean isDocument) {
        Dexter.withActivity(activity)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            if (isDocument) {
                                Intent intent = new Intent(activity, NormalFilePickActivity.class);
                                intent.putExtra(Constant.MAX_NUMBER, 5);
                                intent.putExtra(NormalFilePickActivity.SUFFIX, new String[]{"xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf"});
                                startActivityForResult(intent, Constant.REQUEST_CODE_PICK_FILE);
                            } else {
                                Intent intent = new Intent(activity, ImagePickActivity.class);
                                intent.putExtra(IS_NEED_CAMERA, true);
                                intent.putExtra(Constant.MAX_NUMBER, 5);
                                startActivityForResult(intent, Constant.REQUEST_CODE_PICK_IMAGE);
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            Toast.makeText(activity, "Please give permission", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_FILE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<NormalFile> docPaths = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                    if (docPaths != null && docPaths.size() > 0) {
                        Log.e("Doc Path == > ", docPaths.get(0).getPath());
                        for (NormalFile file : docPaths) {
                            fileList.add(new Attachment(file.getPath(), false));
                        }
                        Preferences.writeString(activity, Constants.ATTACH_FILE, docPaths.get(0).getPath());
                        setFileAdapter(fileList);
                    } else {
                        Toast.makeText(activity, "File not selected", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case Constant.REQUEST_CODE_PICK_IMAGE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<ImageFile> imgPath = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                    if (imgPath != null && imgPath.size() > 0) {
                        Log.e("Image Path == > ", imgPath.get(0).getPath());
                        for (ImageFile file : imgPath) {
                            fileList.add(new Attachment(file.getPath(), true));
                        }
                        Preferences.writeString(activity, Constants.ATTACH_FILE, imgPath.get(0).getPath());
                        setFileAdapter(fileList);
                    } else {
                        Toast.makeText(activity, "File not selected", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }
}
