package com.task24.fragment.postjob;

import android.edittext.EditTextSFTextRegular;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.task24.R;
import com.task24.fragment.BaseFragment;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DescribeFragment extends BaseFragment {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.et_describe)
    EditTextSFTextRegular etDescribe;

    private View view;

    public static DescribeFragment newInstance() {
        DescribeFragment fragment = new DescribeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            return view;
        }

        view = inflater.inflate(R.layout.fragment_describe, container, false);
        ButterKnife.bind(this, view);

        progress.setProgress(75);
        return view;
    }

    @OnClick({R.id.img_back, R.id.tv_next})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(activity);
        switch (view.getId()) {
            case R.id.img_back:
                goBack();
                break;
            case R.id.tv_next:
                if (!TextUtils.isEmpty(etDescribe.getText().toString().trim())) {
                    Preferences.writeString(activity, Constants.DESCRIBE, etDescribe.getText().toString().trim());
                    activity.replaceFragment(new AttachmentFragment());
                } else {
                    activity.validationError("Please enter description");
                }
                break;
        }
    }
}
