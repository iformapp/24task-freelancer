package com.task24.fragment.postjob;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.textview.TextViewSFDisplayBold;
import android.textview.TextViewSFDisplayRegular;
import android.textview.TextViewSFTextRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.task24.R;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.fragment.BaseFragment;
import com.task24.model.ClientRate;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PriceRateFragment extends BaseFragment implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.tv_next)
    TextViewSFDisplayRegular tvNext;
    @BindView(R.id.rv_rates)
    RecyclerView rvRates;
    @BindView(R.id.tv_title)
    TextViewSFDisplayBold tvTitle;
    @BindView(R.id.tv_enter_price)
    TextViewSFTextRegular tvEnterPrice;
    @BindView(R.id.img_back)
    ImageView imgBack;

    private List<ClientRate.Data> priceList;
    private RecyclerviewAdapter mAdapter;
    private View view;
    private boolean isFixedPrice;
    private boolean isEdit;

    public static PriceRateFragment newInstance(boolean isEdit) {
        PriceRateFragment fragment = new PriceRateFragment();
        Bundle args = new Bundle();
        args.putBoolean(Constants.IS_EDIT, isEdit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            return view;
        }

        view = inflater.inflate(R.layout.fragment_select_rate, container, false);
        ButterKnife.bind(this, view);

        if (getArguments() != null) {
            isEdit = getArguments().getBoolean(Constants.IS_EDIT);
        }
        isFixedPrice = Preferences.readBoolean(activity, Constants.IS_FIXED_PRICE, false);

        if (!isEdit) {
            progress.setProgress(50);
        } else {
            progress.setVisibility(View.GONE);
        }

        tvNext.setVisibility(View.GONE);
        if (isFixedPrice) {
            tvTitle.setText(activity.getString(R.string.budget_in_mind));
            tvEnterPrice.setHint(activity.getString(R.string.enter_specific_budget));
        } else {
            tvTitle.setText(activity.getString(R.string.hourly_rate_in_mind));
            tvEnterPrice.setHint(activity.getString(R.string.enter_specific_hourly_rate));
        }

        priceList = new ArrayList<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        rvRates.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvRates.getContext(),
                linearLayoutManager.getOrientation());
        rvRates.addItemDecoration(dividerItemDecoration);

        getClientRates();

        String budget = Preferences.readString(activity, Constants.BUDGET, "");
        if (!TextUtils.isEmpty(budget)) {
            tvEnterPrice.setText(budget);
        }

        return view;
    }

    private void setAdapter() {
        if (priceList != null && priceList.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new RecyclerviewAdapter((ArrayList<?>) priceList, R.layout.item_select_rate, this);
            }
            mAdapter.doRefresh((ArrayList<?>) priceList);
            if (rvRates.getAdapter() == null) {
                rvRates.setAdapter(mAdapter);
            }
        }
    }

    @Override
    public void bindView(View view, final int position) {
        final ClientRate.Data item = priceList.get(position);
        TextView tvRates = view.findViewById(R.id.tv_rates);
        if (!TextUtils.isEmpty(item.rangeTo) && !item.rangeTo.equals("null")) {
            tvRates.setText("$" + item.rangeFrom + " - $" + item.rangeTo);
        } else {
            tvRates.setText("$" + item.rangeFrom);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.writeString(activity, Constants.BUDGET, "");
                Preferences.writeInteger(activity, Constants.CLIENT_RATE_ID, item.id);
                if (!TextUtils.isEmpty(item.rangeTo) && !item.rangeTo.equals("null")) {
                    Preferences.writeString(activity, Constants.CLIENT_RATE, "$" + item.rangeFrom + " - $" + item.rangeTo);
                } else {
                    Preferences.writeString(activity, Constants.CLIENT_RATE, "$" + item.rangeFrom);
                }

                if (isEdit) {
                    imgBack.performClick();
                } else {
                    activity.replaceFragment(new DescribeFragment());
                }
            }
        });
    }

    @OnClick({R.id.img_back, R.id.tv_enter_price})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(activity);
        switch (view.getId()) {
            case R.id.img_back:
                goBack();
                break;
            case R.id.tv_enter_price:
                activity.replaceFragment(EnterRateFragment.newInstance(isEdit));
                break;
        }
    }

    public void getClientRates() {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<ClientRate> call = activity.getService().getClientRates(isFixedPrice ? 1 : 2);
        call.enqueue(new Callback<ClientRate>() {
            @Override
            public void onResponse(Call<ClientRate> call, Response<ClientRate> response) {
                ClientRate model = response.body();
                if (model != null && activity.checkStatus(model)) {
                    priceList = new ArrayList<>();
                    priceList = model.data;
                    setAdapter();
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<ClientRate> call, Throwable t) {
                activity.failureError("get client rate failed");
            }
        });
    }
}
