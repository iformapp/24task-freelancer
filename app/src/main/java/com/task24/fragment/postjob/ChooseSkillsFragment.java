package com.task24.fragment.postjob;

import android.edittext.EditTextSFTextRegular;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.textview.TextViewSFDisplayRegular;
import android.textview.TextViewSFTextRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;
import com.task24.R;
import com.task24.fragment.BaseFragment;
import com.task24.model.SkillsById;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChooseSkillsFragment extends BaseFragment {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.et_search)
    EditTextSFTextRegular etSearch;
    @BindView(R.id.tag_group)
    TagView tagGroup;
    @BindView(R.id.tv_next)
    TextViewSFDisplayRegular tvNext;
    @BindView(R.id.tv_skip)
    TextViewSFTextRegular tvSkip;

    private List<SkillsById.Data> skillsList;
    private boolean isEdit;
    private String serviceId;
    private String skillIds = "";
    private String skillNames = "";

    public static ChooseSkillsFragment newInstance(boolean isEdit) {
        ChooseSkillsFragment fragment = new ChooseSkillsFragment();
        Bundle args = new Bundle();
        args.putBoolean(Constants.IS_EDIT, isEdit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_skills, container, false);
        ButterKnife.bind(this, view);

        if (getArguments() != null) {
            isEdit = getArguments().getBoolean(Constants.IS_EDIT);
        }
        serviceId = Preferences.readString(activity, Constants.SERVICE_ID, "");
        skillsList = new ArrayList<>();

        if (!isEdit) {
            progress.setProgress(20);
        } else {
            progress.setVisibility(View.GONE);
            tvNext.setText(getString(R.string.save));
            tvSkip.setVisibility(View.GONE);
        }

        tagGroup.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(Tag tag, int i) {
                skillsList.get(i).isSelected = !skillsList.get(i).isSelected;
                setTagsData();
            }
        });

        getSkillsByServiceId();

        return view;
    }

    private void setTagsData() {
        ArrayList<Tag> tags = new ArrayList<>();
        Tag tag;
        tagGroup.removeAll();
        if (skillsList != null && skillsList.size() > 0) {
            for (int i = 0; i < skillsList.size(); i++) {
                SkillsById.Data service = skillsList.get(i);
                tag = new Tag(service.skills.name);
                tag.radius = 10f;
                tag.layoutColor = ContextCompat.getColor(activity, service.isSelected ? R.color.colorPrimary : R.color.white);
                tag.tagTextColor = ContextCompat.getColor(activity, service.isSelected ? R.color.white : R.color.black);
                tag.layoutBorderColor = ContextCompat.getColor(activity, service.isSelected ? R.color.colorPrimary : R.color.textgrayAccent);
                tag.layoutBorderSize = 1f;
                tag.isDeletable = false;
                tags.add(tag);
            }
            tagGroup.addTags(tags);
        }
    }

    @OnClick({R.id.img_back, R.id.tv_next, R.id.tv_skip})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(activity);
        switch (view.getId()) {
            case R.id.img_back:
                goBack();
                break;
            case R.id.tv_next:
                getSkillIds();
                if (!TextUtils.isEmpty(skillIds)) {
                    Preferences.writeString(activity, Constants.SKILL_IDS, skillIds);
                    Preferences.writeString(activity, Constants.SKILL_NAMES, skillNames);
                    if (isEdit) {
                        goBack();
                    } else {
                        activity.replaceFragment(new PayTypeFragment());
                    }
                } else {
                    activity.validationError("Please select skill");
                }
                break;
            case R.id.tv_skip:
                Preferences.writeString(activity, Constants.SKILL_IDS, "");
                Preferences.writeString(activity, Constants.SKILL_NAMES, "");
                activity.replaceFragment(new PayTypeFragment());
                break;
        }
    }

    private void getSkillIds() {
        skillIds = "";
        skillNames = "";
        for (int i = 0; i < skillsList.size(); i++) {
            if (skillsList.get(i).isSelected) {
                if (TextUtils.isEmpty(skillIds)) {
                    skillIds = skillsList.get(i).skillId + "";
                    skillNames = skillsList.get(i).skills.name;
                } else {
                    skillIds = skillIds + "," + skillsList.get(i).skillId;
                    skillNames = skillNames + "," + skillsList.get(i).skills.name;
                }
            }
        }
    }

    public void getSkillsByServiceId() {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<SkillsById> call = activity.getService().getSkillsById(serviceId);
        call.enqueue(new Callback<SkillsById>() {
            @Override
            public void onResponse(Call<SkillsById> call, Response<SkillsById> response) {
                SkillsById model = response.body();
                if (model != null && activity.checkStatus(model)) {
                    skillsList = new ArrayList<>();
                    skillsList = model.data;
                    if (isEdit) {
                        skillIds = Preferences.readString(activity, Constants.SKILL_IDS, "");
                        for (int i = 0; i < skillsList.size(); i++) {
                            if (skillIds.contains(String.valueOf(skillsList.get(i).skillId))) {
                                skillsList.get(i).isSelected = true;
                            }
                        }
                    }
                    setTagsData();
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<SkillsById> call, Throwable t) {
                activity.failureError("get skills failed");
            }
        });
    }
}
