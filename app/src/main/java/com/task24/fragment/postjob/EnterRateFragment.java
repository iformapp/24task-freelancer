package com.task24.fragment.postjob;

import android.edittext.EditTextSFTextRegular;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.textview.TextViewSFDisplayBold;
import android.textview.TextViewSFDisplayRegular;
import android.textview.TextViewSFTextRegular;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.task24.R;
import com.task24.fragment.BaseFragment;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnterRateFragment extends BaseFragment {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.tv_next)
    TextViewSFDisplayRegular tvNext;
    @BindView(R.id.tv_title)
    TextViewSFDisplayBold tvTitle;
    @BindView(R.id.rate_seekbar)
    CrystalSeekbar rateSeekbar;
    @BindView(R.id.tv_min_value)
    TextViewSFTextRegular tvMinValue;
    @BindView(R.id.tv_max_value)
    TextViewSFTextRegular tvMaxValue;
    @BindView(R.id.et_rate)
    EditTextSFTextRegular etRate;
    @BindView(R.id.tv_hour)
    TextViewSFTextRegular tvHour;

    private View view;
    private boolean isFixedPrice;
    private boolean isEdit;

    public static EnterRateFragment newInstance(boolean isEdit) {
        EnterRateFragment fragment = new EnterRateFragment();
        Bundle args = new Bundle();
        args.putBoolean(Constants.IS_EDIT, isEdit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            return view;
        }

        view = inflater.inflate(R.layout.fragment_enter_rate, container, false);
        ButterKnife.bind(this, view);

        if (getArguments() != null) {
            isEdit = getArguments().getBoolean(Constants.IS_EDIT);
        }
        isFixedPrice = Preferences.readBoolean(activity, Constants.IS_FIXED_PRICE, false);

        if (isFixedPrice) {
            tvTitle.setText(activity.getString(R.string.specific_budget_in_mind));
            rateSeekbar.setMinValue(5);
            rateSeekbar.setMaxValue(5000);
            tvMinValue.setText("$5");
            tvMaxValue.setText("$5000");
            tvHour.setVisibility(View.GONE);
            etRate.setText("5");
        } else {
            tvTitle.setText(activity.getString(R.string.specific_hourly_rate_in_mind));
            rateSeekbar.setMinValue(2);
            rateSeekbar.setMaxValue(50);
            tvMinValue.setText("$2/hr");
            tvMaxValue.setText("$50/hr");
            etRate.setText("2");
        }

        rateSeekbar.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue) {
                if (isFixedPrice) {
                    tvMinValue.setText("$" + String.valueOf(minValue));
                } else {
                    tvMinValue.setText("$" + String.valueOf(minValue) + "/hr");
                }
                etRate.setText(String.valueOf(minValue));
            }
        });

        if (!isEdit) {
            progress.setProgress(50);
        } else {
            progress.setVisibility(View.GONE);
            tvNext.setText(getString(R.string.save));
            String budget = Preferences.readString(activity, Constants.BUDGET, "");
            if (!TextUtils.isEmpty(budget)) {
                etRate.setText(Utils.priceWithout$(budget));
                rateSeekbar.setMinStartValue(Float.parseFloat(Utils.priceWithout$(budget)));
                rateSeekbar.apply();
            }
        }

        etRate.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String rate = getRate();
                    if (!TextUtils.isEmpty(rate)) {
                        if (isFixedPrice && Integer.parseInt(rate) < 5) {
                            activity.validationError("You can enter between 5 to 5000");
                            return true;
                        } else if (!isFixedPrice && Integer.parseInt(rate) < 2) {
                            activity.validationError("You can enter between 2 to 50");
                            return true;
                        }
                        rateSeekbar.setMinStartValue(Float.parseFloat(rate));
                        rateSeekbar.apply();
                    }
                    Utils.hideSoftKeyboard(activity);
                    return true;
                }
                return false;
            }
        });

        etRate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String rate = getRate();
                if (!TextUtils.isEmpty(rate)) {
                    if (isFixedPrice) {
                        if (Integer.parseInt(rate) > 5000) {
                            etRate.setText("5000");
                            activity.validationError("You can enter between 5 to 5000");
                        }
                    } else {
                        if (Integer.parseInt(rate) > 50) {
                            etRate.setText("50");
                            activity.validationError("You can enter between 2 to 50");
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }

    private String getRate() {
        return etRate.getText().toString().trim();
    }

    @OnClick({R.id.img_back, R.id.tv_next})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(activity);
        switch (view.getId()) {
            case R.id.img_back:
                goBack();
                break;
            case R.id.tv_next:
                if (TextUtils.isEmpty(getRate())) {
                    activity.validationError("Please enter specific rate");
                    return;
                }
                Preferences.writeInteger(activity, Constants.CLIENT_RATE_ID, 0);
                Preferences.writeString(activity, Constants.CLIENT_RATE, "");
                Preferences.writeString(activity, Constants.BUDGET, "$" + getRate());
                if (isEdit) {
                    goTo(PostJobFragment.class.getName());
                } else {
                    activity.replaceFragment(new DescribeFragment());
                }
                break;
        }
    }
}
