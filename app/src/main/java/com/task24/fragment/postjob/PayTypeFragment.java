package com.task24.fragment.postjob;

import android.os.Bundle;
import android.textview.TextViewSFDisplayRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.task24.R;
import com.task24.fragment.BaseFragment;
import com.task24.util.Constants;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PayTypeFragment extends BaseFragment {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.tv_next)
    TextViewSFDisplayRegular tvNext;

    public static PayTypeFragment newInstance() {
        PayTypeFragment fragment = new PayTypeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pay_type, container, false);
        ButterKnife.bind(this, view);

        progress.setProgress(40);
        tvNext.setVisibility(View.GONE);

        return view;
    }

    @OnClick({R.id.img_back, R.id.rl_hourly, R.id.rl_fixed_price})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(activity);
        switch (view.getId()) {
            case R.id.img_back:
                goBack();
                break;
            case R.id.rl_hourly:
                Preferences.writeBoolean(activity, Constants.IS_FIXED_PRICE, false);
                activity.replaceFragment(PriceRateFragment.newInstance(false));
                break;
            case R.id.rl_fixed_price:
                Preferences.writeBoolean(activity, Constants.IS_FIXED_PRICE, true);
                activity.replaceFragment(PriceRateFragment.newInstance(false));
                break;
        }
    }
}
