package com.task24.fragment.postjob;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.adapter.RecyclerviewAdapter;
import com.task24.fragment.BaseFragment;
import com.task24.model.ServicesModel;
import com.task24.util.Constants;
import com.task24.util.EqualSpacingItemDecoration;
import com.task24.util.Preferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectServiceFragment extends BaseFragment implements RecyclerviewAdapter.OnViewBindListner {

    @BindView(R.id.rv_services)
    RecyclerView rvServices;

    private List<ServicesModel.Data> servicesList;
    private RecyclerviewAdapter mAdapter;
    private View view;

    public static SelectServiceFragment newInstance() {
        SelectServiceFragment fragment = new SelectServiceFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            return view;
        }

        view = inflater.inflate(R.layout.fragment_select_service, container, false);
        ButterKnife.bind(this, view);

        Preferences.writeString(activity, Constants.SERVICE_ID, "");
        Preferences.writeString(activity, Constants.SKILL_IDS, "");
        Preferences.writeString(activity, Constants.SKILL_NAMES, "");
        Preferences.writeString(activity, Constants.CLIENT_RATE, "");
        Preferences.writeInteger(activity, Constants.CLIENT_RATE_ID, 0);
        Preferences.writeString(activity, Constants.BUDGET, "");
        Preferences.writeString(activity, Constants.DESCRIBE, "");
        Preferences.writeString(activity, Constants.ATTACH_FILE, "");

        servicesList = Preferences.getTopServices(activity);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        rvServices.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvServices.getContext(),
                linearLayoutManager.getOrientation());
        rvServices.addItemDecoration(dividerItemDecoration);

        setAdapter();
        return view;
    }

    private void setAdapter() {
        if (servicesList != null && servicesList.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new RecyclerviewAdapter((ArrayList<?>) servicesList, R.layout.item_select_service, this);
            }
            mAdapter.doRefresh((ArrayList<?>) servicesList);
            if (rvServices.getAdapter() == null) {
                rvServices.setAdapter(mAdapter);
            }
        }
    }

    @Override
    public void bindView(View view, int position) {
        final ServicesModel.Data service = servicesList.get(position);
        TextView tvService = view.findViewById(R.id.tv_service);
        TextView tvSubService = view.findViewById(R.id.tv_subservice);
        tvService.setText(service.name);
        if (service.services.size() >= 2) {
            tvSubService.setText(service.services.get(0).name + ", " + service.services.get(1).name);
        } else {
            tvSubService.setText("Dummy Services");
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String servicesId = null;
                List<ServicesModel.Services> serviceList = service.services;
                for (int i = 0; i < serviceList.size(); i++) {
                    if (TextUtils.isEmpty(servicesId)) {
                        servicesId = serviceList.get(i).id + "";
                    } else {
                        servicesId = servicesId + "," + serviceList.get(i).id;
                    }
                }
                Preferences.writeString(activity, Constants.SERVICE_ID, servicesId);
                activity.replaceFragment(new ChooseSkillsFragment());
            }
        });
    }
}
