package com.task24.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.ui.home.HomePagerModel;
import com.task24.util.Constants;
import com.task24.util.DividerView;
import com.task24.util.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeItemsAdapter extends RecyclerView.Adapter<HomeItemsAdapter.SimpleViewHolder> {

    private List<HomePagerModel> mDataset;
    private Context context;
    private String types;
    private OnItemClick onItemClick;

    public HomeItemsAdapter(Context context, List<HomePagerModel> objects, String types, OnItemClick onItemClick) {
        this.mDataset = objects;
        this.context = context;
        this.types = types;
        this.onItemClick = onItemClick;
    }

    public interface OnItemClick {
        public void onClickItem(HomePagerModel model);
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (types.equalsIgnoreCase(Constants.HOW_IT_WORKS)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_list_item, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_grid_item, parent, false);
        }
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        HomePagerModel item = mDataset.get(position);
        Utils.loadImage(context, item.icon, holder.imgIcon);
        if (holder.tvTitle != null) {
            holder.tvTitle.setText(item.title);
        }
        if (types.equalsIgnoreCase(Constants.HOW_IT_WORKS)) {
            switch (position) {
                case 0:
                    holder.tvCount.setTextColor(ContextCompat.getColor(context, R.color.pink));
                    holder.tvCount.setBackground(ContextCompat.getDrawable(context, R.drawable.dotted_round_pink));
                    holder.viewBottom.setColor(ContextCompat.getColor(context, R.color.pink));
                    holder.viewTop.setVisibility(View.GONE);
                    holder.viewBottom.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    holder.tvCount.setTextColor(ContextCompat.getColor(context, R.color.sky));
                    holder.tvCount.setBackground(ContextCompat.getDrawable(context, R.drawable.dotted_round_sky));
                    holder.viewBottom.setColor(ContextCompat.getColor(context, R.color.sky));
                    holder.viewTop.setColor(ContextCompat.getColor(context, R.color.sky));
                    holder.viewTop.setVisibility(View.VISIBLE);
                    holder.viewBottom.setVisibility(View.VISIBLE);
                    break;
                case 2:
                    holder.tvCount.setTextColor(ContextCompat.getColor(context, R.color.light_blue));
                    holder.tvCount.setBackground(ContextCompat.getDrawable(context, R.drawable.dotted_round_blue));
                    holder.viewTop.setColor(ContextCompat.getColor(context, R.color.light_blue));
                    holder.viewTop.setVisibility(View.VISIBLE);
                    holder.viewBottom.setVisibility(View.GONE);
                    break;
            }
            if (holder.tvCount != null) {
                holder.tvCount.setText((position + 1) + "");
            }
        }
    }

    @Override
    public int getItemCount() {
        return mDataset != null ? mDataset.size() : 0;
    }

    public List<HomePagerModel> getData() {
        return mDataset;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.img_icon)
        ImageView imgIcon;
        @Nullable
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @Nullable
        @BindView(R.id.view_top)
        DividerView viewTop;
        @Nullable
        @BindView(R.id.tv_count)
        TextView tvCount;
        @Nullable
        @BindView(R.id.view_bottom)
        DividerView viewBottom;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClick != null) {
                        onItemClick.onClickItem(mDataset.get(getAdapterPosition()));
                    } else {
                        Toast.makeText(context, mDataset.get(getAdapterPosition()).title, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
