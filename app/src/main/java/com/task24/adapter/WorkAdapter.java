package com.task24.adapter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.edittext.EditTextSFTextRegular;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.textview.TextViewSFTextMedium;
import android.textview.TextViewSFTextRegular;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.model.ServicesModel;
import com.task24.model.Work;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WorkAdapter extends RecyclerView.Adapter<WorkAdapter.SimpleViewHolder> {

    private List<Work> mDataset;
    private Context context;
    private ServicesAdapter serviceAdapter;
    private SingleSelectionItemAdapter itemAdapter;

    public WorkAdapter(Context context) {
        this.context = context;
    }

    public void doRefresh(List<Work> objects) {
        this.mDataset = objects;
        notifyDataSetChanged();
    }

    public void addWork(Work work) {
        mDataset.add(work);
        notifyDataSetChanged();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.work_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        final Work item = mDataset.get(position);
        holder.tvHeading.setText("Employment" + (position + 1));
        holder.etCompany.setText(item.company);
        holder.tvJobTitle.setText(item.jobTitle);
        holder.tvExperience.setText(item.experience);
        holder.tvStartYear.setText(item.startYear);
        holder.tvEndYear.setText(item.isCurrentlyWorking ? "Present" : item.endYear);

        holder.tvRemove.setPaintFlags(holder.tvRemove.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if (mDataset.size() == 1) {
            holder.tvRemove.setText(context.getString(R.string.clear));
        } else {
            holder.tvRemove.setText(context.getString(R.string.remove));
        }

        holder.tvStartYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectFromDate(holder.tvStartYear, item);
            }
        });

        holder.tvEndYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!item.isCurrentlyWorking)
                    selectToDate(holder.tvStartYear.getText().toString(), holder.tvEndYear, item);
            }
        });

        holder.tvJobTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showItemSelectDialog(holder.tvJobTitle, item);
            }
        });

        holder.tvExperience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSingleSelectionDialog(holder.tvExperience, item);
            }
        });

        holder.chkWorking.setOnCheckedChangeListener(null);
        holder.chkWorking.setChecked(item.isCurrentlyWorking);
        holder.chkWorking.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                for (int i = 0; i < mDataset.size(); i++) {
                    mDataset.get(i).isCurrentlyWorking = false;
                }
                item.isCurrentlyWorking = isChecked;
                notifyDataSetChanged();
            }
        });

        holder.tvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDataset.size() == 1) {
                    item.company = "";
                    item.jobTitle = "";
                    item.experience = "";
                    item.startYear = "";
                    item.endYear = "";
                    item.isCurrentlyWorking = false;
                    notifyDataSetChanged();
                } else {
                    mDataset.remove(position);
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset != null ? mDataset.size() : 0;
    }

    public List<Work> getData() {
        return mDataset;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_heading)
        TextViewSFTextMedium tvHeading;
        @BindView(R.id.tv_remove)
        TextViewSFTextMedium tvRemove;
        @BindView(R.id.et_company)
        EditTextSFTextRegular etCompany;
        @BindView(R.id.tv_job_title)
        TextView tvJobTitle;
        @BindView(R.id.tv_experience)
        TextViewSFTextRegular tvExperience;
        @BindView(R.id.tv_start_year)
        TextViewSFTextRegular tvStartYear;
        @BindView(R.id.tv_end_year)
        TextViewSFTextRegular tvEndYear;
        @BindView(R.id.chk_working)
        CheckBox chkWorking;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            etCompany.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mDataset.get(getAdapterPosition()).company = s.toString();
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }
    }

    private void selectFromDate(final TextView tvYear, final Work item) {
        int year, month;
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.DatePickerDialog, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int i2) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);

                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yyyy");
                String date = dateFormat.format(calendar.getTime());
                tvYear.setText(date);
                item.startYear = date;
            }
        }, year, month, 0);

        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.getDatePicker().findViewById(context.getResources().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
    }

    private void selectToDate(String fromDate, final TextView tvYear, final Work item) {
        if (TextUtils.isEmpty(fromDate)) {
            Toast.makeText(context, "Select Start Year First", Toast.LENGTH_SHORT).show();
            return;
        }

        int year = 0, month = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yyyy");
        Date fDate;
        try {
            fDate = dateFormat.parse(fromDate);
            month = Integer.parseInt((String) DateFormat.format("MM", fDate));
            year = Integer.parseInt((String) DateFormat.format("yyyy", fDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar c = Calendar.getInstance();
        c.set(year, month, 0);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.DatePickerDialog, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int i2) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);

                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yyyy");
                String date = dateFormat.format(calendar.getTime());
                tvYear.setText(date);
                item.endYear = date;
            }
        }, year, month, 0);

        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.getDatePicker().findViewById(context.getResources().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
    }

    private void showItemSelectDialog(final TextView tvJobTitle, final Work item) {
        final Dialog dialog = new Dialog(context, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select_black);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        final EditText etSearch = dialog.findViewById(R.id.et_search);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        etSearch.setHint(String.format(context.getString(R.string.search_for), context.getString(R.string.services).toLowerCase()));

        rvTypes.setLayoutManager(new LinearLayoutManager(context));
        List<ServicesModel.Data> mData = Preferences.getTopServices(context);
        if (mData != null && mData.size() > 0) {
            for (int i = 0; i < mData.size(); i++) {
                if (mData.get(i).name.equalsIgnoreCase(tvJobTitle.getText().toString())) {
                    mData.get(i).isSelected = true;
                }
            }
            serviceAdapter = new ServicesAdapter(context, mData);
            rvTypes.setAdapter(serviceAdapter);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard((Activity) context);
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard((Activity) context);
                if (serviceAdapter != null && serviceAdapter.getSelectedItem() != null) {
                    tvJobTitle.setText(serviceAdapter.getSelectedItem().name);
                    item.serviceId = serviceAdapter.getSelectedItem().id;
                    item.jobTitle = serviceAdapter.getSelectedItem().name;
                    dialog.dismiss();
                } else {
                    Toast.makeText(context, "Please select service", Toast.LENGTH_SHORT).show();
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (serviceAdapter != null)
                    serviceAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etSearch.post(new Runnable() {
                    @Override
                    public void run() {
                        Utils.openSoftKeyboard((Activity) context, etSearch);
                    }
                });
            }
        });
        etSearch.requestFocus();
    }

    private void showSingleSelectionDialog(final TextView tvLevel, final Work item) {
        final Dialog dialog = new Dialog(context, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select_black);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        EditText etSearch = dialog.findViewById(R.id.et_search);
        etSearch.setVisibility(View.GONE);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        rvTypes.setLayoutManager(new LinearLayoutManager(context));
        String[] experience = context.getResources().getStringArray(R.array.experience);
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(experience));
        int selectedPosition = -1;
        if (arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).equalsIgnoreCase(tvLevel.getText().toString())) {
                    selectedPosition = i;
                }
            }
            itemAdapter = new SingleSelectionItemAdapter(context, arrayList, selectedPosition);
            rvTypes.setAdapter(itemAdapter);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard((Activity) context);
                if (itemAdapter != null && itemAdapter.getSelectedItem() != null) {
                    tvLevel.setText(itemAdapter.getSelectedItem());
                    item.experience = itemAdapter.getSelectedItem();
                    dialog.dismiss();
                } else {
                    Toast.makeText(context, "Please select one item", Toast.LENGTH_SHORT).show();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }
}
