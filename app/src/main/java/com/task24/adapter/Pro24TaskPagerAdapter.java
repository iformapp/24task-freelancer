package com.task24.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.textview.TextViewSFTextSemiBold;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.task24.R;
import com.task24.util.Preferences;
import com.task24.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Pro24TaskPagerAdapter extends PagerAdapter {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_desciption)
    TextView tvDesciption;
    @BindView(R.id.tv_profile_complete)
    TextViewSFTextSemiBold tvProfileComplete;

    private Context mContext;

    public Pro24TaskPagerAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup v = (ViewGroup) inflater.inflate(R.layout.item_24task_pro, collection, false);
        ButterKnife.bind(this, v);

        if (position == 0) {
            tvProfileComplete.setVisibility(View.GONE);
            String s = mContext.getString(R.string.what_s_24task_pro);
            int[] colorList = {R.color.red};
            String[] words = {"PRO"};
            tvTitle.setText(Utils.getBoldString(mContext, s, null, colorList, words));
            tvDesciption.setText(mContext.getString(R.string.what_pro_text));
        } else if (position == 1) {
            tvProfileComplete.setVisibility(View.GONE);
            String s = mContext.getString(R.string.how_it_works);
            int[] colorList = {R.color.colorPrimary};
            String[] words = {"works"};
            tvTitle.setText(Utils.getBoldString(mContext, s, null, colorList, words));
            tvDesciption.setText(mContext.getString(R.string.how_it_works_text));
        } else if (position == 2) {
            tvProfileComplete.setVisibility(View.GONE);
            String s = mContext.getString(R.string.guarantee_full_time);
            int[] colorList = {R.color.lightgreen};
            String[] words = {"Full-time"};
            tvTitle.setText(Utils.getBoldString(mContext, s, null, colorList, words));
            tvDesciption.setText(mContext.getString(R.string.guarantee_job_text));
        } else if (position == 3) {
            tvProfileComplete.setVisibility(View.GONE);
            String s = mContext.getString(R.string.guarantee_payment);
            int[] colorList = {R.color.skin};
            String[] words = {"payment"};
            tvTitle.setText(Utils.getBoldString(mContext, s, null, colorList, words));
            tvDesciption.setText(mContext.getString(R.string.guarantee_payment_text));
        } else if (position == 4) {
            tvProfileComplete.setVisibility(View.GONE);
            String s = mContext.getString(R.string.how_much_do_we_pay);
            int[] colorList = {R.color.colorPrimary};
            String[] words = {"pay"};
            tvTitle.setText(Utils.getBoldString(mContext, s, null, colorList, words));
            tvDesciption.setText(mContext.getString(R.string.how_much_pay_text));
        } else if (position == 5) {
            if (Preferences.getProfileData(mContext).percentage != null) {
                String profilePercentage = Preferences.getProfileData(mContext).percentage.totalPercentage + "%";
                tvProfileComplete.setText(Utils.getColorString(mContext,
                        mContext.getString(R.string.your_profile_is, profilePercentage), profilePercentage, R.color.red));
                tvProfileComplete.setVisibility(View.VISIBLE);
            }
            tvTitle.setText(mContext.getString(R.string.how_to_apply));
            String s = mContext.getString(R.string.how_to_apply_text);
            int[] colorList = {R.color.lightgreen};
            String[] words = {"100%"};
            tvDesciption.setText(Utils.getBoldString(mContext, s, null, colorList, words));
        }
        collection.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }
}
