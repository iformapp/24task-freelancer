package com.task24.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewSFDisplayRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.task24.R;
import com.task24.model.Projects;
import com.task24.model.Recommended;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecommendedFreelancerAdapter extends RecyclerView.Adapter<RecommendedFreelancerAdapter.SimpleViewHolder> {

    private Context context;
    private ArrayList<Recommended> mDataList;

    public RecommendedFreelancerAdapter(Context context) {
        this.context = context;
    }

    public void doRefresh(ArrayList<Recommended> mDataList) {
        this.mDataList = mDataList;
        notifyDataSetChanged();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recommended_list, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        Recommended item = mDataList.get(position);
        holder.tvName.setText(item.name);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_user)
        ImageView imgUser;
        @BindView(R.id.img_flag)
        ImageView imgFlag;
        @BindView(R.id.tv_name)
        TextViewSFDisplayRegular tvName;
        @BindView(R.id.ratingbar)
        ScaleRatingBar ratingbar;
        @BindView(R.id.tv_reviews)
        TextViewSFDisplayRegular tvReviews;
        @BindView(R.id.tv_rate)
        TextViewSFDisplayRegular tvRate;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, getAdapterPosition() + "", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
