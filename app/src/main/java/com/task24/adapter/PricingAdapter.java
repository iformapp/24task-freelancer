package com.task24.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewHelveticaLight;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.task24.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PricingAdapter extends RecyclerView.Adapter<PricingAdapter.SimpleViewHolder> {

    private Context context;
    private ArrayList<String> pricingList;

    public PricingAdapter(Context context) {
        this.context = context;
    }

    public void doRefresh(ArrayList<String> pricingList) {
        this.pricingList = pricingList;
        notifyDataSetChanged();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.price_list_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        if (position == 0) {
            holder.tvTitle.setText("VIP");
            holder.tvDescription.setText("150 hours or more/Month");
        } else if (position == 1) {
            holder.tvTitle.setText("Plus");
            holder.tvDescription.setText("90 hours or more/Month");
        } else if (position == 2) {
            holder.tvTitle.setText("By The Hour");
            holder.tvDescription.setText("By The Hour");
        }
        holder.tvPrice.setText("$" + pricingList.get(position));
    }

    @Override
    public int getItemCount() {
        return pricingList.size();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextViewHelveticaLight tvTitle;
        @BindView(R.id.tv_price)
        TextViewHelveticaLight tvPrice;
        @BindView(R.id.tv_description)
        TextViewHelveticaLight tvDescription;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, getAdapterPosition() + "", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
