package com.task24.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.task24.R;
import com.task24.model.ServicesModel;
import com.task24.util.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExpertiseItemAdapter extends RecyclerView.Adapter<ExpertiseItemAdapter.SimpleViewHolder>
        implements Filterable {

    private List<ServicesModel.Data> mDataset;
    private List<ServicesModel.Data> mDatasetFiltered;
    private Context context;
    private boolean isSearchMode = false;
    private boolean isExpertise;

    public ExpertiseItemAdapter(Context context, ArrayList<ServicesModel.Data> objects, boolean isExpertise) {
        this.mDataset = objects;
        this.mDatasetFiltered = objects;
        this.context = context;
        this.isExpertise = isExpertise;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_full, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        ServicesModel.Data item = mDatasetFiltered.get(position);

        if (!isExpertise) {
            holder.tvTitle.setText(item.experience);
            holder.tvTitle.setGravity(Gravity.CENTER);
        } else {
            holder.tvTitle.setText(item.name);
        }
        holder.tvHeading.setVisibility(View.GONE);

        if (item.isSelected) {
            holder.tvTitle.setBackground(ContextCompat.getDrawable(context, R.drawable.blue_button_bg));
            Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFTEXT_BOLD);
            holder.tvTitle.setTypeface(tf);
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.white));
        } else {
            holder.tvTitle.setBackgroundColor(Color.TRANSPARENT);
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black));
            Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFTEXT_REGULAR);
            holder.tvTitle.setTypeface(tf);
        }

        holder.rlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelected();
                mDatasetFiltered.get(position).isSelected = true;
                notifyDataSetChanged();
            }
        });
    }

    private void clearSelected() {
        if (mDatasetFiltered != null && mDatasetFiltered.size() > 0) {
            for (int i = 0; i < mDatasetFiltered.size(); i++) {
                mDatasetFiltered.get(i).isSelected = false;
            }
        }
    }

    public ServicesModel.Data getSelectedItem() {
        if (mDatasetFiltered != null && mDatasetFiltered.size() > 0) {
            for (int i = 0; i < mDatasetFiltered.size(); i++) {
                if (mDatasetFiltered.get(i).isSelected) {
                    return mDatasetFiltered.get(i);
                }
            }
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mDatasetFiltered != null ? mDatasetFiltered.size() : 0;
    }

    public List<ServicesModel.Data> getData() {
        return mDatasetFiltered;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_heading)
        TextView tvHeading;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.rl_view)
        View rlView;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    isSearchMode = false;
                    mDatasetFiltered = mDataset;
                    //clearSelected();
                    //selectFirst();
                } else {
                    isSearchMode = true;
                    List<ServicesModel.Data> filteredList = new ArrayList<>();
                    for (ServicesModel.Data row : mDataset) {
                        String rowText = row.name.toLowerCase();
                        if (!TextUtils.isEmpty(rowText)) {
                            if (rowText.contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }
                    }

                    mDatasetFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDatasetFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDatasetFiltered = (List<ServicesModel.Data>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
