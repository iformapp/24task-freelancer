package com.task24.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewSFDisplayRegular;
import android.textview.TextViewSFTextMedium;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.task24.R;
import com.task24.model.Projects;
import com.task24.ui.projects.ProjectCycleActivity;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProposalsAdapter extends RecyclerView.Adapter<ProposalsAdapter.SimpleViewHolder> {

    private Context context;
    private ArrayList<String> projectsList;

    public ProposalsAdapter(Context context) {
        this.context = context;
    }

    public void doRefresh(ArrayList<String> projectsList) {
        this.projectsList = projectsList;
        notifyDataSetChanged();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_proposal_list, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        //Projects item = projectsList.get(position);
        //holder.tvStatus.setText("");

        holder.tvAward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ProjectCycleActivity) context).showAwardedDialog();
            }
        });
    }

    @Override
    public int getItemCount() {
        return projectsList.size();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_user)
        ImageView imgUser;
        @BindView(R.id.tv_name)
        TextViewSFDisplayRegular tvName;
        @BindView(R.id.ratingbar)
        ScaleRatingBar ratingbar;
        @BindView(R.id.tv_reviews)
        TextViewSFDisplayRegular tvReviews;
        @BindView(R.id.tv_bid_price)
        TextViewSFDisplayRegular tvBidPrice;
        @BindView(R.id.tv_proposal)
        TextViewSFDisplayRegular tvProposal;
        @BindView(R.id.tv_read_more)
        TextViewSFTextMedium tvReadMore;
        @BindView(R.id.tv_award)
        TextViewSFTextMedium tvAward;
        @BindView(R.id.tv_chat)
        TextViewSFTextMedium tvChat;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
