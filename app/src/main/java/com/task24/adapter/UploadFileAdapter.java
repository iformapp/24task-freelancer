package com.task24.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.task24.R;
import com.task24.model.Attachment;
import com.task24.ui.BaseActivity;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UploadFileAdapter extends RecyclerView.Adapter<UploadFileAdapter.SimpleViewHolder> {

    private ArrayList<Attachment> mDataset;
    private Context context;
    private BaseActivity activity;
    private OnFileDeleteListener onFileDeletelistener;

    public UploadFileAdapter(Context context) {
        this.context = context;
        activity = (BaseActivity) context;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_uploaded_files, parent, false);
        return new SimpleViewHolder(view);
    }

    public void doRefresh(ArrayList<Attachment> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

    public interface OnFileDeleteListener {
        public void onFileDelete(ArrayList<Attachment> mDataset);
    }

    public void setOnFileDeleteListener(OnFileDeleteListener onFileDeletelistener) {
        this.onFileDeletelistener = onFileDeletelistener;
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        Attachment attachment = mDataset.get(position);
        File file = new File(attachment.filepath);
        viewHolder.tvFileName.setText(file.getName());
        if (attachment.isImage)
            Glide.with(context).load(file).into(viewHolder.imgFile);
        else
            viewHolder.imgFile.setImageResource(R.drawable.file);

        viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDataset.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, mDataset.size());
                if (onFileDeletelistener != null) {
                    onFileDeletelistener.onFileDelete(mDataset);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_file_name)
        TextView tvFileName;
        @BindView(R.id.img_delete)
        ImageView imgDelete;
        @BindView(R.id.img_file)
        ImageView imgFile;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File file = new File(mDataset.get(getAdapterPosition()).filepath);
                    activity.viewFile(file);
                }
            });
        }
    }
}
