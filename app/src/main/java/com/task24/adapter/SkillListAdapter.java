package com.task24.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.textview.TextViewSFDisplayRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.task24.R;
import com.task24.model.Skill;
import com.task24.ui.workprofile.SelectSkillsActivity;
import com.task24.util.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SkillListAdapter extends RecyclerView.Adapter<SkillListAdapter.SimpleViewHolder>
        implements Filterable {

    private ArrayList<Skill> mDataset;
    private ArrayList<Skill> mDatasetFiltered;
    private Context context;

    public SkillListAdapter(Context context, ArrayList<Skill> objects) {
        this.mDataset = objects;
        this.mDatasetFiltered = objects;
        this.context = context;
    }

    public void doRefresh(ArrayList<Skill> objects) {
        this.mDatasetFiltered = objects;
        notifyDataSetChanged();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_skill_list, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        final Skill item = mDatasetFiltered.get(position);
        holder.tvSkillName.setText(item.skillTitle);

        if (item.isSelected) {
            holder.rlMain.setBackgroundColor(Color.WHITE);
            Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFDISPLAY_BOLD);
            holder.tvSkillName.setTypeface(tf);
            holder.imgAddRemove.setImageResource(R.drawable.close_red);
        } else {
            holder.rlMain.setBackgroundColor(Color.TRANSPARENT);
            Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFDISPLAY_REGULAR);
            holder.tvSkillName.setTypeface(tf);
            holder.imgAddRemove.setImageResource(R.drawable.add);
        }

        holder.imgAddRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item.isSelected = !item.isSelected;
                notifyDataSetChanged();
                ((SelectSkillsActivity) context).refreshSelectedSkills(mDatasetFiltered, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDatasetFiltered != null ? mDatasetFiltered.size() : 0;
    }

    public ArrayList<Skill> getData() {
        return mDatasetFiltered;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_skill_name)
        TextViewSFDisplayRegular tvSkillName;
        @BindView(R.id.img_add_remove)
        ImageView imgAddRemove;
        @BindView(R.id.rl_main)
        RelativeLayout rlMain;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mDatasetFiltered = mDataset;
                } else {
                    ArrayList<Skill> filteredList = new ArrayList<>();
                    for (Skill row : mDataset) {
                        String rowText = row.skillTitle.toLowerCase();
                        if (!TextUtils.isEmpty(rowText)) {
                            if (rowText.contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }
                    }

                    mDatasetFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDatasetFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDatasetFiltered = (ArrayList<Skill>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
