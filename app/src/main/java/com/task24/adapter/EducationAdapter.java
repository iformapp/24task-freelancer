package com.task24.adapter;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.edittext.EditTextSFTextRegular;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.textview.TextViewSFTextMedium;
import android.textview.TextViewSFTextRegular;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.model.Education;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EducationAdapter extends RecyclerView.Adapter<EducationAdapter.SimpleViewHolder> {

    private List<Education> mDataset;
    private Context context;
    private SingleSelectionItemAdapter itemAdapter;

    public EducationAdapter(Context context) {
        this.context = context;
    }

    public void doRefresh(List<Education> objects) {
        this.mDataset = objects;
        notifyDataSetChanged();
    }

    public void addEducation(Education education) {
        mDataset.add(education);
        notifyDataSetChanged();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.education_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        final Education item = mDataset.get(position);
        holder.tvHeading.setText("Education" + (position + 1));
        holder.etDegree.setText(getValue(item.degree));
        holder.etCollege.setText(getValue(item.college));
        holder.tvLevel.setText(getValue(item.level));
        holder.tvStartYear.setText(getValue(item.startYear));
        holder.tvEndYear.setText(getValue(item.endYear));
        holder.tvRemove.setPaintFlags(holder.tvRemove.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if (mDataset.size() == 1) {
            holder.tvRemove.setText(context.getString(R.string.clear));
        } else {
            holder.tvRemove.setText(context.getString(R.string.remove));
        }

        holder.tvLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showItemSelectDialog(holder.tvLevel, item);
            }
        });

        holder.tvStartYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectFromDate(holder.tvStartYear, item);
            }
        });

        holder.tvEndYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectToDate(holder.tvStartYear.getText().toString(), holder.tvEndYear, item);
            }
        });

        holder.tvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDataset.size() == 1) {
                    item.college = "";
                    item.degree = "";
                    item.startYear = "";
                    item.endYear = "";
                    notifyDataSetChanged();
                } else {
                    mDataset.remove(position);
                    notifyDataSetChanged();
                }
            }
        });
    }

    private String getValue(String value) {
        return TextUtils.isEmpty(value) ? "" : value;
    }

    @Override
    public int getItemCount() {
        return mDataset != null ? mDataset.size() : 0;
    }

    public List<Education> getData() {
        return mDataset;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_heading)
        TextViewSFTextMedium tvHeading;
        @BindView(R.id.tv_remove)
        TextViewSFTextMedium tvRemove;
        @BindView(R.id.et_degree)
        EditTextSFTextRegular etDegree;
        @BindView(R.id.et_college)
        EditTextSFTextRegular etCollege;
        @BindView(R.id.tv_level)
        TextViewSFTextRegular tvLevel;
        @BindView(R.id.tv_start_year)
        TextViewSFTextRegular tvStartYear;
        @BindView(R.id.tv_end_year)
        TextViewSFTextRegular tvEndYear;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            etCollege.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mDataset.get(getAdapterPosition()).college = s.toString();
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            etDegree.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mDataset.get(getAdapterPosition()).degree = s.toString();
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }
    }

    private void selectFromDate(final TextView tvYear, final Education item) {
        int year, month;
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.DatePickerDialog, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int i2) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);

                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yyyy");
                String date = dateFormat.format(calendar.getTime());
                tvYear.setText(date);
                item.startYear = date;
            }
        }, year, month, 0);

        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.getDatePicker().findViewById(context.getResources().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
    }

    private void selectToDate(String fromDate, final TextView tvYear, final Education item) {
        if (TextUtils.isEmpty(fromDate)) {
            Toast.makeText(context, "Select Start Year First", Toast.LENGTH_SHORT).show();
            return;
        }

        int year = 0, month = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yyyy");
        Date fDate;
        try {
            fDate = dateFormat.parse(fromDate);
            month = Integer.parseInt((String) DateFormat.format("MM", fDate));
            year = Integer.parseInt((String) DateFormat.format("yyyy", fDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar c = Calendar.getInstance();
        c.set(year, month, 0);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.DatePickerDialog, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int i2) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);

                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yyyy");
                String date = dateFormat.format(calendar.getTime());
                tvYear.setText(date);
                item.endYear = date;
            }
        }, year, month, 0);

        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.getDatePicker().findViewById(context.getResources().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
    }

    private void showItemSelectDialog(final TextView tvLevel, final Education item) {
        final Dialog dialog = new Dialog(context, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_item_select_black);
        dialog.setCancelable(true);

        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);
        EditText etSearch = dialog.findViewById(R.id.et_search);
        etSearch.setVisibility(View.GONE);
        RecyclerView rvTypes = dialog.findViewById(R.id.rv_items);

        rvTypes.setLayoutManager(new LinearLayoutManager(context));
        String[] educationLevel = context.getResources().getStringArray(R.array.education_level);
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(educationLevel));
        int selectedPosition = -1;
        if (arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).equalsIgnoreCase(tvLevel.getText().toString())) {
                    selectedPosition = i;
                }
            }
            itemAdapter = new SingleSelectionItemAdapter(context, arrayList, selectedPosition);
            rvTypes.setAdapter(itemAdapter);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemAdapter != null && itemAdapter.getSelectedItem() != null) {
                    tvLevel.setText(itemAdapter.getSelectedItem());
                    item.level = itemAdapter.getSelectedItem();
                    dialog.dismiss();
                } else {
                    Toast.makeText(context, "Please select one item", Toast.LENGTH_SHORT).show();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }
}
