package com.task24.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.textview.TextViewSFTextRegular;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.task24.R;
import com.task24.model.Available;
import com.task24.model.GeneralModel;
import com.task24.segment.SegmentedButton;
import com.task24.segment.SegmentedButtonGroup;
import com.task24.ui.BaseActivity;
import com.task24.util.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AvailabilityAdapter extends RecyclerView.Adapter<AvailabilityAdapter.SimpleViewHolder> {

    private ArrayList<Available.Data> mDataset;
    private Context context;
    private BaseActivity activity;
    private boolean isWorkBase;
    private SparseBooleanArray workBaseArray;

    public AvailabilityAdapter(Context context, ArrayList<Available.Data> objects, boolean isWorkBase) {
        this.mDataset = objects;
        this.context = context;
        this.isWorkBase = isWorkBase;
        activity = (BaseActivity) context;
        workBaseArray = new SparseBooleanArray();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_availability, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        final Available.Data item = mDataset.get(position);
        holder.tvSchedule.setText(item.name);
        if (!TextUtils.isEmpty(item.detail)) {
            holder.tvAvailable.setText("(" + item.detail + ")");
        }
        if (item.isChecked)
            workBaseArray.put(position, true);
        holder.segmentGroup.setPosition(item.isChecked ? 1 : 0);

        holder.segmentGroup.setOnPositionChangedListener(new SegmentedButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(int status) {
                if (status == 0) {
                    if (isWorkBase)
                        workBaseArray.delete(position);
                    holder.tabYes.setTypeface(Constants.SFTEXT_REGULAR);
                    holder.tabNo.setTypeface(Constants.SFTEXT_BOLD);
                } else {
                    if (isWorkBase)
                        workBaseArray.put(position, true);
                    holder.tabNo.setTypeface(Constants.SFTEXT_REGULAR);
                    holder.tabYes.setTypeface(Constants.SFTEXT_BOLD);
                }
                if (isWorkBase) {
                    if (workBaseArray.size() == 2) {
                        updateWorkbase("2");  // For both selected
                    } else {
                        if (workBaseArray.size() == 0) {
                            updateWorkbase("3");  // If nothing selected
                        } else {
                            updateWorkbase(String.valueOf(workBaseArray.keyAt(0)));  // For one work type selected
                        }
                    }
                } else {
                    updatePayTypes(item.id, status);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset != null ? mDataset.size() : 0;
    }

    public ArrayList<Available.Data> getData() {
        return mDataset;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_schedule)
        TextViewSFTextRegular tvSchedule;
        @BindView(R.id.tv_available)
        TextViewSFTextRegular tvAvailable;
        @BindView(R.id.tab_no)
        SegmentedButton tabNo;
        @BindView(R.id.tab_yes)
        SegmentedButton tabYes;
        @BindView(R.id.segmentGroup)
        SegmentedButtonGroup segmentGroup;
        @BindView(R.id.view)
        View view;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void updatePayTypes(int payTypeId, int status) {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().updatePayTypes(activity.getUserID(), payTypeId, status, activity.getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (activity.checkStatus(response.body())) {

                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("update paytype failed");
            }
        });
    }

    private void updateWorkbase(String workbaseIds) {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().updateWorkBase(activity.getUserID(), workbaseIds, activity.getJWT());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (activity.checkStatus(response.body())) {

                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("update workbase failed");
            }
        });
    }
}
