package com.task24.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.task24.R;
import com.task24.model.ServicesModel.Data;
import com.task24.util.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.SimpleViewHolder>
        implements Filterable {

    private List<Data> mDataset;
    private List<Data> mDatasetFiltered;
    private Context context;
    private boolean isSearchMode = false;

    public ServicesAdapter(Context context, List<Data> objects) {
        this.mDataset = objects;
        this.mDatasetFiltered = objects;
        this.context = context;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_full, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        Data item = mDatasetFiltered.get(position);
        holder.tvTitle.setText(item.name);
//        if (isSearchMode) {
//            if (position == 0) {
//                holder.tvHeading.setVisibility(View.VISIBLE);
//                holder.tvHeading.setText(R.string.all_types);
//            } else {
//                holder.tvHeading.setVisibility(View.GONE);
//            }
//        } else {
//            if (position == 0) {
//                holder.tvHeading.setVisibility(View.VISIBLE);
//                holder.tvHeading.setText(R.string.most_popular);
//            } else if (position == 6) {
//                holder.tvHeading.setVisibility(View.VISIBLE);
//                holder.tvHeading.setText(R.string.all_types);
//            } else {
//                holder.tvHeading.setVisibility(View.GONE);
//            }
//        }
        holder.tvHeading.setVisibility(View.GONE);

        if (item.isSelected) {
            holder.tvTitle.setBackground(ContextCompat.getDrawable(context, R.drawable.blue_button_bg));
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.white));
            Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFTEXT_BOLD);
            holder.tvTitle.setTypeface(tf);
        } else {
            holder.tvTitle.setBackgroundColor(Color.TRANSPARENT);
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black));
            Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFTEXT_REGULAR);
            holder.tvTitle.setTypeface(tf);
        }

        holder.rlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelected();
                mDatasetFiltered.get(position).isSelected = true;
                notifyDataSetChanged();
            }
        });
    }

    private void clearSelected() {
        if (mDatasetFiltered != null && mDatasetFiltered.size() > 0) {
            for (int i = 0; i < mDatasetFiltered.size(); i++) {
                mDatasetFiltered.get(i).isSelected = false;
            }
        }
    }

    public Data getSelectedItem() {
        if (mDatasetFiltered != null && mDatasetFiltered.size() > 0) {
            for (int i = 0; i < mDatasetFiltered.size(); i++) {
                if (mDatasetFiltered.get(i).isSelected) {
                    return mDatasetFiltered.get(i);
                }
            }
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mDatasetFiltered != null ? mDatasetFiltered.size() : 0;
    }

    public List<Data> getData() {
        return mDatasetFiltered;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_heading)
        TextView tvHeading;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.rl_view)
        View rlView;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    isSearchMode = false;
                    mDatasetFiltered = mDataset;
                    //clearSelected();
                    //selectFirst();
                } else {
                    isSearchMode = true;
                    List<Data> filteredList = new ArrayList<>();
                    for (Data row : mDataset) {
                        String rowText = row.name.toLowerCase();
                        if (!TextUtils.isEmpty(rowText)) {
                            if (rowText.contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }
                    }

                    mDatasetFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDatasetFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDatasetFiltered = (List<Data>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
