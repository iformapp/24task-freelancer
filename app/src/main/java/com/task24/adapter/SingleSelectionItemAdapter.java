package com.task24.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.task24.R;
import com.task24.util.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SingleSelectionItemAdapter extends RecyclerView.Adapter<SingleSelectionItemAdapter.SimpleViewHolder> {

    private List<String> mDataset;
    private Context context;
    private int selectedPosition;

    public SingleSelectionItemAdapter(Context context, ArrayList<String> objects, int selectedPosition) {
        this.mDataset = objects;
        this.context = context;
        this.selectedPosition = selectedPosition;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_full, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        String item = mDataset.get(position);

        holder.tvTitle.setText(item);
        holder.tvTitle.setGravity(Gravity.CENTER);

        if (selectedPosition == position) {
            holder.tvTitle.setBackground(ContextCompat.getDrawable(context, R.drawable.blue_button_bg));
            Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFTEXT_BOLD);
            holder.tvTitle.setTypeface(tf);
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.white));
        } else {
            holder.tvTitle.setBackgroundColor(Color.TRANSPARENT);
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black));
            Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFTEXT_REGULAR);
            holder.tvTitle.setTypeface(tf);
        }

        holder.rlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                notifyDataSetChanged();
            }
        });
    }

    public String getSelectedItem() {
        return mDataset.get(selectedPosition);
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    @Override
    public int getItemCount() {
        return mDataset != null ? mDataset.size() : 0;
    }

    public List<String> getData() {
        return mDataset;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.rl_view)
        View rlView;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
