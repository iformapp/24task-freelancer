package com.task24.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewSFDisplayBold;
import android.textview.TextViewSFDisplayRegular;
import android.textview.TextViewSFTextMedium;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.task24.R;
import com.task24.model.Projects;
import com.task24.ui.projects.ProjectCycleActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.SimpleViewHolder> {

    private Context context;
    private ArrayList<Projects> projectsList;

    public ProjectsAdapter(Context context) {
        this.context = context;
    }

    public void doRefresh(ArrayList<Projects> projectsList) {
        this.projectsList = projectsList;
        notifyDataSetChanged();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_projects_list, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        Projects item = projectsList.get(position);
        holder.tvTitle.setText(item.title);
        holder.tvDaysleft.setText(item.daysleft);
        holder.tvBudget.setText(item.budget);
        holder.tvBids.setText(item.bids);
        //holder.tvStatus.setText("");
    }

    @Override
    public int getItemCount() {
        return projectsList.size();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_status)
        TextViewSFTextMedium tvStatus;
        @BindView(R.id.tv_title)
        TextViewSFDisplayBold tvTitle;
        @BindView(R.id.tv_daysleft)
        TextViewSFDisplayRegular tvDaysleft;
        @BindView(R.id.tv_bids)
        TextViewSFDisplayRegular tvBids;
        @BindView(R.id.tv_budget)
        TextViewSFDisplayBold tvBudget;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ProjectCycleActivity.class);
                    context.startActivity(i);
                }
            });
        }
    }
}
