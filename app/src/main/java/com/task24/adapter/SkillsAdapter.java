package com.task24.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.textview.TextViewSFTextRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.task24.R;
import com.task24.model.Skill;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SkillsAdapter extends RecyclerView.Adapter<SkillsAdapter.SimpleViewHolder> {

    private ArrayList<Skill> mDataset;
    private Context context;

    public SkillsAdapter(Context context, ArrayList<Skill> objects) {
        this.mDataset = objects;
        this.context = context;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_skills, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        Skill item = mDataset.get(position);
        holder.tvLanguage.setText(item.skillTitle);
        holder.tvLevel.setText(item.skillValue);
    }

    @Override
    public int getItemCount() {
        return mDataset != null ? mDataset.size() : 0;
    }

    public ArrayList<Skill> getData() {
        return mDataset;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_language)
        TextViewSFTextRegular tvLanguage;
        @BindView(R.id.tv_level)
        TextViewSFTextRegular tvLevel;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
