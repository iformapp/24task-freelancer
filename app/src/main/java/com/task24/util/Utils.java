package com.task24.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.task24.model.ServicesModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Utils {

    private static final String ENCODING_UTF_8 = "UTF-8";

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public static SpannableStringBuilder getBoldString(Context context, String s, String[] fonts, int[] colorList, String[] words) {
        if (TextUtils.isEmpty(s))
            return null;

        SpannableStringBuilder ss = new SpannableStringBuilder(s);
        try {
            for (int i = 0; i < words.length; i++) {
                if (s.contains(words[i])) {
                    if (colorList != null) {
                        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, colorList[i])), s.indexOf(words[i]),
                                s.indexOf(words[i]) + words[i].length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }

                    if (fonts != null && fonts.length > i) {
                        Typeface font = Typeface.createFromAsset(context.getAssets(), fonts[i]);
                        ss.setSpan (new CustomTypefaceSpan("", font), s.indexOf(words[i]), s.indexOf(words[i]) + words[i].length(),
                                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ss;
    }

    public static SpannableStringBuilder getColorString(Context context, String s, String word, int color) {
        SpannableStringBuilder ss = new SpannableStringBuilder(s);
        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), s.indexOf(word),
                s.indexOf(word) + word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }

    public static void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink,
                    startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setHighlightColor(Color.TRANSPARENT); // prevent TextView change background when highlight
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    public static void makeLinks(CheckBox checkBox, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(checkBox.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = checkBox.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink,
                    startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        checkBox.setHighlightColor(Color.TRANSPARENT); // prevent TextView change background when highlight
        checkBox.setMovementMethod(LinkMovementMethod.getInstance());
        checkBox.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null)
            return;

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static String priceWith$(Object object) {
        String price = String.valueOf(object);
        if (TextUtils.isEmpty(price)) {
            return "$0";
        }
        if (price.contains("$")) {
            price = price.replace("$", "");
        }
        return "$" + price.trim();
    }

    public static String priceWithout$(Object object) {
        String price = String.valueOf(object);
        if (TextUtils.isEmpty(price)) {
            return "0";
        }
        if (price.contains("$")) {
            price = price.replace("$", "");
        }
        return price.trim();
    }

    public static void openSoftKeyboard(Activity activity, View view) {
        if (activity == null)
            return;

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static String numberFormat(String number, int decimalPoint) {
        try {
            Double d = Double.parseDouble(number);
            StringBuilder pattern = new StringBuilder();
            pattern.append("0.");
            for (int i = 0; i < decimalPoint; i++) {
                pattern.append("0");
            }
            NumberFormat nf = new DecimalFormat(pattern.toString());
            return nf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return number;
    }

    public static String numberFormat(double number, int decimalPoint) {
        try {
            StringBuilder pattern = new StringBuilder();
            pattern.append("0.");
            for (int i = 0; i < decimalPoint; i++) {
                pattern.append("0");
            }
            NumberFormat nf = new DecimalFormat(pattern.toString());
            return nf.format(number);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(number);
    }

    public static int getServiceId(Context context, String serviceName) {
        List<ServicesModel.Data> mData = Preferences.getTopServices(context);
        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i).name.equalsIgnoreCase(serviceName)) {
                return mData.get(i).id;
            }
        }
        return 0;
    }

    public static String getExperienceLevel(int exp) {
        String level = "";
        switch (exp) {
            case Constants.LESS_THAN_1_ID :
                level = Constants.LESS_THAN_1;
                break;
            case Constants.YEAR_1_3_ID :
                level = Constants.YEAR_1_3;
                break;
            case Constants.YEAR_4_6_ID :
                level = Constants.YEAR_4_6;
                break;
            case Constants.YEAR_7_9_ID :
                level = Constants.YEAR_7_9;
                break;
            case Constants.YEAR_10_12_ID :
                level = Constants.YEAR_10_12;
                break;
            case Constants.YEAR_13_ID :
                level = Constants.YEAR_13;
                break;
        }
        return level;
    }

    public static int getExperienceLevel(String exp) {
        int level = 0;
        switch (exp) {
            case Constants.LESS_THAN_1 :
                level = Constants.LESS_THAN_1_ID;
                break;
            case Constants.YEAR_1_3 :
                level = Constants.YEAR_1_3_ID;
                break;
            case Constants.YEAR_4_6 :
                level = Constants.YEAR_4_6_ID;
                break;
            case Constants.YEAR_7_9 :
                level = Constants.YEAR_7_9_ID;
                break;
            case Constants.YEAR_10_12 :
                level = Constants.YEAR_10_12_ID;
                break;
            case Constants.YEAR_13 :
                level = Constants.YEAR_13_ID;
                break;
        }
        return level;
    }

    public static String getLanguageLevel(int levelId) {
        String level = "";
        switch (levelId) {
            case Constants.BASIC_ID:
                level = Constants.BASIC;
                break;
            case Constants.CONVERSATIONAL_ID:
                level = Constants.CONVERSATIONAL;
                break;
            case Constants.FLUENT_ID:
                level = Constants.FLUENT;
                break;
            case Constants.NATIVE_ID:
                level = Constants.NATIVE;
                break;
        }
        return level;
    }

    public static String getEducationLevel(int levelId) {
        String level = "";
        switch (levelId) {
            case Constants.Associate_ID:
                level = Constants.Associate;
                break;
            case Constants.Bachelor_ID:
                level = Constants.Bachelor;
                break;
            case Constants.Master_ID:
                level = Constants.Master;
                break;
            case Constants.Doctorate_ID:
                level = Constants.Doctorate;
                break;
        }
        return level;
    }

    public static int getEducationLevel(String level) {
        int levelId = 0;
        switch (level) {
            case Constants.Associate :
                levelId = Constants.Associate_ID;
                break;
            case Constants.Bachelor :
                levelId = Constants.Bachelor_ID;
                break;
            case Constants.Master :
                levelId = Constants.Master_ID;
                break;
            case Constants.Doctorate :
                levelId = Constants.Doctorate_ID;
                break;
        }
        return levelId;
    }

    public static String getRatingLevel(int rating) {
        String level = "";
        switch (rating) {
            case Constants.Beginner_ID :
                level = Constants.Beginner;
                break;
            case Constants.Intermediate_ID :
                level = Constants.Intermediate;
                break;
            case Constants.Expert_ID :
                level = Constants.Expert;
                break;
        }
        return level;
    }

    public static int getRatingId(String rating) {
        int ratingId = 0;
        switch (rating) {
            case Constants.Beginner :
                ratingId = Constants.Beginner_ID;
                break;
            case Constants.Intermediate :
                ratingId = Constants.Intermediate_ID;
                break;
            case Constants.Expert :
                ratingId = Constants.Expert_ID;
                break;
        }
        return ratingId;
    }

    public static String changeDateFormat(String source, String target, String dateString) {
        SimpleDateFormat input = new SimpleDateFormat(source);
        SimpleDateFormat output = new SimpleDateFormat(target);
        try {
            Date date = input.parse(dateString);
            return output.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public static void loadImage(Context context, String url, ImageView imageView) {
        if (context != null && imageView != null)
            Glide.with(context).load(url).into(imageView);
    }

    public static void loadImage(Context context, int drawable, ImageView imageView) {
        if (context != null && imageView != null)
            Glide.with(context).load(drawable).into(imageView);
    }

    public static String getFileExtFromBytes(File f) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            byte[] buf = new byte[5]; //max ext size + 1
            fis.read(buf, 0, buf.length);
            StringBuilder builder = new StringBuilder(buf.length);
            for (int i=1;i<buf.length && buf[i] != '\r' && buf[i] != '\n';i++) {
                builder.append((char)buf[i]);
            }
            return builder.toString().toLowerCase();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static String base64Decode(String string) {
        String decoded;
        try {
            byte[] bytes = Base64.decode(string, Base64.URL_SAFE | Base64.NO_WRAP | Base64.NO_PADDING);
            decoded = new String(bytes, ENCODING_UTF_8);
        } catch (IllegalArgumentException e) {
            throw new DecodeException("Received bytes didn't correspond to a valid Base64 encoded string.", e);
        } catch (UnsupportedEncodingException e) {
            throw new DecodeException("Device doesn't support UTF-8 charset encoding.", e);
        }
        return decoded;
    }

    public static String decode(String token) {
        final String[] parts = splitToken(token);
        return base64Decode(parts[1]);
    }

    private static String[] splitToken(String token) {
        String[] parts = token.split("\\.");
        if (parts.length == 2 && token.endsWith(".")) {
            //Tokens with alg='none' have empty String as Signature.
            parts = new String[]{parts[0], parts[1], ""};
        }
        if (parts.length != 3) {
            throw new DecodeException(String.format("The token was expected to have 3 parts, but got %s.", parts.length));
        }
        return parts;
    }

    public static String getFileNameFromUrl(String url) {
        try {
            URL resource = new URL(url);
            String urlString = resource.getFile();
            return urlString.substring(urlString.lastIndexOf('/') + 1).split("\\?")[0].split("#")[0];
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }
}
