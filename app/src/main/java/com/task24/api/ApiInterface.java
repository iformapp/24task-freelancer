package com.task24.api;

import com.task24.model.Available;
import com.task24.model.ClientRate;
import com.task24.model.GeneralModel;
import com.task24.model.GetPrice;
import com.task24.model.JWTData;
import com.task24.model.Language;
import com.task24.model.Profile;
import com.task24.model.ServicesModel;
import com.task24.model.SkillsById;
import com.task24.model.UserSkillsModel;
import com.task24.util.Constants;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiInterface {

    @FormUrlEncoded
    @POST(Constants.LOGIN)
    Call<Object> login(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST(Constants.SIGNUP)
    Call<Object> register(@Field("username") String username, @Field("password") String password,
                          @Field("email") String email, @Field("profile_type_id") int profileTypeId);

    @FormUrlEncoded
    @POST(Constants.SOCIAL_LOGIN)
    Call<Object> socialLogin(@Field("google_id") String google_id, @Field("facebook_id") String facebook_id,
                             @Field("username") String username, @Field("first_name") String first_name,
                             @Field("last_name") String last_name, @Field("email") String email,
                             @Field("profile_type_id") int profileTypeId);

    @FormUrlEncoded
    @POST(Constants.UPDATE_PROFILE + "/{id}.json")
    Call<GeneralModel> updateName(@Path("id") int id, @Field("first_name") String first_name,
                                  @Field("last_name") String last_name, @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.UPDATE_PROFILE + "/{id}.json")
    Call<GeneralModel> updateWorkBase(@Path("id") int id, @Field("workbase") String workbase,
                                      @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.UPDATE_EXPERIENCE)
    Call<GeneralModel> updateExperience(@Field("profile_id") int profile_id, @Field("length") String length,
                                        @Field("service_category_id") String service_id, @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.ADD_PAY_TYPES)
    Call<GeneralModel> addPayTypes(@Field("profile_id") int profile_id, @Field("pay_type_id") String pay_type_id,
                                   @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.UPDATE_PAY_TYPES + "/{id}.json")
    Call<GeneralModel> updatePayTypes(@Path("id") int id, @Field("pay_type_id") int pay_type_id,
                                      @Field("status") int status, @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.ADD_LANGUAGES)
    Call<GeneralModel> addLanguages(@Field("profile_id") int profile_id, @Field("language_id") String language_id,
                                    @Field("level") String level, @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.ADD_SKILLS + "/{id}.json")
    Call<GeneralModel> addSkills(@Path("id") int id, @Field("skill_id") String skill_id,
                                 @Field("rating") String rating, @Field("jwt") String jwt);

    @Multipart
    @POST(Constants.UPDATE_PROFILE + "/{id}.json")
    Call<Object> updateProfile(@Path("id") int id, @Part("first_name") RequestBody first_name,
                               @Part("last_name") RequestBody last_name, @Part("email") RequestBody email,
                               @Part("contactNo") RequestBody contactNo, @Part("mobile_prefix") RequestBody mobile_prefix,
                               @Part("jwt") RequestBody jwt, @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST(Constants.UPDATE_PROFILE + "/{id}.json")
    Call<Object> updateClientProfile(@Path("id") int id, @Field("first_name") String first_name,
                                     @Field("last_name") String last_name, @Field("email") String email,
                                     @Field("contactNo") String contactNo, @Field("mobile_prefix") String mobile_prefix,
                                     @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.UPDATE_PROFILE + "/{id}.json")
    Call<GeneralModel> addPayRate(@Path("id") int id, @Field("pay_rate") String pay_rate,
                                  @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.HEADLINES)
    Call<GeneralModel> addHeadlines(@Field("profile_id") int profile_id, @Field("content") String content,
                                    @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.SUMMARIES)
    Call<GeneralModel> addSummay(@Field("profile_id") int profile_id, @Field("content") String content,
                                 @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.EDUCATIONS)
    Call<GeneralModel> addEducations(@Field("profile_id") int profile_id, @Field("degree") String degree,
                                     @Field("school_name") String school_name, @Field("start_date") String start_date,
                                     @Field("end_date") String end_date, @Field("level") String level,
                                     @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.UPDATE_EXPERIENCE)
    Call<GeneralModel> addEmployment(@Field("profile_id") int profile_id, @Field("length") String length,
                                     @Field("service_category_id") String service_id, @Field("start_date") String start_date,
                                     @Field("end_date") String end_date, @Field("company_name") String company_name,
                                     @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.LOCATION)
    Call<GeneralModel> updateLocation(@Field("profile_id") int profile_id, @Field("country") String country,
                                      @Field("region") String region, @Field("city") String city,
                                      @Field("longitude") double longitude, @Field("latitude") double latitude,
                                      @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.UPDATE_PASSWORD + "/{id}.json")
    Call<GeneralModel> updatePassword(@Path("id") int id, @Field("oldPassword") String oldPassword,
                                      @Field("password") String password, @Field("jwt") String jwt);

    @FormUrlEncoded
    @POST(Constants.FEEDBACK)
    Call<GeneralModel> addFeedback(@Field("profile_id") int profile_id, @Field("note") String note,
                                   @Field("jwt") String jwt);

    @Multipart
    @POST(Constants.RESUME)
    Call<GeneralModel> addResume(@Part("profile_id") RequestBody profile_id, @Part MultipartBody.Part file,
                                 @Part("jwt") RequestBody jwt);

    @FormUrlEncoded
    @POST(Constants.FORGOT_PASSWORD)
    Call<JWTData> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST(Constants.RESET_PASSWORD)
    Call<GeneralModel> resetPassword(@Field("password") String password, @Field("otp") String otp,
                                     @Field("email") String email);

    @GET(Constants.SERVICES)
    Call<ServicesModel> getServices();

    @GET(Constants.GET_PROFILE + "/{id}.json")
    Call<Profile> getProfile(@Path("id") int id);

    @GET(Constants.LANGUAGES)
    Call<Language> getLanguages();

    @GET(Constants.TOP_SERVICES)
    Call<ServicesModel> getTopServices();

    @GET(Constants.SKILLS + "/{id}.json")
    Call<UserSkillsModel> getSkills(@Path("id") int id);

    @GET(Constants.PAY_TYPE)
    Call<Available> getPayType();

    @FormUrlEncoded
    @POST(Constants.ADD_FREE_TRIAL)
    Call<GeneralModel> addFreeTrial(@Field("fullname") String fullname, @Field("phone") String phone,
                                    @Field("website") String website, @Field("description") String description,
                                    @Field("email") String email, @Field("subject") String subject,
                                    @Field("location") String location, @Field("jwt") String jwt, @Field("profile_id") int profile_id);

    @FormUrlEncoded
    @POST(Constants.PRICE_CALCULATION)
    Call<GetPrice> getPriceCalculation(@Field("noOfHours") String noOfHours);

    @FormUrlEncoded
    @POST(Constants.TOKEN_EXPIRED)
    Call<GeneralModel> isTokenExpired(@Field("jwt") String jwt);

    @POST(Constants.VERIFY_EMAIL + "/{id}.json")
    Call<GeneralModel> verifyEmail(@Path("id") int id);

    @FormUrlEncoded
    @POST(Constants.VERIFY_EMAIL_OTP + "/{id}.json")
    Call<GeneralModel> verifyEmailOtp(@Path("id") int id, @Field("otp") String otp);

    @FormUrlEncoded
    @POST(Constants.VERIFY_FACEBOOK + "/{id}.json")
    Call<GeneralModel> verifyFacebook(@Path("id") int id, @Field("facebook_id") String facebookId);

    @Multipart
    @POST(Constants.VERIFY_ID + "/{id}.json")
    Call<GeneralModel> verifyProfileId(@Path("id") int id, @Part("type") RequestBody type, @Part MultipartBody.Part file,
                                       @Part("jwt") RequestBody jwt);

    @FormUrlEncoded
    @POST(Constants.GET_SKILL_BY_ID)
    Call<SkillsById> getSkillsById(@Field("service_id") String serviceId);

    @FormUrlEncoded
    @POST(Constants.GET_CLIENT_RATES)
    Call<ClientRate> getClientRates(@Field("pay_type_id") int payTypeId);

    @Multipart
    @POST(Constants.ADD_JOB_POST)
    Call<GeneralModel> addJobPost(@Part("profile_id") RequestBody profile_id, @Part MultipartBody.Part[] file,
                                  @Part("jwt") RequestBody jwt, @Part("description") RequestBody description,
                                  @Part("client_rate_id") RequestBody clientRateId, @Part("skill_id") RequestBody skillId,
                                  @Part("budget") RequestBody budget, @Part("pay_type_id") RequestBody payTypeId,
                                  @Part("title") RequestBody title);
}
